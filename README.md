# ML 4 LTDB

This study aims to apply ML algorithms to detects abnormal signals from LTDBs (see [ArXiv:106.08046](https://arxiv.org/abs/1806.08046))  of the ATLAS calorimeter.

**Authors**: Anthony Correia, Enzo Merzoug.\
**Supervisor**: Philippe Schwemling

## Installation

Using a conda environment is not required. However, using the default conda environment ensures the python scripts will work properly.  It is also an extremely easy way to install ROOT (the v6.24/06 is used in this analysis).

### Install conda

Please refer to the [official documentation](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html) to install conda in linux. 

For instance, in linux ,  miniconda can be downloaded and installed with
```
curl -O https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh -p $HOME/miniconda3
```

Then, to run conda, you can write:
```
source $HOME/miniconda3/bin/activate
```

The latter can be inserted in your `.bashrc` file.

### Setup the conda environment

The conda environment is named `ML4LTB`. To install it, you can follow the 1st way (with the `environment.yml` file) or the 2nd way (using only bash commands).

#### 1st option: using the `environment.yml` file (not tested)

The conda environment `ML4LTDB` can be cloned using the `setup/environment.yml` file:
```
conda env create -f setup/environment.yml
```

#### 2nd option: using bash commands (tested)

To install the necessary libraries, please have a look at `setup/setup_env.sh`. In theory, you can run:
```
source setup/setup_env.sh
```
(or you can run each line of thile bash script separately).

### Enter the environment

To enter the `ML4LTDB` environment, run:
```
conda activate ML4LTDB
```

Moreover, to define environment variables used in the analysis, and add the path of the repository to the `PYTHONPATH`, run:

```
source setup/setup.sh
```

### `.bashrc`

Personnaly, I have defined the following command in my `.bashrc` file:
```
alias activate_ML4LTDB='source ~/miniconda3/bin/activate ; source ~/.../ml4ltdb/setup/setup.sh ; conda activate ML4LTDB'
```
where `~/.../` has to be changed into the path to the local repository.

Thanks to it, I just need to run `activate_ML4LTDB` to run conda, load the environment variables of the project and open the conda environment.

### Configuration (input and output files)

Paths can be configured in `project/loc.py`:
- `root_folder`: path where all the ROOT files are located
- `out_folder`: path where the images, tables, pickle files, json files are saved

**Please change these paths to what suit you.**

The ROOT files (located in `root_folder`) must be located and named in the following way:
`{root_folder}/{run}/atlas/data21_900GeV.{run}.physics_L1Calo.merge.RAW._lb0596._SFO-ALL._{4 numbers}.root`
For instance:
`{root_folder}/00405127/atlas/data21_900GeV.00405127.physics_L1Calo.merge.RAW._lb0596._SFO-ALL._0001.root`

The format of the root file name can actually be changed by modifying the regular expression `unproc_file_format` in `lib.project.defs.py`.

### Open a jupyter notebook
In order to open a jupyter notebook, please run in your command line interface
```
jupyter notebook
```
and open the link that appeared in your internet browser. The link should look like this
```
http://127.0.0.1:8888/?token=many_characters
```

## Structure of the repository

This repository is organised into folder:
- `lib` contains general-purpose python functions used throughout the analysis (e.g., plotting, loading samples, ...).
- `project` contains variables and functions specific to the project.
- `scripts` contains the python scripts
- `others` contains other files not used directly in the analysis, but relevant to it
- `output` contains the output files of the analysis
- `notebook` contains python notebooks, mainly used for development and checks. There are also jupyter notebooks in `scripts`.

In short, the python scripts that are run are in the folder `scripts`. They use python functions defined in `lib` and `project`.

## Analysis

For more information about the analysis, please refer to the `README.md` file in the folder `scripts` and its associated jupyter notebook.


Several notebooks are scattered across the folders in `scripts`. They have been used to develop the main scripts and for sub-analyses. They are however not used in the main analysis.

<!-- This project is divided into 2 distinct objectives
1. Select and study signal events using a combination of one-dimensional cuts and multi-dimensional custering
2. Identify and study noise events

An important note is that layers 1 and 2, and layer 0 and 3 have similar behaviours. Thus, it is desirable to study these layers separately.

In this analysis, we limit ourselves with signal having an ADC amplitude higher or equal that 4 ($e_{\text{max}} - e_{\text{min} > 3}$). Lower amplitude events are considered too ambiguous to discern. One could also argue that such events would not be used in a real scenario anyway.

### Clustering on the full data without preselection

Clustering directly on the layer 2 with the cut $e_{\text{max}} - e_{\text{min} > 3}$ already allows to find few interesting categories.
- A signal category

<img src="Illustrations/layer_2_all/signal.png" alt="signal" width="200"/>

- A 1-peak category

<img src="Illustrations/layer_2_all/one_peak.png" alt="signal" width="200"/>

- A 2-peak category 

<img src="Illustrations/layer_2_all/two_peaks.png" alt="signal" width="200"/>

**TODO:** It might be interesting to check whether:
- the probability of having 2 peaks is consistent with the probability of have only one
- the probabily of having a peak is consistent with a Gaussian law.

**Note:** it could be interesting to study the difference between this clustering and the same clustering with the cut $p_{\text{max}}<20$. It is also with considering performing the cut p_{\text{max}}>20$ when it comes to study background.

### Select and study signal events

#### Peaked background
"Peaked" events of **layer 1 and 2** are removed using the following cut $\frac{\min{\left(\sigma_+, \sigma_-\right)}}{\sigma_+ + \sigma_-} >= 0.02$. Actually, this cut could be reduced to $\frac{\min{\left(\sigma_+, \sigma_-\right)}}{\sigma_+ + \sigma_-} \neq 0$, but a somewhat more stringent cut is desirable. 

Note that this cut does not remove only peaked background in layer 0 and 3. Other forms of background exist in these layers.

#### $\chi^2$
A $\chi^2$ between an event and the signal simulated shape is computed for each event (using even an overkilled optimisation procedure to reduce the $\chi^2$ at its most!) . It is shown that the $\chi^2$ is a good signal discriminant for every layers, and is better for large amplitude (most probably because noise level is smaller relative to the signal amplitude).

A quite stringent cut $\chi^2 < 0.015$ can be applied to remove obvious noise events (and possibly not obvious signal events). It is shown that this cut is reasonable for any layers.

#### Clustering

At this point, 3 cuts are applied:
- $\chi^2 < 0.015$
- $\frac{\min{\left(\sigma_+, \sigma_-\right)}}{\sigma_+ + \sigma_-} > 0.02$
- $e_{\text{max}} - e_{\text{min}} > 3$

Given the powerfullness of the $\chi^2$ cut, other cuts (on $p_{\text{max}}$ for instance) are unnecessary.

It is shown that an HDBSCAN algorithm is able to find different categories signal shapes for layer 1 and 2. For layer 0 and 3, it is not able to find any categories: the $\chi^2$ cut only retains signal events.

Using a very large number of events (of a full run), it is found that the categories of signal shapes found by the clusterer are highly correlated with where it stands inside the ATLAS calorimeter. Thus, it is relevant to perform separate clustering for each detector part.
Clustering on these detectors allow a more precise refining : few background events are distinguished, which can thus be removed from the dataframe. These background look like a peak or a square signal together with a random noise. 

### Background

The main purpose of this project is not actually selecting signal, but rather studying noise events. On top of "Gaussian" noise events, they are also strangely behaved background events. 

#### Inverted cut

The most natural background to analyse is with the inverted cut $\chi^2 > 0.015$, where the cut $\frac{\min{\left(\sigma_+, \sigma_-\right)}}{\sigma_+ + \sigma_-} > 0.02$ is kept for layer 1 and 2.

Following this precedure, 1-peak, 2-peak (and some 3-peak) events are found.

For the layer 0 and 3, removing the cut $\frac{\min{\left(\sigma_+, \sigma_-\right)}}{\sigma_+ + \sigma_-} < 0.02$ (which does not make much sense as it does not only remove peaked background) the medium and fast oscillating background (shown in next section) are found (but not the slower oscllations!).

#### Layer 0 and 3

In layer 0 and 3, the level of noise if much higher than in layer 1 and 3. Moreover, strange background shapes have been observed
- Oscillating background at a high, medium and small frequency (found using the inverted cut $\frac{\min{\left(\sigma_+, \sigma_-\right)}}{\sigma_+ + \sigma_-} < 0.02$).

<img src="Illustrations/oscillations/oscillation_fast.png" alt="oscillation fast" width="200"/>
<img src="Illustrations/oscillations/oscillation_medium.png" alt="oscillation medium" width="200"/>
<img src="Illustrations/oscillations/oscillation_slow.png" alt="oscillation slow" width="200"/>

- Square and almost periodic shapes

- peaks


Now, what remain to be done are
1. have a more sensitive way to select some categories of background (rather than just performing some random cut)
2. see where the background events stand in the calorimeter
3. Check whether the probability of having 1 cut is constant with the probability of having 2
4. Look at when the events are measured, and check whether it looks like a poisson law for a given run -->



