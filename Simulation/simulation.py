## Simulation of Gaussian noise + a signal in order to test ML algorithms on it

import sys

from pandas.core.frame import DataFrame
sys.path.append("/home/bunwe/ml4ltdb/")

import random  
import matplotlib.pyplot as plt 

from typing import List, Optional
import matplotlib as mpl

import numpy as np

import pandas as pd

from tqdm import tqdm

from project.files import compute_voi

column_names = [f's{i}' for i in range(32)]
features = ['mean', 'std', 'max', 'min', 
    'pedestal', 'median', 'pos_max', 'pos_min']

def signal1(pos:int, neg:int, amplitude:float=1000):
    """ Creates a dataframe with a dirac of amplitude amplitude at position pos and a dirac of amplitude - amplitude at position neg.
    
    Args:
       len : length of the list
       pos : position of the positive dirac
       neg : position of the negative dirac
       amplitude : amplitude of the diracs
    
    Returns:
        df : the dataframe of values corresponding to the wanted signal
    """

    df = pd.DataFrame()
    df[column_names] = [[0]*32]
    df['is_signal'] = True
    df['s'+str(pos)] = [amplitude]
    df['s'+str(neg)] = [-amplitude]

    return(df)


def generate_dataset(nbr_sig, nbr, std):
    """ Generate a dataset to try ML algos on.
    
    Args:
       nbr_sig : number of signals
       nbr : total number of samples (signals + pure noise)
       
    
    Returns:
        dataset : the dataset
    """
    sample = signal1(5,25)
    signals = signal1(5,25)
    noise = np.random.normal(scale=std, size=(nbr, 32))
    dataset = pd.DataFrame(noise, columns=column_names)
    dataset['is_signal'] = False
    zeros = pd.DataFrame(np.zeros((nbr-nbr_sig,len(column_names))), columns=column_names)
    zeros['is_signal'] = False

    for i in tqdm(range(nbr_sig-1)):
        signals = pd.concat([signals, sample])
    
    signals = pd.concat([signals,zeros]).reset_index()
    dataset += signals
    
    return compute_voi(dataset) 

def generate_dataset_2(nbr_sig, nbr, std):
    """ Generate a dataset to try ML algos on with random amplitude, pos_max and pos_min for the signals.
    
    Args:
       nbr_sig : number of signals
       nbr : total number of samples (signals + pure noise)
       
    
    Returns:
        dataset : the dataset
    """
    amplitude_sig = 1000*(1+np.random.normal(scale=0.2))
    signals = signal1(5, 25, amplitude = amplitude_sig)
    noise = np.random.normal(scale=std, size=(nbr, 32))
    dataset = pd.DataFrame(noise, columns=column_names)
    dataset['is_signal'] = False
    zeros = pd.DataFrame(np.zeros((nbr-nbr_sig, len(column_names))), columns=column_names)
    zeros['is_signal'] = False

    for i in tqdm(range(nbr_sig-1)):
        pos_max = np.random.randint(0, 30)
        pos_min = np.random.randint(pos_max+1, 31)
        amplitude_sig = 1000*(1+np.random.normal(scale=0.2))
        sample = signal1(pos_max, pos_min, amplitude = amplitude_sig)
        signals = pd.concat([signals, sample])
    
    signals = pd.concat([signals, zeros]).reset_index()
    dataset += signals
    
    return compute_voi(dataset) 


# def noise_plot(signal:List[float], std:float=50,
#     ax:Optional[mpl.axes.Axes]=None):
#     """ Adds gaussian noise to a given signal and returns a plot.
    
#     Args:
#        signal : the signal
#        std : standard deviation of the noise 
#        ax : axes of the plot
    
#     Returns:
#         fig, ax : the simulation plot
#         nums : the simulation list 
#     """
    
#     ax_was_None = False
#     if ax is None:
#         fig, ax = plt.subplots(figsize=(8, 6))
#         ax_was_None = True
#     nums = []  

#     for i in range(len(signal)):  
#         temp = random.gauss(0, std) 
#         nums.append(temp)  
#         nums[i] += signal[i] 

#     ax.plot(nums)
#     ax.set_xlabel("Time (ns)")
#     ax.set_ylabel("Amplitude")

#     if ax_was_None:
#         return fig, ax

#     return(nums)

# def noise(signal:List[float], std:float=50):
#     """ Adds gaussian noise to a given signal.
    
#     Args:
#        signal : the signal
#        std : standard deviation of the noise 
    
#     Returns:
#         nums : the simulation list 
#     """
#     nums = []  
#     for i in range(len(signal)):  
#         temp = random.gauss(0, std) 
#         nums.append(temp)  
#         nums[i] += signal[i] 

#     return(nums)

# def simulation_voi(simu):
#     """ Gives the variables of interest of a simulation:
    
#     * `'pedestal'`: first value 
#     * `'mean'`: time average 
#     * `'std'`: standard deviation
#     * `'max'` maximal value recorded
#     * `'min'`: minimal value recorded
#     * `'median'`: median
#     * `'pos_min'`: index of the min value
#     * `'pos_max'`: index of the max value

#     Args:
#         simu : the simulation

#     Returns:
#         df_voi : a dictionnary of the variables of interest
#     """

#     simu = np.array(simu)
#     df_voi = pd.DataFrame()
 
#     df_voi['pedestal'] = [simu[0]]
#     df_voi['mean'] = np.mean(simu)
#     df_voi['std'] = np.sqrt(np.mean(np.square(simu)) - df_voi['mean']**2)
#     df_voi['max'] = max(simu)
#     df_voi['min'] = min(simu)
#     df_voi['median'] = np.median(simu)
#     df_voi['pos_min'] = np.argmin(simu)
#     df_voi['pos_max'] = np.argmax(simu)

#     return(df_voi)




