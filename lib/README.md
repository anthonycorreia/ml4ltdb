## Global package of ML4LTDB

This package contains modules that are supposed to be very general, and that do not (really) depend on this project.

- `bdt.py` contains functions to analyse the result of a BDT training. They were used at the start of the project, but we noticed the BDT approach was not so relevant.
- `plot.py` contains functions to plot histograms or evnnts
- `clustering.py` contains functions to plot the result of clustering
- `rootdata.py` contains functions to load a ROOT dataframe
- `utils.py` contain functions to load/save a JSON/PICKLE file.
