""" Module for BDT
"""

from typing import Union, Optional, List, Dict

import numpy as np
import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt

from sklearn.metrics import classification_report, roc_auc_score, roc_curve, auc
from sklearn.ensemble import AdaBoostClassifier

from lib.utils import array_like
import lib.plot as lp


def plot_roc(
    X_test: array_like, y_test: array_like,
    bdt: AdaBoostClassifier,
    ax=None
) -> Optional[Union[mpl.figure.Figure, mpl.axes.Axes]]:
    """ Plot the ROC curve

    Args:
        X_test: numpy.ndarray
            test sample containing only the columns used in the training.
        y_test: numpy.array
            test sample, `0` if the corresponding `X_test` event is 
            a background, `1` otherwise.
        bdt: trained BDT such as ADaboost. Other training can be used.

    Returns:
        fig, ax: figure and ax of the plot
    """

    ax_was_None = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(8, 6))
        ax_was_None = True

    # Get the results -----
    # result of the BDT of the test sample
    decisions = bdt.decision_function(X_test)
    fpr, tpr, _ = roc_curve(y_test, decisions)  # roc_curve
    # y_test: true results
    # decisions: result found by the BDT
    # fpr: Increasing false positive rates such that element i is the false positive rate of predictions with score >= thresholds[i].
    # tpr: Increasing true positive rates such that element i is the true positive rate of predictions with score >= thresholds[i].
    # thresholds: Decreasing thresholds on the decision function used to
    # compute fpr and tpr. thresholds[0] represents no instances being
    # predicted and is arbitrarily set to max(y_score) + 1
    roc_auc = auc(fpr, tpr)

    # Plot the results -----
    ax.plot(fpr, tpr, lw=1, label='ROC (area = %0.2f)' % (roc_auc))
    ax.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')
    ax.set_xlim([-0.05, 1.05])
    ax.set_ylim([-0.05, 1.05])
    ax.set_xlabel('False Positive Rate')
    ax.set_ylabel('True Positive Rate')
    title = 'Receiver operating characteristic'

    ax.legend(loc="lower right", fontsize=20.)
    lp.set_default_grid(ax)
    # Save the results -----

    if ax_was_None:
        return fig, ax


def compare_train_test(
    bdt: AdaBoostClassifier,
    X_train: array_like, y_train: array_like,
    X_test: array_like, y_test: array_like,
    bins=30,
    sig_color='g', bkg_color='r',
    ax: Optional[mpl.axes.Axes] = None
) -> Optional[Union[mpl.figure.Figure, mpl.axes.Axes]]:
    """ Plot the BDT output for the training and test sample.

    Args:
        bdt : trained BDT classifier
        X_train : training sample containing only the columns used in the training
        y_train : training sample, `0` if the corresponding `X_train` event is 
                a background, `1` otherwise.
        X_test : test sample containing only the columns used in the training.
        y_test : test sample, `0` if the corresponding `X_test` event is 
                a background, `1` otherwise.
        bins : number of bins of the plotted histograms

    Returns:
        fig, ax: figure and ax of the plot
    """
    ax_was_None = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(8, 6))
        ax_was_None = True

    # BDT outputs =======================================================
    decisions_dic = {}

    decisions_dic['train_sig'] = bdt.decision_function(
        X_train[y_train > 0.5])
    decisions_dic['train_bkg'] = bdt.decision_function(
        X_train[y_train < 0.5])
    decisions_dic['test_sig'] = bdt.decision_function(
        X_test[y_test > 0.5])
    decisions_dic['test_bkg'] = bdt.decision_function(
        X_test[y_test < 0.5])

    # Range of the full plot ============================================
    low = min(np.min(d) for d in decisions_dic.values())
    high = max(np.max(d) for d in decisions_dic.values())

    low_high = (low, high)

    # Histograms for training data =====================================
    ax.hist(decisions_dic['train_sig'],
            color=sig_color, alpha=0.5, range=low_high, bins=bins,
            histtype='stepfilled', density=True,
            label='S (train)')
    ax.hist(decisions_dic['train_bkg'],
            color=bkg_color, alpha=0.5, range=low_high, bins=bins,
            histtype='stepfilled', density=True,
            label='B (train)')

    # Histograms for test data =========================================
    hist, bins = np.histogram(decisions_dic['test_sig'],
                              bins=bins, range=low_high, density=True)
    scale = len(decisions_dic['train_sig']) / sum(hist)
    # Compute and rescale the error
    err = np.sqrt(hist * scale) / scale

    center = (bins[:-1] + bins[1:]) / 2
    ax.errorbar(center, hist, yerr=err, fmt='o', c=sig_color, label='S (test)')

    hist, bins = np.histogram(decisions_dic['test_bkg'],
                              bins=bins, range=low_high, density=True)
    # Compute and rescale the error
    scale = len(decisions_dic['test_bkg']) / sum(hist)
    err = np.sqrt(hist * scale) / scale

    ax.errorbar(center, hist, yerr=err, fmt='o', c=bkg_color, label='B (test)')

    # Labels =================================================================
    ax.set_xlabel("BDT output")
    ax.set_ylabel("Arbitrary units")

    ax.legend()
    lp.set_default_grid(ax)

    if ax_was_None:
        return fig, ax


def add_bdt_dataframe(
    dataframe: pd.DataFrame, bdt: AdaBoostClassifier,
    columns: List[str],
    bdt_name: str = "bdt"
):
    """Add the BDT output column to a dataframe under the label
    `'bdt'`

    Args:
        dataframe: Dataframe that contains the columns used for
            the training.
        bdt: trained BDT
        columns: list of columns used to train the BDT
        bdt_name: name of the branch where the BDT output is saved
    """
    dataframe[bdt_name] = bdt.decision_function(dataframe[columns])


def get_importances(
    bdt: AdaBoostClassifier, train_columns: List[str],
    verbose: bool = True
) -> Dict[str, float]:
    """Get the importances of the columns used in the
    training, sorted by importance.

    Args:
        bdt: Trained classifier such as Adaboost
        train_columns: list of column names the classifier
            has been trained with
        verbose: print the result

    Returns:
        dict_importances: dictionnary that associates
            a column name with its importance for the
            training
    """
    list_importances = bdt.feature_importances_

    # Sorting
    index_sort = np.argsort(list_importances)[::-1]
    train_columns_sorted = np.array(train_columns)[index_sort]
    list_importances_sorted = list_importances[index_sort]

    dict_importances = {}

    if verbose:
        print("COLUMNS SORTED BY IMPORTANCE")
        print(f"{'Column':<30} | Importance")
        print('-'*50)
    for column, importance in zip(train_columns_sorted, list_importances_sorted):
        if verbose:
            print(f"{column:<30} | {importance:.2f}")
        dict_importances[column] = importance

    return dict_importances
