""" Functions for clustering, mainly
for visualisation of results.
"""

from unicodedata import category
from project.defs import list_samples_columns

from lib.utils import array_like, try_makedirs
import lib.plot as lp
from typing import Union, List, Dict, Optional, Tuple
import itertools

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd

def get_edges_from_clust_labels(
    clust_labels:array_like
)->np.array:
    """ Get the histogram edges suited for the
    cluster labels given in `clu_labels`.

    Args: 
        clu_labels: list of labels given by a 
            clustering algorithm, for some
            dataframe.

    Returns:
        edges: bin edges for a histogram of
            `clu_labels`
    """
    clust_labels = np.unique(clust_labels)
    mini = min(clust_labels)
    maxi = max(clust_labels)
    nb = len(clust_labels)
    
    return np.linspace(mini - 0.5, maxi + 0.5, nb + 1, 
        endpoint=True
    )

def plot_result_clustering(
    dataframe: array_like,
    clu_label_name: str,
    clu_label_alias: str,
    cats:  List[str],
    dict_df_flags: Dict[str, str],
    dict_df_labels: Dict[str, str],
    dict_df_colors: Dict[str, str],
    **kwargs
)->Union[mpl.figure.Figure, mpl.axes.Axes]:
    """ Plot the result of the clustering, i.e., what is
    the clustering labels of the events, depending on the
    categories (`cats`, signal, noise or peaked) they are in.
    
    Args:
        dataframe: dataframe to which the clustering has
            been performed
        clu_label_name: name of the column that contains the
            cluster labels.
        clu_label_alias : label of the cluster labels used 
            in the plots            
        cats: categories of interest
        dict_df_flags: associates a category with its flag in
            the dataframe
        dict_df_labels: associates a category with its label
        dict_df_colors: associates a category with its color
        **kwargs: fed into :py:func:`lib.plot.plot_hist`
    
    Returns:
        fig, ax: figure and axis of the histogram
    """
    
    
    fig, ax = plt.subplots(figsize=(8, 6))

    edges = get_edges_from_clust_labels(dataframe[clu_label_name])
    for cat in cats:
        dataframe_cat = dataframe.query(
            f"{dict_df_flags[cat]}==True"
        )
        lp.plot_hist(
            dataframe_cat[clu_label_name],
            f"{clu_label_alias} category",
            label=dict_df_labels[cat],
            fc=(0, 0, 0, 0),
            edgecolor=dict_df_colors[cat],
            ax=ax,
            bins=edges,
            lw=3,
            **kwargs
        )

    ax.legend()

    return fig, ax

def plot_result_clustering_hist1d(
    dataframe: pd.DataFrame,
    features: List[str],
    column_labels: Dict[str, str],
    clu_label_name: str,
    correct_clu_label_cats: Dict[str, int],
    cats:  List[str],
    dict_df_flags: Dict[str, str],
    dict_df_labels: Dict[str, str],
    dict_color_clu_cats: Dict[str, Dict[bool, 'str']],
    **kwargs
)->Union[mpl.figure.Figure, mpl.axes.Axes]:
    """ Plot the result of the clustering, i.e., what is
    the clustering labels of the events, depending on the
    categories (`cats`, signal, noise or peaked) they are in.
    
    Args:
        dataframe: dataframe to which the clustering has
            been performed
        features: list of features used in the clustering
        column_labels: associates a feature with its label
        clu_label_name: name of the column that contains the
            cluster labels.
        correct_clu_label_cats: Associates a category with
            its correct cluster label.     
        cats: categories (`S`, `P`, `N`) of interest
        dict_df_flags: associates a category with its flag in
            the dataframe
            dict_df_labels: associates a category with its label
            dict_df_colors: associates a category with its color
        dict_color_clu_cats: associates a category with
            another dictionnary, whose keys are `True` (correct label)
            or `False` (wrong label). The associated value is
            the color to use.
        **kwargs: fed into :py:func:`lib.plot.plot_hist`
    
    Returns:
        fig, ax: figure and axis of the histogram
    """
    
    ## PLOT =========================================================
    fig, axes_ = plt.subplots(1, len(features), figsize=(8 * len(features), 6))
    axes = [axes_] if len(features)==1 else axes_
    clu_labels = np.unique(dataframe[clu_label_name])
    
    
    for ifeat, feature in enumerate(features):
        ax = axes[ifeat]
        min_max = [dataframe[feature].min(), dataframe[feature].max()]
                        
        for cat in cats:
            dataframe_cat = dataframe.query(f"{dict_df_flags[cat]}")
            correct_clu_label = correct_clu_label_cats[cat]
            
            dict_df_cat = {
                True  : dataframe_cat.query(
                    f"{clu_label_name} == {correct_clu_label}"
                    ),
                False : dataframe_cat.query(
                    f"{clu_label_name} != {correct_clu_label}"
                    ),
            }

            for is_correct, df in dict_df_cat.items():
                adjective = "correct" if is_correct else "wrong"
                color = dict_color_clu_cats[cat][is_correct]
                
                style = {}
                if is_correct:
                    style = {
                        "alpha": 0.5,
                        "color": color,
                    }
                
                else:
                    style = {
                        "fc": (0, 0, 0, 0),
                        "edgecolor": color
                    }

                lp.plot_hist(
                    df[feature],
                    label_var=column_labels[feature],
                    label=f"{dict_df_labels[cat]} in {adjective} cluster",
                    ax=ax,
                    min_max=min_max,
                    **style,
                    **kwargs
                )
        
        ax.legend(fontsize=12)

    return fig, axes_


def plot_result_clustering_hist2d(
    dataframe: pd.DataFrame, 
    features: List[str],
    column_labels: Dict[str, str],
    clu_label_name: str,
    correct_clu_label_cats: Dict[str, int],
    cats:  List[str],
    dict_df_flags: Dict[str, str],
    dict_df_labels: Dict[str, str],
    dict_color_clu_cats: Dict[str, Dict[bool, 'str']],
    **kwargs
    ):
    """ Plot the super-imposed and not super-imposed
    2d histograms of `name_var1` and `name_var2`
    for the various categories listed in `cats`.

    Args:
        dataframe: contains the columns `name_var1` 
            and `name_var2` and the flags corresponding
            to the categories in `cats`.
        features: list of features used in the clustering
        column_labels: associates a feature with its label
        clu_label_name: name of the column that contains the
            cluster labels.
        correct_clu_label_cats: Associates a category with
            its correct cluster label.  
        cats: categories of interest   
        dict_df_flags: associates a category with its flag in
            the dataframe
        dict_df_labels: associates a category with its label
        dict_color_clu_cats: associates a category with
            another dictionnary, whose keys are `True` (correct label)
            or `False` (wrong label). The associated value is
            the color to use.
        **kwargs: fed into :py:func:`lib.plot.multi_plot_hist2d`
    """

    for name_var1, name_var2 in itertools.combinations(features, 2):
        label_var1 = column_labels[name_var1]
        label_var2 = column_labels[name_var2]

        list_var1, list_var2 = [], []
        list_labels = []
        list_colors = []

        for cat in cats:
            df_cat = dataframe.query(f"{dict_df_flags[cat]} == True")
            label_cat = dict_df_labels[cat]
            correct_clu_label = correct_clu_label_cats[cat]
            
            dict_df_cat = {
                True  : df_cat.query(
                    f"{clu_label_name} == {correct_clu_label}"
                    ),
                False : df_cat.query(
                    f"{clu_label_name} != {correct_clu_label}"
                    ),
            }

            for is_correct, df in dict_df_cat.items():
                if len(df)!=0:
                    adjective = "correct" if is_correct else "wrong"
                    label = f"{label_cat} in {adjective} cluster"
                    list_var1.append(df[name_var1])
                    list_var2.append(df[name_var2])
                    list_labels.append(label)
                    list_colors.append(dict_color_clu_cats[cat][is_correct])

                else:
                    print(f"No events for {label}")

        lp.multi_plot_hist2d(
            list_var1, list_var2,
            label_var1, label_var2,
            list_labels,    
            one_ax=True,
            list_colors=list_colors,
            **kwargs
        )

        lp.multi_plot_hist2d(
            list_var1, list_var2,
            label_var1, label_var2,
            list_labels,    
            one_ax=False,
            list_colors=list_colors,
            **kwargs
        )


list_colors_default = [
    'grey', 'purple', 'blue',
    'orange', 'red', 'green'
]

def plot_clusters_hist2d(
    dataframe: pd.DataFrame, 
    features: List[str],
    column_labels: Dict[str, str],
    clu_label_name: str,
    dict_color_clu_cats: Dict[str, str] = None,
    column_ranges: Optional[Dict[str, List[float]]] = None,
    column_nbins: Optional[Dict[str, int]] = None,
    category_name: str = "Cluster",
    category_names: Optional[Dict[int, str]] = None,
    to_plot: Tuple[str] = ("one_ax", "multi_ax"),
    path: Optional[str] = None,
    name: Optional[str] = None,
    return_figs: bool = False,
    **kwargs
    )-> List[mpl.figure.Figure]:
    """ Plot the super-imposed and not super-imposed
    2d histograms of `name_var1` and `name_var2`
    for various clusters

    Args:
        dataframe: contains the columns `name_var1` 
            and `name_var2` and the flags corresponding
            to the categories in `cats`.
        features: list of features used in the clustering
        column_labels: associates a feature with its label
        clu_label_name: name of the column that contains the
            cluster labels.
        dict_color_clu_cats: Associates a category with
            a color
        category_name: name of the categories
        category_names: associates a cluster label with a name
            (a string)
        to_plot: whether to plot all the clusters in one `ax` 
            or to have one ax for each cluster (`multi_ax`)
        path, name: path and name of the figures to save.
            If not given, the figure is not saved.
        return_figs: whether to return the figures
        **kwargs: fed into :py:func:`lib.plot.multi_plot_hist2d`

    Returns:
        dict_figs : associates "{name_var1}_{name_var2}"
            with its two figures 
            (one with all the clusters in one axis,
            the other with one axis for each cluster)
    """

    list_clu_labels = np.unique(dataframe[clu_label_name])

    if dict_color_clu_cats is None:
        assert len(list_clu_labels) <= len(list_colors_default), \
            "Please specify a list of colors"
        dict_color_clu_cats = {
            list_clu_labels[i]: list_colors_default[i]
            for i in range(len(list_clu_labels))
        }

    dict_figs = {}

    for name_var1, name_var2 in itertools.combinations(features, 2):
        label_var1 = column_labels[name_var1]
        label_var2 = column_labels[name_var2]

        list_var1, list_var2 = [], []
        list_labels = []
        list_colors = []

        for clu_label in list_clu_labels:
            df_clu_label = dataframe.query(
                f"{clu_label_name} == {clu_label}"
            )

            list_var1.append(df_clu_label[name_var1])
            list_var2.append(df_clu_label[name_var2])
                        
            if category_names is not None:
                label = category_names.get(
                    clu_label,
                    label
                )
            label = f"{category_name} {clu_label}"

            list_labels.append(label)
            list_colors.append(dict_color_clu_cats[clu_label])
        
        ## BINNING -------------------------
        min_max = None
        nbins = 50


        if column_ranges is not None:
            min_max1 = column_ranges.get(name_var1)
            min_max2 = column_ranges.get(name_var2)

            min_max = [min_max1, min_max2]

        if column_nbins is not None:
            default_nbins = column_nbins.get("default", nbins)
            nbins1 = column_nbins.get(name_var1, default_nbins)
            nbins2 = column_nbins.get(name_var2, default_nbins)

            nbins = [nbins1, nbins2]

        ## FIGURES -------------------------
        if return_figs:
            dict_figs[f"{name_var1}_{name_var2}"] = []
        if "one_ax" in to_plot:
            res = lp.multi_plot_hist2d(
                list_var1, list_var2,
                label_var1, label_var2,
                list_labels, 
                list_raw_labels=list_clu_labels,   
                one_ax=True,
                list_colors=list_colors,
                min_max=min_max,
                bins=nbins,
                name=f"{name}_{name_var1}_{name_var2}" if name is not None else None,
                path=path if name is not None else None,
                return_fig=return_figs,
                **kwargs
            )
            if return_figs:
                dict_figs[f"{name_var1}_{name_var2}"].append(res[0])              
            
        if "multi_ax" in to_plot:
            res = lp.multi_plot_hist2d(
                list_var1, list_var2,
                label_var1, label_var2,
                list_labels, 
                list_raw_labels=list_clu_labels,      
                one_ax=False,
                list_colors=list_colors,
                min_max=min_max,
                bins=nbins,
                path=path,
                name=f"{name}_{name_var1}_{name_var2}" if name is not None else None,
                return_fig=return_figs,
                **kwargs
            )
            if return_figs:
                dict_figs[f"{name_var1}_{name_var2}"].append(res[0]) 
            
    if return_figs:
        return dict_figs


def plot_samples_clusters(
    dataframe: pd.DataFrame,
    clu_label_name: str,
    seed: Optional[int] = None,
    dict_color_clu_labels : Dict[str, str] = {},
    method: str = "random",
    title: str = "Cluster {cluster_label}",
    one_fig: bool = False,
    name: str = None,
    path: str = None,
    **kwargs
) -> Union[mpl.figure.Figure, mpl.axes.Axes]:
    """ 
    Plot one sample for each cluster,
    as an illustration of the clustering process.

    Args:
        dataframe: dataframe that contains
            the samples and the cluster labels
            under the name `clu_label_name`
        clu_label_name: name of the column that
            contains the cluster labels in
            the dataframe
        seed : seed to randomly choose an event from
            the dataframe
        dict_color_clu_cats : Associates a cluster label 
            with a color for the plot
        method: "random", "average" or "sum"
        one_fig: whether to plot everything in the same figure,
            or to plot them in separate figures
        name, path: path and name of the file 
        title: title of the figures
        **kwargs: passed to :py:func:`lib.plot.plot_event`
    
    Returns:
        fig, axes: figure and axes of the plots
    """
    list_clusters = np.unique(dataframe[clu_label_name])
    nb_clusters = len(list_clusters)
    if one_fig:
        fig, origin_axes, axes = lp.get_multi_fig_ax(
            nb_clusters
        )

    for i_cluster, cluster_label in enumerate(list_clusters):
        if not one_fig:
            fig, ax = plt.subplots(figsize=(8, 6))
        else:
            ax = axes[i_cluster]

        dataframe_cluster = dataframe.query(
            f"{clu_label_name} == {cluster_label}"
        )
        if method == "average":
            dataframe_cluster_one_event = np.mean(
                dataframe_cluster[list_samples_columns], axis=0
            )
        elif method == "sum":
            dataframe_cluster_one_event = np.sum(
                dataframe_cluster[list_samples_columns], axis=0
            )
        elif method == "random": 
            dataframe_cluster_one_event = dataframe_cluster.sample(
                1, random_state=seed
            ).iloc[0]
        
        lp.plot_event(
            dataframe_cluster_one_event - \
                dataframe_cluster_one_event['s0'], 
            ax=ax,
            color=dict_color_clu_labels.get(cluster_label),
            **kwargs
        )

        if title is not None:
            ax.set_title(
                title.format(cluster_label=cluster_label)
            )
        
        if not one_fig and name is not None:
            lp.save_fig(
                fig, path=path, name=f"{name}_{cluster_label}"
            )
    if one_fig and name is not None:
        lp.save_fig(
            fig, path=path, name=name
        )
    if one_fig:
        plt.tight_layout()
        return fig, origin_axes   