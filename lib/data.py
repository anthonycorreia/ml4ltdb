""" Module that contains functions to 
deal with dataframes.
"""

from typing import List, Dict, Optional

import pandas as pd
import numpy as np
from scipy.stats import wasserstein_distance

#######################################################
##################### COMPUTATION #####################
#######################################################

def compute_vals(
    dataframe: pd.DataFrame, formula_columns: Dict[str, str]
) -> pd.DataFrame:
    """If the columns in `columns` are not in the
    dataframe, compute them and add them to it.

    Some columns are computed without `formula_columns`.

    * `min_std_pos_neg = min(std_pos, std_neg)`
    * `max_std_pos_neg = max(std_pos, std_neg)`
    * `mean_m_pedestal_red = (mean - pedestal) / std`
    * `rounded_mean = round(mean)`

    Args:
        dataframe: dataframe
        formula_columns: associates a name of column
            with its formula.

    Returns:
        dataframe: dataframe with the new variables
            computed
    """

    for column, formula in formula_columns.items():
        if column == "min_std_pos_neg":
            dataframe["min_std_pos_neg"] = \
                dataframe[["std_pos", "std_neg"]].min(1)
        elif column == "max_std_pos_neg":
            dataframe["max_std_pos_neg"] = \
                dataframe[["std_pos", "std_neg"]].max(1)
        elif column == "max_std_pos_neg_m_min_std_pos_neg":
            dataframe["max_std_pos_neg_m_min_std_pos_neg"] = \
                dataframe["max_std_pos_neg"] - dataframe["min_std_pos_neg"]
        elif column == "std_d_max_m_min":
            dataframe["std_d_max_m_min"] = \
                dataframe["std"] / dataframe["max_m_min"]
        elif column == "relative_mean":
            dataframe["relative_mean"] = \
                (dataframe["mean"] - dataframe["min"]) / \
                    (dataframe["max"] - dataframe["min"]) - 0.5
        elif column == "std_period_d_mean_period":
            dataframe["std_period_d_mean_period"] = \
                dataframe["std_period"] / dataframe["mean_period"]
        elif column.endswith("_rounded"):
            dataframe[column] = \
                dataframe[column.replace('_rounded', '')].round()
        # elif column == "mean_m_pedestal_red":
        #     dataframe["mean_m_pedestal_red"] = \
        #         dataframe["mean_m_pedestal"] / dataframe['std']
        else:
            dataframe[column] = dataframe.eval(formula)

    return dataframe


def compute_distance_columns(
    dataframe1: pd.DataFrame,
    dataframe2: pd.DataFrame,
    columns: List[str],
    verbose: bool = True,
    **kwargs
) -> Dict[str, float]:
    """Compute the Wasserstein distance between the signal
    and background distributions of various columns.
    :py:func:`scipy.stats.wasserstein_distance` is used.

    Args:
        dataframe1, dataframe2: two dataframes
        columns: list of columns
        verbose: if the result is printed
        **kwargs: passed to 
            :py:func:`scipy.stats.wasserstein_distance`

    Returns:
        sorted_distance_dict: associated a column name with
            the distance of this column between the
            two dataframes. The dictionnary is sorted
            by decreasing distance.
    """
    distance_dict = {}

    for column in columns:
        distance_dict[column] = wasserstein_distance(
            dataframe1[column],
            dataframe2[column],
            **kwargs
        )

    sorted_distance_dict = dict(
        sorted(distance_dict.items(), key=lambda item: -item[1]))
    if verbose:
        print("Columns sorted from higher to lower distances")
        for column in sorted_distance_dict:
            print(column, '-'*5)
            print(distance_dict[column])

    return sorted_distance_dict

#######################################################
################### TRANSFORMATION ####################
#######################################################

def get_1D_to_2D(array):
    """ Transforme a 1D array into
    a 2D array with one column.

    args:
        array: a 1D array
    
    returns:
        array_2D: a 2D array of
            1 column, that is `array`
    """
    array = np.asarray(array)
    if len(np.shape(array)) == 1:
        return array.reshape(len(array), 1)
    
    return array


def cut_dataframe(
    dataframe: pd.DataFrame, cut: str, 
    inplace: bool = True,
    verbose: bool = True
)->Optional[pd.DataFrame]:
    """ Perform a cut on a dataframe

    args:
        dataframe: dataframe that undergoes the cut
        cut: the cut
        inplace: whether the function is inplace
        verbose: print the cut efficiency
    
    Returns:
        cut_dataframe: if not inplace.
    """

    len_before = len(dataframe)
    df_return = dataframe.query(
        cut, inplace=inplace
    )
    len_after = len(dataframe)
    if verbose:
        if len_before!=0:
            print(
                f"Cut {cut} -> efficiency {(len_before - len_after)/len_before}"
            )
        else:
            print("Initially no events in the dataframe")

    return df_return
