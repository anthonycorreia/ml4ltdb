"""
PLOT HISTOGRAMS
Author: Anthony Correia
Date: 06 Jan. 2021

- Plot 1D and 2D histograms
- Save figures
"""

# ============================= LIBRARIES =============================

# Types
from typing import Union, Optional, Tuple, List, Dict
# Files
import os.path as op
from copy import deepcopy

# Matplotlib
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.colors import LogNorm, Normalize, LinearSegmentedColormap
import matplotlib.patches as mpatches
from matplotlib.backends.backend_pdf import PdfPages

# Data
import pandas as pd
import numpy as np
from math import ceil

# Project
from project import loc
from .utils import array_like, try_makedirs, select_elem_dict


# Matplotlib style ---------------------------------------------------

from matplotlib import rcParams

style = {
    'font.family': 'serif',
    'font.serif': 'DejaVu Serif',
    'legend.fontsize': 20,
    'xtick.labelsize': 20,
    'ytick.labelsize': 20,
    'axes.labelsize': 25,
    'text.latex.preamble': r'\usepackage{amsmath}',
    'text.usetex': True,  # A PASSER A `False` POUR DESACTIVER LATEX
    'font.size': 20
}
rcParams.update(style)

# =============================  LABELS  =============================


def _latex_format(text: str):
    """Replace `_` by `\\_` in a string to avoid latex errors
    with matplotlib

    Args:
        text: string to correct

    Returns:
        corrected string
    """
    # to avoid latex errors when I want a label like 'B0_M'
    return text.replace(
        '_', '\\_')


def get_label(column: str, column_labels: Optional[Dict[str, str]] = None) -> str:
    """ If `column` is in `column_labels`, the label is returned.
    Otherwise, `column` is returned.

    Args:
        column: Name of the column of a dataframe
        column_labels: associate a column name with an alias,
            used in the plots

    Returns:
        label of the column
    """
    if column_labels is None:
        column_labels = {}

    if column in column_labels:
        return column_labels[column]
    else:
        return _latex_format(column)


# ============================= SAVE FIG =============================

def save_fig(
        fig: mpl.figure.Figure,
        path: Optional[str] = None, name: Optional[str] = None,
        exts: Optional[Union[str, List[str]]] = None,
        show: bool = True):
    """ Save `fig` in `{lib.loc.out_folder}/{path}/`,
    under the name `name`, only if `name` is specified.
    ``
    Args:
        fig : figure to save
        path: path where to save the picture
        name: name of the file to save
        exts: extension of the files (png, pdf, ...)

        show: the figure is shown
    """

    if exts is None:
        exts = ['png', 'pdf']

    # Match the borders of the images with the borders of the object
    fig.tight_layout()

    if name is not None:
        if path is not None:
            path = op.join(loc.images_folder, path)
            # Create the folders that don't exist yet
            try_makedirs(path)

        # Save in pdf and png
        if isinstance(exts, str):
            exts = [exts]

        for ext in exts:
            name_ext = f"{name}.{ext}"
            if path is not None:
                full_path = op.join(path, name_ext)
            else:
                full_path = name_ext

            print(f"Fig saved in {full_path}")

            fig.savefig(full_path, dpi=300)

        if not show:
            plt.close(fig)


# =============================    TOOLS   =============================

def set_default_grid(ax: mpl.axes.Axes):
    """ Set the default grid (grey)

    Args:
        matplotlib object where to plot
    """
    ax.grid(axis='both', alpha=0.2, color='grey')


def get_min_max_1d(
    var: Union[array_like, List[array_like]],
    min_max_default: Tuple[float, float] = None
)-> Tuple[float, float]:
    """ Obtain the min and max of an array, only if
    not already given in the tuple
    `min_max_default`

    Args:
        `var` : array or list of arrays
        `min_max_default` : default values to use.
            The min and max are computed only
            if not already specified in this tuple.

    Returns:
        min_max: tuple (min, max) of `var`
    """
    if min_max_default is None:
        min_max_default = [None, None]


    if min_max_default[0] is None:
        if isinstance(var, list) or isinstance(var, tuple):
            mini = min(min(var_e) for var_e in var)
        else:
            mini = min(var)
    else:
        mini = min_max_default[0]
    if min_max_default[1] is None:
        if isinstance(var, list) or isinstance(var, tuple):
            maxi = max(max(var_e) for var_e in var)
        else:
            maxi = max(var)
    else:
        maxi = min_max_default[1]

    return mini, maxi

def get_multi_fig_ax(
    nb_axes: int,
    max_ncols: int = 3
):
    """ Get the  fig and axes given the
    number of figures to plot and the maximum
    number of columns in the grid of figures.

    Args:
        nb_axes: number of axes required
        max_ncols: number of columns (max)

    Returns:
        fig: figure
        origin_axes: axes before flatting,
            as obtained by :py:func:`plt.subplots`
        axes: axes after flatting
    """
    ncols = min(max_ncols, nb_axes)
    nrows = ceil(nb_axes / ncols)

    fig, origin_axes = plt.subplots(
        ncols=ncols,
        nrows=nrows,
        figsize=(8*ncols, 6*nrows)
    )

    if (ncols == 1) and (nrows == 1):
        axes = [origin_axes]
    else:
        axes = origin_axes.flat

    for iax in range(nb_axes, ncols * nrows):
        plt.delaxes(axes[iax])

    return fig, origin_axes, axes



# ============================= PLOT HISTS =============================

def get_bins_1D(bins, min_max):
    """ Get the number of bins 
    """
    if isinstance(bins, str) and bins == "discretised":
        bins = int(min_max[1] - min_max[0] + 1)
    return bins

def get_bins_2D(bins, min_max):
    """ Get the number of bins 
    """
    if isinstance(bins, str) and bins == "discretised":
        bins = ["discretised", "discretised"]
    if isinstance(bins, list) or isinstance(bins, tuple):
        bins = deepcopy(bins)
        if len(bins) == 2:
            for i in range(2):
                if isinstance(bins[i], str) and bins[i] == "discretised":
                    bins[i] = int((min_max[i][1] - min_max[i][0]) + 1)

    return bins


def plot_hist(var: array_like, label_var: str, ax: mpl.axes.Axes = None,
              bins: Union[int, array_like, str] = 50,
              min_max: Tuple[float, float] = None,
              logscale: bool = False,
              normalise: bool = False,
              with_err=False,
              **kwargs
              ) -> Optional[Union[mpl.figure.Figure, mpl.axes.Axes]]:
    """ Plot the histogram of a variable.

    Args:
        var (array-like): series of data to plot
        label_var: Label of the x-axis
        bins: bins, or number of bins, or `discretised`
        min_max: (min, max) of the x-axis
        ax: matplotlib object where to plot
        logscale: is the y-axis in log scale?
        normalise: whether to divide the histograms
            by the number of events
        with_err: compute the poisson errorin each bin
        **kwargs: other parameters, passed to the plot function

    Returns:
        fig, ax: figure and axis (if `ax` was `None`)
    """
    # Fig, ax ------------------------------------
    if ax is None:
        ax_was_None = True
        fig, ax = plt.subplots(figsize=(8, 6))
    else:
        ax_was_None = False

    # Histogram -----------------------------------
    min_max = get_min_max_1d(var, min_max)
    bins = get_bins_1D(bins, min_max)

    hist, bin_edges = np.histogram(
        var,
        bins=bins,
        range=min_max
    )

    if with_err:
        hist_err = np.sqrt(hist)

    if normalise:
        hist = hist / hist.sum()
        if with_err:
            hist_err = hist_err / hist.sum()

    # Plot --------------------------------------
    ax.bar(
        (bin_edges[1:] + bin_edges[:-1])/2, hist,
        align='center',
        width=bin_edges[1] - bin_edges[0],
        yerr=hist_err if with_err else None,
        **kwargs
    )

    # Labels -----------------------------------
    ax.set_xlabel(label_var)
    ax.set_ylabel("Number of events")

    # Grid -------------------------------------
    set_default_grid(ax)

    # Log scale
    if logscale:
        ax.set_yscale('log')

    # Return -----------------------------------
    if ax_was_None:
        return fig, ax


def plot_hist2d(
    var1: array_like, var2: array_like,
    label_var1: str, label_var2: str,
    bins=50,
    min_max=None,
    fig: mpl.figure.Figure = None,
    ax: mpl.axes.Axes = None,
    logscale: bool = False,
    **kwargs
) -> Optional[Union[mpl.figure.Figure, mpl.axes.Axes]]:
    """
    Plot a 2D histogram.

    Args:
        var1, var2: variables to plot
        label_var1, label_var2: x and y labels
        bins: passed to :py:func:`matplotlib.pyploy.hist2d`
        min_max: passed to :py:func:`matplotlib.pyploy.hist2d`
            as `range`
        fig, ax: existing figure and ax where to plot
        logscale: Colorbar in log scale?
        **kwargs: other parameters fed into
            :py:func:`matplotlib.pyploy.hist2d`

    Returns:
        fig, ax: figure and axis (if `ax` was `None`)
    """

    # Fig and ax
    if ax is None:
        ax_was_None = True
        fig, ax = plt.subplots(figsize=(8, 6))
    else:
        ax_was_None = False

    norm = None

    # Histogram
    if logscale:
        norm = LogNorm()

    if min_max is None:
        min_max = [None, None]
    min_max = (
        get_min_max_1d(var1, min_max[0]),
        get_min_max_1d(var2, min_max[1]),
    )
   
    bins = get_bins_2D(bins, min_max)

    _, _, _, im = ax.hist2d(
        var1, var2, range=min_max,
        bins=bins, norm=norm,
        **kwargs)
    
    # Labels
    ax.set_xlabel(label_var1)
    ax.set_ylabel(label_var2)

    # Grid
    set_default_grid(ax)

    # Colorbar
    cbar = fig.colorbar(im, ax=ax)
    cbar.set_label("Number of events")

    # Return
    if ax_was_None:
        return fig, ax

def multi_plot_hist(
    list_df: List[pd.DataFrame],
    columns: List[str],
    column_labels: dict = None,
    range_columns: dict = None,
    nbins: Union[int, Dict[str, int]] = 40,
    list_labels: Optional[List[str]] = None,
    list_colors: Optional[List[str]] = None,
    logscale: bool = False,
    normalise: bool = True,
    **kwargs
) -> Union[mpl.figure.Figure, mpl.axes.Axes]:
    """ Plot 1D histograms of several variables in different
    subplots spread across a grid.

    Args:
        list_df: list of dataframe
        columns: list of the name of the columns to plot
        column_labels: associates a name of column with its alias
            to use in the figure.
        range_columns (dict, optional): associates a variable
            with the desired range
        nbins: number of bins to used in all the plots. Can also
            be a dictionnary that associates a column with its
            number of bins. In this case, the `"default"` key
            allow to give the number of bins if one column is
            not in the dataframe.
        list_labels: list of labels of the dataframe in
            `list_df` for the legend
        list_df_colors: list of plot colors of the dataframe in
            `list_df`
        logscale: if a y log scale is used for all the plots
        normalise: if the plots are normalised
        **kwargs: fed into the `ax.hist` functions.

    Returns:
        fig, axes: Figure and axes of the figure
    """
    if column_labels is None:
        column_labels = {}
    if range_columns is None:
        range_columns = {}
    if list_colors is None:
        list_colors = ['g', 'r', 'darkorange']


    # Axes ====================================
    fig, origin_axes, axes = get_multi_fig_ax(
        nb_axes=len(columns)
    )

    # Figures ===============================
    for i_ax, column in enumerate(columns):

        # Variables depending on i
        ax = axes[i_ax]

        # Range
        range_column = range_columns.get(column)
        if range_column is None:
            global_min = np.min([np.min(df[column]) for df in list_df])
            global_max = np.max([np.max(df[column]) for df in list_df])
            range_column = [global_min, global_max]

        # Histograms
        for i_df, df in enumerate(list_df):
            if list_labels is not None:
                label = list_labels[i_df]
            else:
                label = None

            ax.hist(
                df[column],
                label=label,
                color=list_colors[i_df],
                range=range_column,
                bins=select_elem_dict(nbins, column),
                density=normalise,
                alpha=0.5,
                **kwargs
            )

        # Labels
        ax.set_xlabel(get_label(column, column_labels))
        ax.set_ylabel("Number of events")

        # Grid
        set_default_grid(ax)

        # Log scale
        if logscale:
            ax.set_yscale('log')

        if i_ax == 0 and list_labels is not None:
            ax.legend()

    return fig, origin_axes

def multi_plot_hist_same_col(
    list_df: List[pd.DataFrame],
    column: str,
    column_label: str,
    list_labels: List[str],
    min_max: Tuple[float, float]=None,
    bins: Union[int, array_like] = 40,
    list_colors: Optional[Union[str, List[str]]] = None,
    logscale: bool = False,
    normalise: bool = True,
    with_err: Union[List[bool], bool] = False,    
    bin_width_scale: float = 1.,
    method: str = "multi_ax", # "one_ax", "multi_ax", "one_ax_stacked", "one_ax_stacked_norm"
    max_ncols: int = 3,
    return_bins: bool = False,
    axes: Optional[mpl.axes.Axes] = None,
    **kwargs
):
    """ Plot the same column for different dataframes.

    Args:
        list_df: list of dataframes
        column, column_label : column to plot and its (latex) label
        min_max: min and max values
        bins: number of bins or bin edges
        list_labels, list_colors: label and color
            for each dataframe in `list_df`
        logscale: whether the y-axis is in logscale
        normalise: whether the histogram of each dataframe
            Us normalised to sum up to 1
        with_err: plot the error bars as well
        method: Method of plotting:

            * `one_ax` : one ax for all the histograms.
            * `multi_ax` : one ax for each histogram
            * `one_ax_stacked` : one ax, each histogram is stacked
                on top of the others
            * `one_ax_stacked_norm` : `one_ax_stacked` and each bin
                is normalised to 1.
        max_ncols: Maximum number of columns. Only if `method` is
            `multi_max`
        bin_width_scale: width of bins in the plot, w.r.t. the one of the
            histogram
        return_bins : Returns the bin edges
        **kwargs: passed to the `ax.bar` function
    """

    if list_colors is None:
        list_colors = ['g', 'r', 'darkorange', 'b']

    ## Formatting ===========================================
    if axes is not None:
        axes_was_None = False
    else:
        if method.startswith("one_ax"):
            fig, axes = plt.subplots(
                figsize=(12, 6)
            )
            origin_axes = axes
        elif method.startswith("multi_ax"):
            fig, origin_axes, axes = get_multi_fig_ax(
                nb_axes=len(list_df),
                max_ncols=max_ncols
            )
        axes_was_None = True

    if isinstance(list_colors, str):
        list_colors = [list_colors] * len(list_df)

    
    if isinstance(bins, str) and bins == "discretised":
        min_max = get_min_max_1d(
            [df[column] for df in list_df], 
            min_max
        )
        bins = int(min_max[1] - min_max[0] + 1)

    ## Computing the histograms =============================
    # Histograms
    hists = [None] * len(list_df)

    if with_err:
        hists_err = [None] * len(list_df)
        if isinstance(with_err, bool):
            with_err = [True] * len(list_df)
    else:
        with_err = [False] * len(list_df)
    
    for i, df in enumerate(list_df):
        hists[i], bin_edges = np.histogram(
            df[column],
            bins=bins,
            range=min_max
        )
        if with_err[i]:
            hists_err[i] = np.sqrt(hists[i])
        if normalise:
            hist_sum = hists[i].sum()
            if hist_sum != 0:
                hists[i] = hists[i] / hist_sum # Normalised
                if with_err[i]:
                    hists_err[i] =  hists_err[i] / hist_sum

    bin_centres = (bin_edges[1:] + bin_edges[:-1])/2
    bin_width = bin_edges[1] - bin_edges[0]

    if method == "one_ax_stacked_norm":
        sum_hist = sum([hist for hist in hists])

    ## Plot the histograms ==================================
    
    for i in range(len(list_df)):
        # Axis ---------------------------------------
        if method=="multi_ax":
            ax = axes[i]
        else:
            ax = axes

        # Histogram to plot ---------------------------
        if method == "multi_ax" or method == "one_ax":
            plotted_hist = hists[i]
            if with_err[i]:
                plotted_hist_err = hists_err[i]
        elif method == "one_ax_stacked":
            plotted_hist = sum([hist for hist in hists[i:]])
            if with_err[i]:
                plotted_hist_err = hists_err[i]
        elif method == "one_ax_stacked_norm":
            plotted_hist = sum([hist for hist in hists[i:]]) / sum_hist
            if with_err[i]:
                plotted_hist_err = hists_err[i] / sum_hist

        # Plot -----------------------------------------

        ax.bar(
            bin_centres, plotted_hist,
            label=list_labels[i],
            align='center',
            width=bin_width * bin_width_scale,
            color=list_colors[i],
            **kwargs
        )

        if with_err[i]:
            ax.errorbar(
                bin_centres,
                plotted_hist, plotted_hist_err,
                linestyle='',
                lw=2, capsize=5, capthick=2,
                color=list_colors[i]
            )

        ax.set_xlabel(column_label)

        if logscale:
            ax.set_yscale("log")

        set_default_grid(ax)

        # Title ----------------------------------------
        if method == "multi_ax":
            ax.set_title(list_labels[i])

        # y-label --------------------------------------

        if method != "one_ax_stacked_norm":
            if normalise:
                ax.set_ylabel("Normalised number of events")
            else:
                ax.set_ylabel("Number of events")

    if method != "multi_ax":
        if method=="one_ax_stacked_norm":
            ax.legend(loc='upper left', bbox_to_anchor=(1., 1.))
        else:
            ax.legend()

    if method=="one_ax_stacked_norm":
        if normalise:
            ax.set_ylabel("Proportion of events")
        else:
            ax.set_ylabel("Proportion of events")

    plt.tight_layout()

    if return_bins:
        if axes_was_None:
            return fig, origin_axes, bin_edges
        else:
            return bin_edges
    else:
        if axes_was_None:
            return fig, origin_axes

color_to_cmap = {
    # 'grey': 'Greys',
    # 'red': 'Reds',
    # 'r': 'Reds',
    # 'blue': 'Blues',
    # 'b': 'Blues',
    # 'green': 'Greens',
    # 'g': 'Greens',
    # 'purple': 'Purples',
    # 'orange': 'Oranges',
    # 'o': 'Oranges'
}

def multi_plot_hist2d(
    list_var1:List[array_like], list_var2:List[array_like],
    label_var1: str, label_var2: str,
    list_labels: List[str],
    list_raw_labels: List[str] = None,
    vmin:Optional[float] = None,
    min_max: Optional[List[List[float]]] = None, bins: int = 50,
    normalise: bool = False, logscale: bool = False,
    one_ax: bool = True,
    max_ncols: int = 3,
    axes: Optional[Union[mpl.axes.Axes, List[mpl.axes.Axes]]] = None,
    fig: Optional[mpl.figure.Figure] = None,
    list_colors: Optional[List[str]] = None,
    path: Optional[str] = None, name : Optional[str] = None,
    return_fig: bool = False,
    **kwargs
)->Union[mpl.figure.Figure, mpl.axes.Axes]:
    """

    Args:
        list_var1, list_var2: list of array-like
            for the variable 1 and 2.
        label_var1, label_var2: labels of the variables 1 and 2
        list_labels: list of name of the categories in `list_var1`
            and `list_var2`
        vmin: lowest number of events
        min_max: Range of the plot, in the format
            `[[min_var1, max_var2], [min_var2, max_var2]]`
        bins : number of bins
        normalise: whether the distribution are normalised
            (`bin_count / sample_count / bin_area`)
        logscale: whether the colorbar is in logscale
        one_ax: whether all the histograms are super-imposed
        max_ncols: only of several axes, maximal number of
            columns in the grid of axes.
        fig, axes : figure and axis where to the histogram(s)
            is(are) plotted. `axes` must be a list of axes
            if the histograms are superimposed (`one_ax` is `False`),
            or just an `ax` otherwise.
        list_colors: list of colors corresponding to colorbars
        path, name: path and name where to save the figure(s)
        return_fig: whether to return the figure at the end
            (e.g., to save it)
        **kwargs: associates an argument `ax.imshow` with either an
            argument, directly fed into ax.imshow, or a list of
            arguments: the element $i$ is fed into `ax.imshow`
            for the ith category in `list_var1` and `list_var2`.

    Returns:
        fig, axes: Figure and axes of the figure
            (if not fed via the arguments)
    """
    if list_raw_labels is None and name is not None and not one_ax:
        list_raw_labels = np.arange(0, len(list_var1))

    ## Colors ----------------------------------------------
    if list_colors is not None:
        cmap = []
        for color in list_colors:
            if color in color_to_cmap:
                cmap.append(color_to_cmap[color])
            else:
                cmap.append(
                    LinearSegmentedColormap.from_list(
                        'Custom',
                        (mpl.colors.to_rgba(color, alpha=0.1), mpl.colors.to_rgba(color, alpha=1.)),
                        # ((1, 1, 1, 1), color),
                        256
                    )
                )

    ## Compute ranges --------------------------------------
    if min_max is None:
        min_max = [None, None]
    min_max = (
        get_min_max_1d(list_var1, min_max[0]),
        get_min_max_1d(list_var2, min_max[1]),
    )

    extent = [min_max[0][0], min_max[0][1], min_max[1][0], min_max[1][1]]

    bins = get_bins_2D(bins, min_max)

    ## First loop to compute the histograms ---------------
    histograms = []
    for var1, var2 in zip(list_var1, list_var2):

        H, _, _ = np.histogram2d(
            var1, var2,
            bins=bins,
            range=min_max,
            density=normalise
        )
        histograms.append(H)

    # colorbar range
    vmax = np.max(histograms)
    if vmin is None:
        if normalise:
            vmin = np.min(histograms)
        else:
            vmin = 1

    norm = mpl.colors.Normalize(vmin=-1, vmax=1)
    # Scale
    if logscale:
        norm = LogNorm(vmin=vmin, vmax=vmax)
    else:
        norm = Normalize(vmin=vmin, vmax=vmax)

    ax_was_none = (axes is None) or (fig is None)
    if ax_was_none:
        if one_ax:
            fig, axes = plt.subplots(
                figsize=(12, 6)
            )
            origin_axes = axes
        elif not one_ax and name is None:

            fig, origin_axes, axes = get_multi_fig_ax(
                nb_axes=len(list_var1),
                max_ncols=max_ncols
            )

    ## Second loop for visualisation -------------------
    if not one_ax and name is not None and return_fig:
        origin_axes = []
    for icat in range(len(list_var1)):

        ## kwargs
        kwargs_cat = {}
        for key, val in kwargs.items():
            if isinstance(val, list):
                kwargs_cat[key] = val[icat]
            else:
                kwargs_cat[key] = val

        if list_colors is not None:
            kwargs_cat['cmap'] = cmap[icat]

        if not one_ax and name is not None:
            fig, ax = plt.subplots(figsize=(8, 6))
            if return_fig:
                origin_axes.append(ax)

        else:
            ax = axes if one_ax else axes[icat]
        im = ax.imshow(
            histograms[icat].T,
            norm=norm,
            extent=extent,
            origin='lower',
            **kwargs_cat
        )

        # Labels
        ax.set_xlabel(label_var1)
        ax.set_ylabel(label_var2)


        # Grid
        set_default_grid(ax)

        # Colorbar
        if (list_colors is None) or not one_ax:
            cbar = plt.colorbar(im, ax=ax)
            if name is None:
                cbar.set_label(list_labels[icat])

        ax.set_aspect('auto')

        if not one_ax and name is not None:
            
            fig.tight_layout()
            save_fig(
                fig, path, f"{name}_{list_raw_labels[icat]}"
            )
            plt.close(fig)

    if list_colors is not None and one_ax:
        patches = [
            mpatches.Patch(
                color=list_colors[i], label=list_labels[i],
                alpha=0.5
            )
            for i in range(len(list_var1))
        ]
        plt.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2,
            borderaxespad=0. )

    #ax.legend()
    fig.tight_layout()

    if one_ax and name is not None:
        save_fig(
            fig, path, name
        )
        plt.close(fig)

    if ax_was_none and return_fig:
        return fig, origin_axes


def plot_event(
    event: pd.Series,
    ax: Optional[mpl.axes.Axes] = None,
    list_columns: Optional[List[str]] = None,
    minus: Optional[str] = None,
    **kwargs
) -> Optional[Union[mpl.figure.Figure, mpl.axes.Axes]]:
    """ Plot the output of the LTDB cards at different instants,
    for a given event.

    Args:
        event: LTDB output as a function of the increment.
        ax: ax where to plot.
        list_columns: column names of the sample values in `event`
        minus: what to substract the samples by (if given)
        **kwargs: fed into the
            :py:func:`matplotlib.pyplot.plot` function

    Returns:
        fig, ax: figure and axis (if `ax` was `None`)

    Raises:
        ArgumentError: the columns of `event` are in the
            wrong format. The columns need to include
            `i`, `samples[i]` or `si`,
            where `i` is an integer between 0 and 31

    """

    increments = np.arange(0, 32)

    if list_columns is None:
        list_columns = [f"s{i}" for i in increments]

    if ax is None:
        ax_was_None = True
        fig, ax = plt.subplots(figsize=(8, 6))
    else:
        ax_was_None = False
    

    if minus is not None:
        minus_value = event[minus]
    else:
        minus_value = 0


    ax.plot(increments, event[list_columns] - minus_value,
            **kwargs)
    ax.set_xlabel("Time increment")
    ax.set_ylabel("Output")
    set_default_grid(ax)

    ax.set_xlim([increments[0], increments[-1]])

    if ax_was_None:
        return fig, ax


def plot_samples_lots(
    dataframe: pd.DataFrame, step: int = 2,
    list_columns: Optional[List[str]] = None,
    minus_mean: bool = False,
    minus_pedestal: bool = False,
    image_path: Optional[str] = None,
    name: Optional[str] = None
):
    """Plot all the signals of a dataframe.
    The columns name are of the form `'s{i}'`
    If `name` is specified, the plots are saved
    in a multi-page PDF document.

    Args:
        dataframe: dataframe
        step: number of curves plotted in one figure
        list_columns: column names of the sample values in `dataframe`
        minus_mean: substract by the mean
        image_path, name:  path and name of the PDF file
            to save.

    Returns:
        fig, ax: Figure and axis of the figure
    """
    list_samples_columns = [f"s{i}" for i in range(32)]

    if name is not None: # Output in a PDF document
        # Add extension (if not given)
        if not name.endswith('.pdf'):
            name = name + '.pdf'

        # Path
        if image_path is None:
            path = loc.images_folder
        else:
            path = op.join(
                loc.images_folder, image_path
            )
        try_makedirs(path)
        full_path = op.join(path, name)

        # Open PDF file
        pdf_file = PdfPages(full_path)



    for i in range(len(dataframe)):
        if (i % step) == 0:
            fig, ax = plt.subplots(figsize=(8, 6))

        samples = dataframe.iloc[i]
        if minus_mean:
            samples_plot = samples[list_samples_columns] - samples['mean']
        elif minus_pedestal:
            samples_plot = samples[list_samples_columns] - samples['pedestal']
        else:
            samples_plot = samples

        plot_event(
            samples_plot,
            ax=ax,
            label=str(i),
            list_columns=list_columns
        )
        if (i % step) != 0:
            ax.legend()
            fig.tight_layout()
            if name is not None:
                pdf_file.savefig(fig)
            else:
                plt.show()
            plt.close(fig)

    if name is not None:
        pdf_file.close()


def correlations(
        dataframe: pd.DataFrame,
        column_labels: Optional[Dict[str, str]] = None,
        ax: Optional[mpl.axes.Axes] = None,
        **kwargs) -> Optional[Union[mpl.figure.Figure, mpl.axes.Axes]]:
    """ Calculate and plot pairwise correlation
    between features of the dataframe.

    Args:
        dataframe : dataset
        column_labels: Associates a column name with its label
        **kwargs: fed into ``pandas.DataFrame.corr()``

    Returns:
        fig, ax: Figure and axis of the figure
    """
    if column_labels is None:
        column_labels = {}

    # simply call df.corr() to get a table of
    # correlation values if you do not need
    # the fancy plotting
    corrmat = dataframe.corr(**kwargs)  # correlation

    ax_was_None = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(12, 10))
        ax_was_None = True

    opts = {'cmap': plt.get_cmap("RdBu"),  # red blue color mode
            'vmin': -1, 'vmax': +1}  # correlation between -1 and 1
    heatmap1 = ax.pcolor(corrmat, **opts)  # create a pseudo color plot
    plt.colorbar(heatmap1, ax=ax)  # color bar

    labels = []
    for column in corrmat.columns.values:
        labels.append(get_label(column, column_labels))

    # shift location of ticks to center of the bins
    ax.set_xticks(np.arange(len(labels)) + 0.5, minor=False)
    ax.set_yticks(np.arange(len(labels)) + 0.5, minor=False)
    ax.set_xticklabels(labels, minor=False, ha='right', rotation=70)
    ax.set_yticklabels(labels, minor=False)

    if ax_was_None:
        return fig, ax
