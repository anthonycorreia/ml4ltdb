"""
OPEN ROOT FILES BELONGING TO THE SAME RUN.

Author: Anthony Correia
Date: 05 Jan. 2021
"""

from typing import Union, Optional, List, Dict
import time

import uproot4
from pandas import DataFrame
from numpy import asarray


def list_paths_into_uproot(list_paths: Union[str, List[str]],
                           tree_name: str) -> Dict[str, str]:
    """ Convert a list of paths to ROOT files to a dictionnary
    that associates each file path with the value `tree_name`. This
    dictionnary can be fed into uproot functions to open
    several ROOT files at once.

    Args:
       list_paths: list of paths to ROOT files
       tree_name: common tree name for every ROOT file

    Returns:
        dict_paths_uproot: dictionnary `{path: tree_name}`
    """
    if isinstance(list_paths, str):
        list_paths = [list_paths]
    return {file_path: tree_name for file_path in list_paths}


# COLUMN =====================================================

def get_ROOT_file_branches(
        path: str,
        tree_name: str
)-> List[str]:
    """
    Load ROOT file(s) into one big dataframe.

    Args:
        path: path of the ROOT file
        tree_name: tree name in the ROOT file

    Returns:
        branches: list of branches in the ROOT file
    """
    return uproot4.open(path)[tree_name].keys()


def get_columns_to_compute_and_computed(
    columns: List[str],
    df_columns: List[str]
)-> Union[List[str], List[str]]:
    """ Get the columns to compute and the ones
    to load, according to whether they are in
    the dataframe.

    Args:
        columns: list of columns to get
        df_columns: list of columns in the dataframe
    
    Returns:
        columns_to_load: elements in `columns` and in
            `df_columns`
        columns_to_compute: elements in `columns` and
            not in `df_columns`
    """
    columns_to_compute = []
    columns_to_load = []

    for column in columns:
        if column in df_columns:
            columns_to_load.append(column)
        else:
            columns_to_compute.append(column)
        
    return columns_to_load, columns_to_compute


# LOAD DATAFRAME ==============================================================

def load_ROOT_into_dataframe(
        paths: Union[str, List[str]],
        tree_name: str, variables: Optional[List[str]] = None,
        library: str = 'pd',
        step_size: Optional[int] = None,
        verbose: bool = True,
        **kwargs) -> DataFrame:
    """
    Load ROOT file(s) into one big dataframe.

    Args:
        paths: file(s) to load
        tree_name: name of the tree in each ROOT file
            (supposed to be constant)
        variables: variables to load from the ROOT files
        library : fed into :py:func:`uproot4.arrays` or
            :py:func:`uproot4.concatenate`,
            or :py:func:`uproot4.iterate`
        step_size: if not `None`, returns a generator
            that spits out dataframes of size `step_size`
        verbose: print information
        **kwargs: fed into :py:func:`uproot4.open` or
            :py:func:`uproot4.concatenate`,
            or :py:func:`uproot4.iterate`

    Returns:
        dataframe: Dataframe that contains all the ROOT files
            or generator that spits out dataframes of size
            `step_size`


    Raise:
        AttributeError: `file_paths` not in the correct format
    """
    start = time.time()
    cut_and_library_pd = False

    # List of 1 str -> 1 str

    if step_size is not None:
        paths_uproot = list_paths_into_uproot(
            list_paths=paths,
            tree_name=tree_name
        )

        dataframe = uproot4.iterate(
            paths_uproot, variables,
            step_size=step_size,
            library=library,
            **kwargs
        )
    else:
        if isinstance(paths, list) and len(paths) == 1:
            paths = paths[0]

        if 'cut' in kwargs and library == "pd":
            if isinstance(paths, list) or isinstance(paths, tuple):
                one_path = paths[0]
            else:
                one_path = paths
            
            if "samples" in get_ROOT_file_branches(one_path, tree_name):
                library = "np"
                cut_and_library_pd = True
                # Need to load the ROOT file into an array
                # and convert the array into a dataframe                

        if isinstance(paths, str):
            file = uproot4.open(paths)[tree_name]
            dataframe = file.arrays(variables, library=library, **kwargs)

        elif isinstance(paths, list):
            paths_uproot = list_paths_into_uproot(list_paths=paths,
                                                  tree_name=tree_name)

            dataframe = uproot4.concatenate(
                paths_uproot, variables,
                library=library, **kwargs
            )

        if cut_and_library_pd:
            # Deal with the 32 `samples` variables
            n_samples = 32

            for i in range(n_samples):
                dataframe[f'samples[{i}]'] = asarray(
                    dataframe['samples'][:, i], dtype='int32'
                )

            del dataframe['samples']

            # Numpy Array -> Pandas DataFrame
            dataframe = DataFrame(dataframe)

            if verbose:
                print("Number of events in the dataframe", len(dataframe))

            else:
                raise AttributeError

    if verbose:
        print(f"Time taken to load the ROOT files: {time.time() - start}")
        print("ROOT files loaded:", paths)

    return dataframe
