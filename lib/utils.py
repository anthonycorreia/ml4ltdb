"""
General-purpose variables for the library

Author: Anthony Correia
Date: 05 Jan. 2021
"""

from typing import Union, List, Optional
from os import makedirs
import os.path as op
import pickle
import json

from numpy import array
from pandas import Series

array_like = Union[array, Series]

def cut_into_name(cut: str) -> str:
    """ Turn a cut into a something that
    could go in the name of a file.
    """
    cut = cut.replace(" ", "")
    cut = cut.replace(">=", "_greatereq_")
    cut = cut.replace("<=", "_lowereq_")

    cut = cut.replace(">", "_greater_")
    cut = cut.replace("<", "_lower_")
    
    cut = cut.replace("==", "_eq_")

    return cut

def try_makedirs(path: str):
    """Try to create the directories along the
    the path `path`, if they do not already exist.

    Args:
        path: path
    """
    try:
        makedirs(path)
        print(f"Create the folders in {path}")
    except FileExistsError:
        pass


def select_elem_dict(elem_or_dict, key):
    """Returns the value of a key in a dictionnary,
    or the all object.

    Args:
        elem_or_dict: an element, or a dict
            of elements
        key: desired key in the dictionnary

    Returns:
        the element `elem_or_dict` or
        `elem_or_dict[elem]`

    Notes:
        An example of usage is to be able to have either
        a global binninb (`nbins=50`) or different
        binning for different keys
        (`bins = {key1: value1, key2: value, 'default': default_value}`)
    """
    if isinstance(elem_or_dict, dict):
        return elem_or_dict.get(key, elem_or_dict.get("default"))
    else:
        return elem_or_dict


# =============================== PICKLE FILES ===============================

def dump_pickle(elem, path: str, verbose: bool = True):
    """Save an element in a pickle file

    Args:
        elem: element to save
        path: path of the pickle file
        verbose: print the path where the file is saved
    """

    with open(path, 'wb') as pickle_file:
        pickle.dump(elem, pickle_file)

    if verbose:
        print(f"Pickle file saved in {path}")


def load_pickle(path, verbose: bool = True):
    """Load a pickle file

    Args:
        path: path of the pickle file
        verbose: print the path of the loaded pickle file

    Returns:
        elem: element in the pickle file
    """
    with open(path, "rb") as input_file:
        elem = pickle.load(input_file)

    if verbose:
        print(f"Pickle file loaded: {path}")

    return elem


def dump_json(elem: dict, path: str, verbose: bool = True):
    """Save an element in a json file

    Args:
        elem: element to save
        path: path of the json file
        verbose: print the path where the file is saved
    """

    with open(path + ".json", 'w') as json_file:
        json.dump(elem, json_file, indent=4)

    if verbose:
        print(f"Json file saved in {path}" + ".json")


def load_json(path, verbose: bool = True):
    """Load a json file

    Args:
        path: path of the json file
        verbose: print the path of the loaded json file

    Returns:
        elem: element in the json file
    """
    with open(path, "r") as input_file:
        elem = json.load(input_file)

    if verbose:
        print(f"Json file loaded: {path}")

    return elem

# =============================== TABLE ===============================

def get_latex_column_table(
    L: list
)->str:
    """ Return a sub latex column table from a list

    Args:
        L : List whose each element is a cellule of the 
            sub latex column table

    Returns
    -------
    latex_table :  latex column table
    """
    if isinstance(L, tuple) or isinstance(L, set):
        latex_table = '\\begin{tabular}[c]{@{}l@{}} '
        for i, l in enumerate(L):
            latex_table += l
            if i != len(L) - 1:
                latex_table += ' \\\\ '
        latex_table += '\\end{tabular}'
    else:
        assert isinstance(L, str), print(f'\n \n {L}')
        latex_table = L
    return latex_table

def write_table(
    table: List[List], 
    name: str, path: Optional[str]=None, 
    title: str='line'):
    """ Write a latex table from a table.
    The first line is the title line, separated by
    a double line from the other lines.
    The ufloat numbers are automatically formatted.
    The floated numbers are shown with 3 decimals.
    
    Args:
        table: table to convert into latex
        name: name of the .tex file to save
        folder_name: folder name where to save the .tex file
        title: Title column or title line is separated from
            the rest of the table by a double line
    """    
    ## PATH ============================
    if path is not None:
        try_makedirs(path)
        file_path = op.join(path, f"{name}.tex")
    else:
        file_path = f"{name}.tex"
    
    ## LATEX TABLE =====================
    with open(file_path, 'w') as f:
        n_column = len(table[0])
        if title=='line' or title=='l':
            f.write('\\begin{tabular}[t]{'+ 'l'*(n_column) +'}')
        else:
            f.write('\\begin{tabular}[t]{l||'+ 'c'*(n_column-1) +'}')
        f.write('\n')
        for i, line in enumerate(table):
            formatted_line = []
            for e in line:
                if isinstance(e, int):
                    formatted_line.append(f"{e:,}")                    
                elif isinstance(e, float):
                    formatted_line.append(f"{e:.3f}")
                else:
                    formatted_line.append(str(e))

            f.write("&".join(formatted_line) + "\\\\")
            f.write('\n')
            f.write('\\hline')
            if i==0 and (title=='line' or title=='l'):
                f.write('\\hline')
            f.write('\n')
                    
        f.write("\\end{tabular}")
    
    print("Later table saved in", file_path)
    
