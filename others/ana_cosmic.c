#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX_EXCLUDED 1000

void reco_parabola(int pm1,int p0,int pp1, double *pmax, double *tmax);
void reco_cubic(int pm1, int p0, int pp1, int pp2, double *pmax1, double *pmax2, double *tmax1, double *tmax2);
int tab_veto[MAX_EXCLUDED];


// Declaration of leaf types
Int_t           channelId;
Int_t           barrel_ec;
Int_t           pos_neg;
Int_t           FT;
Int_t           slot;
Int_t           channel;
Int_t           calibLine;
Int_t           isConnected;
Int_t           channelHash;
Int_t           febHash;
Int_t           oflHash;
Int_t           offlineId;
Int_t           layer;
Float_t         eta;
Float_t         phi;
ULong64_t       IEvent;
Short_t         samples[32];
Int_t           Nsamples;
Short_t         BCID;
Short_t         latomeChannel;
UShort_t        bcidVec[32];
UInt_t          latomeSourceId;
Short_t         samples_ADC_BAS[32];
UShort_t        bcidVec_ADC_BAS[32];
Int_t           energyVec_ET[32];
UShort_t        bcidVec_ET[32];
Bool_t          saturVec_ET[32];
Int_t           energyVec_ET_ID[32];
UShort_t        bcidVec_ET_ID[32];
Bool_t          saturVec_ET_ID[32];
UShort_t        bcidLATOMEHEAD;
UInt_t          latomeidLATOMEHEAD;
UInt_t          l1idLATOMEHEAD;

// List of branches
TBranch        *b_channelId;   //!
TBranch        *b_barrel_ec;   //!
TBranch        *b_pos_neg;   //!
TBranch        *b_FT;   //!
TBranch        *b_slot;   //!
TBranch        *b_channel;   //!
TBranch        *b_calibLine;   //!
TBranch        *b_isConnected;   //!
TBranch        *b_channelHash;   //!
TBranch        *b_febHash;   //!
TBranch        *b_oflHash;   //!
TBranch        *b_offlineId;   //!
TBranch        *b_layer;   //!
TBranch        *b_eta;   //!
TBranch        *b_phi;   //!
TBranch        *b_IEvent;   //!
TBranch        *b_samples;   //!
TBranch        *b_Nsamples;   //!
TBranch        *b_BCID;   //!
TBranch        *b_latomeChannel;   //!
TBranch        *b_bcidVec;   //!
TBranch        *b_latomeSourceId;   //!
TBranch        *b_samples_ADC_BAS;   //!
TBranch        *b_bcidVec_ADC_BAS;   //!
TBranch        *b_energyVec_ET;   //!
TBranch        *b_bcidVec_ET;   //!
TBranch        *b_saturVec_ET;   //!
TBranch        *b_energyVec_ET_ID;   //!
TBranch        *b_bcidVec_ET_ID;   //!
TBranch        *b_saturVec_ET_ID;   //!
TBranch        *b_bcidLATOMEHEAD;   //!
TBranch        *b_latomeidLATOMEHEAD;   //!
TBranch        *b_l1idLATOMEHEAD;   //!

int is_excluded(int chanid,int n_excluded_channels);

void ana_cosmic(string input_fname, string veto_fname, string output_fname,string frame_fname,int mon_num)
{
int nentries, ientry;
int isamp;

int event_tag;

double rms,mean,pedestal,samp_max,samp_min;
int pos_of_max,pos_of_min;

int pm1,p0,pp1,pp2;
double tmax,pmax;
double cub_pmax1,cub_pmax2,cub_tmax1,cub_tmax2;

int n_excluded_channels;

FILE *outfile,*framefile;

FILE *file_veto;

// Code section
TFile *f=new TFile(input_fname.c_str());
TTree *fChain=(TTree *) f->Get("LARDIGITS");

fChain->SetBranchAddress("channelId", &channelId, &b_channelId);
fChain->SetBranchAddress("barrel_ec", &barrel_ec, &b_barrel_ec);
fChain->SetBranchAddress("pos_neg", &pos_neg, &b_pos_neg);
fChain->SetBranchAddress("FT", &FT, &b_FT);
fChain->SetBranchAddress("slot", &slot, &b_slot);
fChain->SetBranchAddress("channel", &channel, &b_channel);
fChain->SetBranchAddress("calibLine", &calibLine, &b_calibLine);
fChain->SetBranchAddress("isConnected", &isConnected, &b_isConnected);
fChain->SetBranchAddress("channelHash", &channelHash, &b_channelHash);
fChain->SetBranchAddress("febHash", &febHash, &b_febHash);
fChain->SetBranchAddress("oflHash", &oflHash, &b_oflHash);
fChain->SetBranchAddress("offlineId", &offlineId, &b_offlineId);
fChain->SetBranchAddress("layer", &layer, &b_layer);
fChain->SetBranchAddress("eta", &eta, &b_eta);
fChain->SetBranchAddress("phi", &phi, &b_phi);
fChain->SetBranchAddress("IEvent", &IEvent, &b_IEvent);
fChain->SetBranchAddress("samples", samples, &b_samples);
fChain->SetBranchAddress("Nsamples", &Nsamples, &b_Nsamples);
fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
fChain->SetBranchAddress("latomeChannel", &latomeChannel, &b_latomeChannel);
fChain->SetBranchAddress("bcidVec", bcidVec, &b_bcidVec);
fChain->SetBranchAddress("latomeSourceId", &latomeSourceId, &b_latomeSourceId);
fChain->SetBranchAddress("samples_ADC_BAS", samples_ADC_BAS, &b_samples_ADC_BAS);
fChain->SetBranchAddress("bcidVec_ADC_BAS", bcidVec_ADC_BAS, &b_bcidVec_ADC_BAS);
fChain->SetBranchAddress("energyVec_ET", energyVec_ET, &b_energyVec_ET);
fChain->SetBranchAddress("bcidVec_ET", bcidVec_ET, &b_bcidVec_ET);
fChain->SetBranchAddress("saturVec_ET", saturVec_ET, &b_saturVec_ET);
fChain->SetBranchAddress("energyVec_ET_ID", energyVec_ET_ID, &b_energyVec_ET_ID);
fChain->SetBranchAddress("bcidVec_ET_ID", bcidVec_ET_ID, &b_bcidVec_ET_ID);
fChain->SetBranchAddress("saturVec_ET_ID", saturVec_ET_ID, &b_saturVec_ET_ID);
fChain->SetBranchAddress("bcidLATOMEHEAD", &bcidLATOMEHEAD, &b_bcidLATOMEHEAD);
fChain->SetBranchAddress("latomeidLATOMEHEAD", &latomeidLATOMEHEAD, &b_latomeidLATOMEHEAD);
fChain->SetBranchAddress("l1idLATOMEHEAD", &l1idLATOMEHEAD, &b_l1idLATOMEHEAD);


printf("Program start\n");

nentries=fChain->GetEntries();
printf("Number of entries : %d\n",nentries);

/* Open output files */
outfile=fopen(output_fname.c_str(),"at");
framefile=fopen(frame_fname.c_str(),"at");
printf("Outfile opened\n");

for (ientry=0; ientry<nentries; ientry++)
//for (ientry=0; ientry<130000; ientry++)
  {
  if (ientry == 0)
    {
    int i=0;
    int excluded_chanid;
    int bidon;
    char strline[132];
    /* Load channel exclusion list */
    printf("Loading channel exclusion list : %s\n",veto_fname.c_str());
    file_veto=fopen(veto_fname.c_str(),"rt");
    while (!feof(file_veto))
      {
      fgets(strline,132,file_veto);
      sscanf(strline,"%d %d",&bidon,&excluded_chanid);
      //fscanf(file_veto, "%d %d\n",&bidon,&excluded_chanid);
      tab_veto[i++]=excluded_chanid;
      /* Debug printout */
      printf("coucou %d %d\n",bidon,excluded_chanid);
      }
    fclose(file_veto);
    n_excluded_channels=i;
    }
  //printf("ientry=%d\n",ientry);
  fChain->GetEntry(ientry);
  //Compute average and rms of samples
  mean=rms=0.0;
  samp_max=-1000;
  samp_min=9999;
  pos_of_max=-1;
  pos_of_min=-1;
  event_tag=0;
  //printf("Ientry=%d, nsamp=%d\n",ientry,Nsamples);
  pedestal=(double)samples[0];
  for (isamp=0;isamp<Nsamples;isamp++)
    {
    if (samples[isamp]>samp_max)
      {
      samp_max=samples[isamp];
      pos_of_max=isamp;
      }
    if (samples[isamp]<samp_min)
      {
      samp_min=samples[isamp];
      pos_of_min=isamp;
      }
    mean+=samples[isamp];
    rms+=(samples[isamp]*samples[isamp]);
    }
  mean/=Nsamples;
  rms/=Nsamples;
  rms-=(mean*mean);
  if (rms>0) rms=sqrt(rms); else rms=-sqrt(-rms);
  // Analyze the data to classify the event and define its tag
  if ( (pedestal < 100.0) || (pedestal > 2000.0) ) event_tag = event_tag | 1;
  if ( fabs(rms)<0.001) event_tag = event_tag | 2;
  if ( (fabs(rms)>5.0) && (fabs(rms)<10.0) ) event_tag=event_tag | 4;
  if (fabs(rms)>10.0) event_tag=event_tag | 8;
  // Do the parabola reconstruction
  if ( (pos_of_max<1) || (pos_of_max>30) )
    {
    pmax=-1.0; tmax=-9999.0;
    }
  else
    {
    pm1=samples[pos_of_max-1];
    p0=samples[pos_of_max];
    pp1=samples[pos_of_max+1];
    reco_parabola(pm1,p0,pp1, &pmax, &tmax);
    }
  //Do the cubic reconstruction
  if ( (pos_of_max<1) || (pos_of_max>29) )
    {
    cub_pmax1=cub_pmax2=-1.0; cub_tmax1=cub_tmax2=-9999.0;
    }
  else
    {
    pm1=samples[pos_of_max-1];
    p0=samples[pos_of_max];
    pp1=samples[pos_of_max+1];
    pp2=samples[pos_of_max+2];
    reco_cubic(pm1,p0,pp1, pp2, &cub_pmax1, &cub_pmax2, &cub_tmax1,&cub_tmax2);
    }
  // Check condition to decide whether we output the analysis event data
  //if ( (event_tag == event_tag) && (is_excluded(channelId,n_excluded_channels)==0) )
  //if ((rms>4) && (rms<1000) && is_excluded(channelId,n_excluded_channels)==0 && (abs(eta)<1.5 || layer != 0 ) 
  //    && ( (phi<-0.544)||(phi>-0.543) ))
  if ((rms>4) && (rms<1000) && is_excluded(channelId,n_excluded_channels)==0 && (abs(eta)<1.5 || layer != 0 ) )
    {
    fprintf(outfile,"%d %d %d %f %f %d %lf %lf %lf %lf %lf %d %d %d %d %d %lf %lf %lf %lf %lf %lf\n",ientry, IEvent, layer, eta, phi, BCID, pedestal, mean,rms,samp_max,samp_min,pos_of_max,pos_of_min,event_tag, mon_num, channelId,pmax,tmax,cub_pmax1,cub_pmax2,cub_tmax1,cub_tmax2);
  // Check condition to decide whether we output the raw data frame
  //if ((rms>4) && (rms<1000) && is_excluded(channelId,n_excluded_channels)==0 && (abs(eta)<1.5 || layer != 0 ))
  if ( (pos_of_max>0) && (pos_of_max<6) && (layer==2) && (samp_max-pedestal>100.0) && (samp_max-pedestal<3000.0))
      {
      for (isamp=0; isamp<Nsamples; isamp++)
        fprintf(framefile,"%d %d %d %f %f %d %lf %lf %lf %lf %lf %d %d %d %d %d %d %d %lf %lf %lf %lf %lf %lf\n",ientry,IEvent,layer,eta,phi,BCID,pedestal,mean,
        rms,samp_max,samp_min,pos_of_max,pos_of_min,isamp,samples[isamp],event_tag,mon_num,channelId,pmax,tmax,cub_pmax1,cub_pmax2,cub_tmax1,cub_tmax2);
      }
    }
  }

fclose(outfile);
fclose(framefile);
}

int is_excluded(int chanid,int n_excluded_channels)
  {
  int result=0;
  for (int i=0; i<n_excluded_channels; i++)
    if (tab_veto[i]==chanid) result=1;

  return result; 
  }


void reco_parabola(int pm1,int p0,int pp1, double *pmax, double *tmax)
{
double a,b,c; /* Coefficients of the parabola */
double t,p; /* Position and value of the extremum */

c=(double)p0; b=(pp1-pm1)/2.0; a=(pp1+pm1)/2.0-c;

if (fabs(a)<1e-9) {t=-1000.0; p=-1000.0;} else {t=-b/2./a; p=a*t*t+b+c;}

*tmax=t; *pmax=p;
}

void reco_cubic(int pm1, int p0, int pp1, int pp2, double *pmax1, double *pmax2, double *tmax1, double *tmax2)
{
double a,b,c,d; /* Coefficients of the cubic function */

double delta,t1,t2;
double p1,p2;

d=p0;
b=(pm1+pp1)/2.0-d;
a=(pp2-pp1-3.0*b)/7.0;
c=(pp1-pm1)/2.0-a;

/* Recherche des extremas */
delta=b*b-3.*a*c;
if ( (delta >= 0.0) && (fabs(a)>1.e-9) )
  {
  /* Calcul des solutions */
  t1=(-b+sqrt(delta))/3./a;
  t2=(-b-sqrt(delta))/3./a;
  /* Cherchons quelle est la bonne solution */
  p1=a*t1*t1*t1+b*t1*t1+c*t1+d;
  p2=a*t2*t2*t2+b*t2*t2+c*t2+d;
  }
else
  {
  t1=-1000.0; t2=-1000.0; p1=-1000.0; p2=-1000.0;
  }
*tmax1=t1; *tmax2=t2; *pmax1=p1; *pmax2=p2;
}
