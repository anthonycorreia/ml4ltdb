## Local package of ML4LTDB

This folder is rather messy (no subfolders...) as I did not have the time to tidy it up. However, everything should be functional. It contains functions and global variables specific to the project.

First, there are python modules that define global variables used throughout the project:
- `defs.py`: some general global variables
- `loc.py` paths of input our output files
- `defs_label.py`: definition of the label of the variables
- `defs_range.py`: definition of the range or the binning of some variables, for the histograms

2 python modules are particularly used in the analysis, and they are bound to be modified by the user.
- `cuts.py` defines the cut to select signal, peaked and oscillating background
- `configuration.py` contains a dictionnary `configurations`. Thus, we can associates a name (e.g., `run1_layer2`, etc.) with the full configuration of a ROOT file we want to produce for further analysis (e.g., the columns to compute and save in the ROOT file, the cuts, etc.)

The other modules consist of functions. There are python modules dedicated to computing variables of interest (VOI)
- `files.py` contains function to compute VOIs such as mean, standard deviation, etc.
- `compute_chi2.py` allows to compute the optimised $\chi^2$
- `voi_background`: contains functions to compute voi used to isolate peaked and oscillating background (e.g., period, number of peaks, etc.)
- `col.py` aimed to compute a combination of variables of interest (e.g., `max_m_mean` is interpreted $\max{e} - \min{e}$, and its label is computed from the latex of `max` and `mean`).

Finally, the remaining python modules contain functions used in the project
- `plot.py` contains just a function to plot 1D or 2D histograms in each layer
- `normalize.py` is used to standardise the distributions
- `channel_id.py` allows to find the (predominant) channels of a dataframe, and their counts in the dataframe.

