""" Count the number of events for different
`channelId`. These functions have been written
considering that most of strange background events
tend to happen to the same set of channels.
"""

from typing import Dict, Optional, Union
import numpy as np

def get_channel_id_counts(dataframe)->Dict[str, int]:
    """ Find the number of events in each channel.

    Args:
        dataframe: dataframe
    
    Returns:
        channel_id_counts:  associates a channelId 
            with its count, sorted from high to low
    """
    counts = dataframe[["channelId", "s0"]]\
        .groupby("channelId").count()\
            .sort_values("s0", ascending=False)
    
    return {
        int(channel_id): int(count)
        for channel_id, count in zip(
            counts.index.values, 
            counts["s0"].values
        )
    }
    

def select_new_predominant_channel_id(
    channel_id_counts: Dict[str, int] =  Dict,
    previous_channel_id_counts: Optional[Dict[str, int]] = None,
    min_count: Union[int, float] = 0.1,
    nb_events: int = None
)-> Dict[str, int]:
    """ Select the channels that have enough counts,
    and that do not belong to another dictionnary (
    being in that sense "new").

    Args:
        channel_id_counts: associates `channelId`
            with their counts, that for each layer:
            `{layer: {channelId: count}}`
        previous_channel_id_counts: the channels that
            are retained must not be in this dictionnary
        min_count: minimal number of counts for a channel 
            to be kept in the returned dataframe. If is is
            a float number between 0 and 1
        nb_events_layers: number of events in the dataframe.
            Used if `min_count` is a fraction (between 0 and 1)
            If `None`, it is recomputed.
    
    Returns:
        new_predominant_channelId: associates
            a `channelId` with its number of events,
            excluding not new channels, or channels
            that do no have enough registered events.
    """
    if previous_channel_id_counts is None:
        previous_channel_id_counts = {}
    new_predominant_channelId = {}

    min_count_is_frac = isinstance(min_count, float) 
    min_count_is_frac &= (min_count <= 1) and (min_count >= 0)
    if min_count_is_frac and (nb_events is None):
        nb_events = sum(list(channel_id_counts.values()))
    
    for channel_id, counts in channel_id_counts.items():           

        if channel_id not in previous_channel_id_counts:
            if min_count_is_frac:
                condition = counts >= min_count * nb_events
            else:
                condition = counts >= min_count
            if condition:
                new_predominant_channelId[channel_id] = counts
    return new_predominant_channelId