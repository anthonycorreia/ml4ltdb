""" Functions to manipulate labels and formula
"""

from typing import List, Dict, Union, Optional
import re
import time

import pandas as pd

from lib import data
from .defs_label import dict_delimiters, column_labels as default_column_labels
from . import files, defs_label
from . import compute_chi2, voi_background


def get_formula_columns(columns: List[str]) -> Dict[str, str]:
    """Get the formula of each column that needs to
    be computed. Uses the dictionnary of delimiters `dict_delimiters`
    defined in `project.defs_label`.

    If the column name ends with `_red`, it means the values 
    are divided by the standard deviation `std`.
    If the column name ends with `_redpn`, it means the values 
    are divided by `std_pos + std_neg`.

    Args:
        columns: list of column names that need to be computed.

    Returns:
        formula_columns: associates a column name with its
            formula.

    Examples:
        >>> get_formula_columns(["mean_m_max"])
        {"mean_m_max": "mean - max"}
    """
    formula_columns = {}
    for column in columns:
        if column.endswith('_red'):
            end = len(column) - 4
        elif column.endswith('_redpn'):
            end = len(column) - len('_redpn')
        elif column.endswith('_d_max_m_min'):
            end = len(column) - len('_d_max_m_min')
        else:
            end = len(column)

        for delim, meaning in dict_delimiters.items():
            formula = column[:end].replace(delim, meaning)
        
        if column.endswith('_red'):
            formula_columns[column] = f"({formula}) / std"
        elif column.endswith('_redpn'):
            formula_columns[column] = f"({formula}) / (std_pos + std_neg)"
        elif column.endswith('_d_max_m_min'):
            formula_columns[column] = f"({formula}) / (max - min)"

        else:
            formula_columns[column] = formula

    return formula_columns


def get_formula_labels(
    columns: List[str],
    column_labels: Optional[Dict[str, str]] = None,
) -> Dict[str, str]:
    """Get the labels of columns that are computed with
    via a formula. If a label is already defined in
    `column_label`, don't compute it.

    If the column name ends with `_red`, it means the values 
    are divided by the standard deviation `std`.

    Args:
        columns: list of column names which we want the
            labels of
        column_labels: associates a column name with 
            its label.


    Returns:
        formula_labels: associates a column name with its
            label.

    Examples:
        >>> get_formula_labels(["mean_m_pedestal"])
        {'mean_m_pedestal': '$\\langle e \\rangle - e_0$'}
    """

    if column_labels is None:
        column_labels = default_column_labels

    formula_labels = {}
    for column in columns:
        if column not in column_labels:
            need_latex = False

            if column.endswith('_red'):
                end = len(column) - 4
            elif column.endswith('_redpn'):
                end = len(column) - len('_redpn')
            elif column.endswith("_rounded"):
                end = len(column) - len('_rounded')
            elif column.endswith('_d_max_m_min'):
                end = len(column) - len('_d_max_m_min')
            else: 
                end = len(column)

            # `"std_pos_m_std_neg"` -> `["std_pos", "_m_", "std_neg"]`
            column_split = re.split(
                '|'.join(['(' + delim + ')'
                        for delim in dict_delimiters]),
                column[:end]
            )

            for i, _ in enumerate(column_split):
                # column name -> column label
                if column_split[i] in column_labels:
                    column_label = column_labels[column_split[i]]

                    # Remove the `$` in the name of the columns
                    if '$' in column_label:
                        need_latex = True
                    column_label = column_label.replace('$', '')

                    column_split[i] = column_label

                # delimiter -> delimiter label
                elif column_split[i] in dict_delimiters:
                    column_split[i] = dict_delimiters[column_split[i]]

            formula_labels[column] = ''.join(column_split)

            if column.endswith('_red'):
                need_latex = True
                formula_labels[column] = r"\frac{" + formula_labels[column] 
                formula_labels[column] += r"}{" 
                formula_labels[column] += column_labels["std"].replace('$', '') 
                formula_labels[column] += r"}"
            elif column.endswith('_redpn'):
                need_latex = True
                formula_labels[column] = r"\frac{" + formula_labels[column] 
                formula_labels[column] += r"}{" 
                formula_labels[column] += column_labels["std_pos"].replace('$', '')
                formula_labels[column] += " + " + column_labels["std_neg"].replace('$', '')
                formula_labels[column] += r"}"
            elif column.endswith("_rounded"):
                need_latex = True
                formula_labels[column] = r"\left\lfloor" \
                    + formula_labels[column]
                formula_labels[column] += r"\right\rceil"
            elif column.endswith('_d_max_m_min'):
                need_latex = True
                formula_labels[column] = r"\frac{" + formula_labels[column] 
                formula_labels[column] += r"}{" 
                formula_labels[column] += column_labels["max"].replace('$', '')
                formula_labels[column] += " - " + column_labels["min"].replace('$', '')
                formula_labels[column] += r"}"
            if need_latex:
                formula_labels[column] = '$' + formula_labels[column] + '$'

    return formula_labels


def compute_all_columns(
    dataframe: pd.DataFrame,
    columns_to_compute: List[str] = None,
    list_functions_run: Optional[List[str]] = None,
    verbose : bool = False
):
    """Compute columns in a dataframe and get its labels.
    In-place (for the computation of the columns).

    Args:
        dataframe: dataframe
        columns_to_compute: list of columns to compute
        list_function_run: names of functions to run in order
            to compute some columns
        verbose: print the time the operation has taken
    
    Returns:
        column_labels: associates a column name with 
            its label.
    """
    start = time.time()

    if columns_to_compute is None:
        columns_to_compute = []

    if list_functions_run is not None:
        for function in list_functions_run:
            if function == "peak_voi":
                files.compute_peak_voi(
                    dataframe, zero_column="mean", red=True
                ) 
            elif function == "threshold_voi": 
                files.compute_threshold_voi(
                    dataframe, ref_column="min", threshold=0.8
                )  
            elif function == "chi2":
                if verbose:
                    print("COMPUTE BEST CHI2 (not already computed)")
                chi2 = compute_chi2.compute_chi2_dataframe_to_sig_optimised(
                    dataframe
                )
                dataframe["best_chi2"] = chi2

            elif function == "nb_peaks":
                voi_background.compute_nb_peaks(dataframe)

            elif function == "std_peaks":
                voi_background.compute_std_without_peaks(dataframe)

            elif function == "period":
                voi_background.compute_period_extremum_df(
                    dataframe
                )

            elif function == "diff_extremum":
                voi_background.compute_mean2_diff_extremum_df(dataframe)
            
            else:
                print(f"Computation of {function} is not known!")

    formula_columns = get_formula_columns(columns_to_compute)
    formula_column_labels = get_formula_labels(
        columns_to_compute, defs_label.column_labels
    )
    column_labels = {
        **defs_label.column_labels, **formula_column_labels
    }

    # Compute them
    dataframe = data.compute_vals(
        dataframe, formula_columns
    )

    if verbose:
        print(f"Time taken to compute the columns: {time.time() - start}")

    return column_labels

def get_other_columns_to_compute(
     other_columns: List[str], df_columns
)-> Union[List[str], List[str]]:
    """ Get other columns that can be loaded
    from the ROOT file (instead of recomputed).
    args:
        other_columns: other columns computed as    
            peaked voi, or threshold voi, or chi2
        df_columns: columns in the ROOT
            file
    returns:
        columns_to_load: column that can be
            directly loaded from the ROOT
            file
        list_function_run: Function to run,among
             `peak_voi`, `threshold_voi`, `chi2`, 
             `nb_peaks`, `std_peak`, `period`,
             `diff_extremum` (in order to compute
             the voi not already in the dataframe)
    """

    columns_to_load = []

    function_to_columns = {}

    function_to_columns["peak_voi"] = [
        "pos_first_neg_after_max",
        "mean_before_end_peak",
        "mean_after_end_peak"
    ]
    function_to_columns["threshold_voi"] = [
        "nb_peaks_sup_80_wrt_min"
    ]

    function_to_columns["chi2"] = [
        "best_chi2"
    ]
    
    function_to_columns["nb_peaks"] = [
        "nb_pos_peaks",
        "nb_neg_peaks"
    ]

    function_to_columns["std_peaks"] = [
        "std_without_pos_peaks",
        "std_without_neg_peaks"
    ]

    function_to_columns["period"] = [
        "mean_period_max",
        "mean_period_min",
        "mean_period",
        "std_period_max",
        "std_period_min",
        "std_period"
    ]

    function_to_columns["diff_extremum"] = [
        "mean2_diff_max",
        "mean2_diff_min",
        "mean2_diff"
    ]

    list_functions_run = []

    for column in other_columns:
        if column in df_columns:
            columns_to_load.append(column)
        else:
            found_column = False
            for function, columns in function_to_columns.items():
                if column in columns:
                    found_column = True
                    list_functions_run.append(function)

            if not found_column:
                print(f"How to compute {column} is not known")
                
            # if column=="best_chi2":
            #     dict_voi_to_compute["chi2"] = column not in df_columns
            #     if not dict_voi_to_compute["chi2"]:
            #         columns_to_load.append(column)

            # elif column in peak_voi_columns:
            #     dict_voi_to_compute["chi2"] = column in df_columns
            #     if column in df_columns:
            #         dict_voi_to_compute["peak_voi"] = False
            #         columns_to_load.append(column)
            #     else:
            #         dict_voi_to_compute["peak_voi"] = True

            # elif column.startswith("nb_peaks_sup_"):
            #     if column in df_columns:
            #         threshold_voi = False
            #         columns_to_load.append(column)
            #     else:
            #         threshold_voi = True

    return columns_to_load, list_functions_run