""" Compute the chi2 of a dataframe w.r.t.
the signal shape.

Specifically for the layer 2.
"""

from project import defs, normalize
from scripts.simulation.models import get_sig_model

import numpy as np
import pandas as pd
from numba import jit

## TOOLS =========================================

def compute_chi2(
    array1: np.array, array2: np.array
) -> float:
    """ Compute the :math:`\\chi^2` between
    2 arrays.

    args:
        array1, array2: 2 arrays
    
    returns:
        chi2: reduced chi2 between the 2 arrays
    """
    return np.mean(np.square(array1 - array2)) / len(array1)

@jit(nopython=True)
def compute_chi2_numba(
    array1: np.array, array2: np.array
) -> float:
    """ Compute the :math:`\\chi^2` between
    2 arrays. Compatible with numba.

    args:
        array1, array2: 2 arrays
    
    returns:
        chi2: reduced chi2 between the 2 arrays
    """
    return np.mean(np.square(array1 - array2)) / len(array1)


@jit(nopython=True)
def compute_scale_numba(
    array: np.array, array_red: np.array, 
    pos_max: int = -1, red_pos_max: int = -1
)->float:
    """
    Args: 
        array, array_red : 2 arrays, array_red being
            the reference
        pos_max, red_pos_max: position of the max
            in `array` and `array_red`
            (to avoid recomputing it) 
    
    Returns: 
        scale: max of `array` / max of `array_red`
    """
    if pos_max == -1:
        array_max = np.max(array)
    else:
        array_max = array[pos_max]
    
    if red_pos_max == -1:
        array_red_max = np.max(array_red)
    else:
        array_red_max = array_red[red_pos_max]

    return array_max / array_red_max

def compute_scale(
    array: np.array, array_red: np.array, 
)->float:
    """
    Args: 
        array, array_red : 2 arrays, array_red being
            the reference
    
    Returns: 
        scale: max of `array` / max of `array_red`
    """
        
    return np.max(array) / np.max(array_red)


## OPTIMISATION ==================================================

@jit(nopython=True)
def find_best_pos_max(
    event_array: np.array, 
    sig_array : np.array, 
    pos_max: int = -1,
    sig_pos_max: int = -1,
    wiggle_pos_max_limit: int = 2
) -> int:
    """ Return the best position of the max
    that minimises the $\chi^2$ between 
    simulated shape and one real event.

    Args:
        event_array: one event
        sig_array: the simulated sample
        wiggle_pos_max_limit: maximal absolute
            shift of `pos_max` considered
        pos_max, sig_pos_max: position of the max
            in `event_array` and `sig_array`
            (to avoid recomputing it)
    
    Returns:
        best_pos_max: `pos_max` minimising the
        :math:`\\chi^2` between event and simulated
        sample (when the `pos_max` are aligned)
    """

    ## Compute the positions of the max -------------
    if sig_pos_max == -1:
        sig_pos_max = np.argmax(sig_array)
    if pos_max == -1:
        pos_max = np.argmax(event_array)
    
    ## INIT ----------------------------------------
    best_pos_max = pos_max
    chi2_min = np.inf

    scale = compute_scale_numba(
        event_array, sig_array,
        pos_max, sig_pos_max
    )

    ## For each pos_max, compute chi2 --------------
    # to find the min
    for wiggle in range(- wiggle_pos_max_limit, wiggle_pos_max_limit + 1): 
        # Current pos max
        pos_max_wiggled = pos_max + wiggle

        # Signal
        sig_start_pos = sig_pos_max - pos_max_wiggled
        sig_end_pos = sig_start_pos + defs.n_samples

        # Compute chi2
        chi2 = compute_chi2_numba(
            event_array, sig_array[sig_start_pos:sig_end_pos] * scale
        )

        # If it's smaller than the one stored,
        # it's the current best minimum
        if chi2 < chi2_min:
            best_pos_max = pos_max_wiggled
            chi2_min = chi2

    return best_pos_max # return best pos_max

@jit(nopython=True)
def find_best_ampl_numba(
    event_array: np.array, sig_array:np.array,
    pos_max: int = -1,
    pos_max_modif: int = -1, sig_pos_max: int = -1,
    ampl_var=1.3, 
)->float:
    """ Numba version to find the amplitude of
    the signal that minimises the $\chi^2$ between
    the `event_array` and 
    `sig_array * max(event_array) / max(sig_array)`.

    An entire part of this function has been copied from 
    :py:func:`scipy.optimize._optimize._minimize_scalar_bounded` 
    in order to be compatible with Numba.

    Array:
        event_array: one event
        sig_array: the simulated signal version of an event
        pos_max: position of the max of `event_array`.
            If not given, it is recomputed.
        pos_max_modif: Optimised position of the max of `event_array`.
            If not given, taken to be `pos_max`.
        sig_pos_max: position of the max of `signal_array`.
            If not given, it is recomputed.
        ampl_var: the optimal :math:`\chi^2` is searched between
            `1 / ampl_var` and `ampl_var` multiplied by the
            maximum of `event_array` (that is, the default value).
        verbose: whether the summary of the fit is plotted
    
    returns:
        best_chi2: the lowest :math:`\chi^2` obtained after
            minimisation
    """

    ## Compute the position of the max, if not given -------
    if sig_pos_max == -1:
        sig_pos_max = np.argmax(sig_array)
    if pos_max_modif == -1:
        if pos_max == -1:
            pos_max = np.argmax(event_array)
        pos_max_modif = pos_max

    
    ## Signal indices -------------------------------------
    sig_start_pos = sig_pos_max - pos_max_modif
    sig_end_pos = sig_start_pos + defs.n_samples

    ## Initial scale of event w.r.t. signal ---------------
    scale = compute_scale_numba(
        event_array, sig_array, pos_max, sig_pos_max
    )

    ## MINIMISATION ----------------------------------------
    maxfun = 10 # max iteration
    xatol = 1e-5
    # Test bounds are of correct form
    x1, x2 = 1/ampl_var, ampl_var

    flag = 0

    sqrt_eps = np.sqrt(2.2e-16)
    golden_mean = 0.5 * (3.0 - np.sqrt(5.0))
    a, b = x1, x2
    fulc = a + golden_mean * (b - a)
    nfc, xf = fulc, fulc
    rat = e = 0.0
    x = xf
    fx = compute_chi2_numba(
        event_array, 
        sig_array[sig_start_pos:sig_end_pos] * x * scale
    )
    num = 1
    fu = np.inf

    ffulc = fnfc = fx
    xm = 0.5 * (a + b)
    tol1 = sqrt_eps * np.abs(xf) + xatol / 3.0
    tol2 = 2.0 * tol1


    while (np.abs(xf - xm) > (tol2 - 0.5 * (b - a))):
        golden = 1
        # Check for parabolic fit
        if np.abs(e) > tol1:
            golden = 0
            r = (xf - nfc) * (fx - ffulc)
            q = (xf - fulc) * (fx - fnfc)
            p = (xf - fulc) * q - (xf - nfc) * r
            q = 2.0 * (q - r)
            if q > 0.0:
                p = -p
            q = np.abs(q)
            r = e
            e = rat

            # Check for acceptability of parabola
            if ((np.abs(p) < np.abs(0.5*q*r)) and (p > q*(a - xf)) and
                    (p < q * (b - xf))):
                rat = (p + 0.0) / q
                x = xf + rat

                if ((x - a) < tol2) or ((b - x) < tol2):
                    si = np.sign(xm - xf) + ((xm - xf) == 0)
                    rat = tol1 * si
            else:      # do a golden-section step
                golden = 1

        if golden:  # do a golden-section step
            if xf >= xm:
                e = a - xf
            else:
                e = b - xf
            rat = golden_mean*e

        si = np.sign(rat) + (rat == 0)
        x = xf + si * np.maximum(np.abs(rat), tol1)
        fu = compute_chi2_numba(
            event_array, 
            sig_array[sig_start_pos:sig_end_pos] * x * scale
        )
        num += 1

        if fu <= fx:
            if x >= xf:
                a = xf
            else:
                b = xf
            fulc, ffulc = nfc, fnfc
            nfc, fnfc = xf, fx
            xf, fx = x, fu
        else:
            if x < xf:
                a = x
            else:
                b = x
            if (fu <= fnfc) or (nfc == xf):
                fulc, ffulc = nfc, fnfc
                nfc, fnfc = x, fu
            elif (fu <= ffulc) or (fulc == xf) or (fulc == nfc):
                fulc, ffulc = x, fu

        xm = 0.5 * (a + b)
        tol1 = sqrt_eps * np.abs(xf) + xatol / 3.0
        tol2 = 2.0 * tol1

        if num >= maxfun:
            flag = 1
            break

    if np.isnan(xf) or np.isnan(fx) or np.isnan(fu):
        flag = 2

    fval = fx

    ## Return the minimised value -----------------------------
    default_chi2 = compute_chi2_numba(
        event_array, 
        sig_array[sig_start_pos:sig_end_pos] * scale
    )

    if flag == 0:
        if fval > default_chi2:
            fval = default_chi2
        return fval
    else:
        return default_chi2

## COMPUTE CHI2 ==================================================

@jit(nopython=True)
def compute_chi2_array_to_sig_optimised(
    array: np.ndarray, sig_array: np.array, pos_max: np.array, sig_pos_max: int
)->np.array:
    """ Compute the optimised reduced chi2 between a set of samples and 
    the simulated signal shape. Use Numba for speed-up.


    Args:
        array: array of (events, samples)
        sig_array: simulated signal shape ; might be bigger than the number
            of samples (32) to allow for time translation
        pos_max: position of the max of each event in `array`
        sig_pos_max: position of the max of the signal
            (to avoid recomputing it)
        
    returns:
        chi2: reduced chi2 between each event and the simulated signal shape         
    """
    chi2 = np.empty(shape=len(array), dtype=np.float_)

    for i in range(len(array)): # Loop over the events
        pos_max_i = pos_max[i]

        ## OPTIMISATION ----------------------------------------
        best_pos_max = find_best_pos_max(
            array[i], 
            sig_array,
            pos_max=pos_max_i,
            sig_pos_max=sig_pos_max
        )

        chi2[i] = find_best_ampl_numba(
            array[i], 
            sig_array,
            pos_max=pos_max_i,
            sig_pos_max=sig_pos_max,
            pos_max_modif=best_pos_max,
        )
        
    return chi2

def compute_chi2_dataframe_to_sig_optimised(
    dataframe: pd.DataFrame,
    divide_by: str = "std"
) -> np.array:
    """ Compute the chi2 between a dataframe and a signal
    shape. The :math:`\chi^2` is optimised following 2 procedures:

    * the effective position of the max can be shifted from -2 to 2
    * the best amplitude is found by a minimisation of the :math:`\chi^2`,
        for the effective position of the max found at the previous
        step.

    Args: 
        dataframe: dataframe of a ROOT file, that must contain
            (for each event): the 32 samples and `pos_max`.
        
    returns:
        chi2: reduced chi2 between each event and the simulated signal shape
    """
    ## PREPARE THE SIGNAL ARRAY ======================================
    _, sig_array = get_sig_model()
    sig_array = (sig_array - np.mean(sig_array)) / np.max(sig_array)

    sig_array_red = np.array(sig_array, dtype=np.float_)
    zeros_array = np.zeros(shape=defs.n_samples, dtype=np.float_)

    sig_array_red_big = np.concatenate((zeros_array, sig_array_red, zeros_array))

    sig_pos_max = np.argmax(sig_array_red_big)

    ## PREPARE  DATA ==================================================
    array = normalize.get_red_df(
        dataframe, divide_by=divide_by
    )
    array = np.array(array, dtype=np.float_)
    
    pos_max = np.array(dataframe["pos_max"], dtype=np.int_)

    ## COMPUTATION ====================================================
    
    return compute_chi2_array_to_sig_optimised(
        array, sig_array_red_big, pos_max, sig_pos_max
    )