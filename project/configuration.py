""" Configuration of ROOT files.
"""

from copy import deepcopy
from project import defs, cuts
from scripts.clustering.data_cuts import dict_signal_cuts

run_number_dict = {
    1: "00405127",
    2: "00405135",
    3: "00405218",
    4: "00405396",
    5: "00405514",
    6: "00405543",
    7: "00405604"
}

configurations = {}

#############################################################
##################### OLD CONFIGURATIONS ####################
#############################################################

list_voi = defs.computed_variables + \
    ['lower_than_mean_cons', 'larger_than_mean_cons',
     'std_pos', 'std_neg', 'nb_pos', 'nb_neg']

columns_to_compute = [
    'max_m_mean', 'min_m_mean', 'pos_max_m_pos_min',
    'mean_m_pedestal',
    'median_m_mean', 'mean_m_std_m_pedestal',
    'std_pos_m_std_neg',
    'nb_pos_m_nb_neg'
]

all_columns = list_voi + columns_to_compute

configurations["BDT_SN"] = {
    "run": "00405127",
    "num": "0644",
    "cut": "layer==2",
    "list_voi": list_voi,
    "columns_to_compute": columns_to_compute,
    "drop_samples": False,
    "folder": "simulation"
}

configurations["BDT_SNP_layer_0"] = {
    "run": "00405127",
    "num": "0644",
    "cut": "layer==0",
    "list_voi": list_voi,
    "columns_to_compute": columns_to_compute,
    "drop_samples": False,
    "folder": "simulation"
}

configurations["BDT_SNP_layer_1"] = {
    "run": "00405127",
    "num": "0644",
    "cut": "layer==1",
    "list_voi": list_voi,
    "columns_to_compute": columns_to_compute,
    "drop_samples": False,
    "folder": "simulation"
}

configurations["SNP2"] = {
    "run": "00405135",
    "num": "0218",
    "cut": "layer==2",
    "list_voi": list_voi,
    "columns_to_compute": columns_to_compute,
    "discriminate_voi": True,
    "drop_samples": False,
    "folder": "simulation",
}

configurations["BDT_SNP_layer_3"] = {
    "run": "00405127",
    "num": "0644",
    "cut": "layer==3",
    "list_voi": list_voi,
    "columns_to_compute": columns_to_compute,
    "drop_samples": False,
    "folder": "simulation"
}

#############################################################
##################### ONLY ONE ROOT FILE ####################
#############################################################

for layer in [0, 1, 2, 3]:
    configurations[f"run1_num1_layer{layer}"] = {
        "run": "00405127",
        "num": "0644",
        "cut": f"layer=={layer}",
        "list_voi": list_voi,
        "columns_to_compute": columns_to_compute,
        "drop_samples": False,
        "folder": "run1_num1",
        "list_functions_run" : [
            "chi2", "peak_voi", "nb_peaks",
            "std_peaks", "period", "diff_extremum"
        ],
        "after_cuts": ["max - min > 3"]
    }


#############################################################
########################### SIGNAL ##########################
#############################################################

columns_to_compute = [
    'max_m_mean', 'min_m_mean',
    'mean_m_pedestal',
    'median_m_mean',
    'std_pos_m_std_neg',
    'nb_pos_m_nb_neg',
    'min_std_pos_neg',
    'min_std_pos_neg_redpn'
]


list_voi = [
    "pos_max", "std", "pedestal",
    "std_pos", "std_neg",
    "max", "min", "mean",
    "median"
]

columns_to_compute = [
    'max_m_mean',
    'mean_m_pedestal',
    "min_std_pos_neg",
    "min_std_pos_neg_redpn"
]

after_cuts = list(dict_signal_cuts.values()) + ["best_chi2 < 0.015"]

configurations["run1_layer2_signal"] = {
    "run": "00405127",
    "num": None, # All of them !
    "cut": "layer==2",
    "list_voi": list_voi,
    "columns_to_compute": columns_to_compute,
    "drop_samples": False,
    "folder": "run1_signal",
    "list_functions_run" : ["chi2", "peak_voi", "threshold_voi"],
    "after_cuts": after_cuts
}

configurations["run1_layer0_only_lowchi2"] = {
    "run": "00405127",
    "num": None, # All of them !
    "cut": "layer==0",
    "list_voi": list_voi,
    "columns_to_compute": columns_to_compute,
    "drop_samples": False,
    "folder": "run1_signal",
    "list_functions_run" : ["chi2", "peak_voi", "threshold_voi"],
    "after_cuts": ["best_chi2 < 0.015"]
}

configurations["run1_layer1_only_lowchi2"] = {
    "run": "00405127",
    "num": None, # All of them !
    "cut": "layer==1",
    "list_voi": list_voi,
    "columns_to_compute": columns_to_compute,
    "drop_samples": False,
    "folder": "run1_signal",
    "list_functions_run" : ["chi2", "peak_voi", "threshold_voi"],
    "after_cuts": ["best_chi2 < 0.015"]
}

configurations["run1_layer2_only_lowchi2"] = {
    "run": "00405127",
    "num": None, # All of them !
    "cut": "layer==2",
    "list_voi": list_voi,
    "columns_to_compute": columns_to_compute,
    "drop_samples": False,
    "folder": "run1_signal",
    "list_functions_run" : ["chi2", "peak_voi", "threshold_voi"],
    # "after_cuts": ["best_chi2 < 0.015"]
}

configurations["run1_layer3_only_lowchi2"] = {
    "run": "00405127",
    "num": None, # All of them !
    "cut": "layer==3",
    "list_voi": list_voi,
    "columns_to_compute": columns_to_compute,
    "drop_samples": False,
    "folder": "run1_signal",
    # "chi2": True,
    # "peak_voi": True,
    # "threshold_voi": True,
    "list_functions_run" : ["chi2", "peak_voi", "threshold_voi"],
    "after_cuts": ["best_chi2 < 0.015"]
}

configurations_default= {
    "list_voi": list_voi,
    "columns_to_compute": columns_to_compute,
    "drop_samples": False,
    "folder": "tests",
    "list_voi": list_voi,
    "columns_to_compute": columns_to_compute,
    "list_functions_run" : [
        "chi2", "peak_voi", "nb_peaks",
        "std_peaks", "period", "diff_extremum"
    ],
}

configurations["run2_num1"] = deepcopy(configurations_default)
configurations["run2_num1"]["run"] = "00405135"
configurations["run2_num1"]["num"] = "0003"

configurations["run2_num2"] = deepcopy(configurations_default)
configurations["run2_num2"]["run"] = "00405135"
configurations["run2_num2"]["num"] = "0218"


configurations["run3_num1"] = deepcopy(configurations_default)
configurations["run3_num1"]["run"] = "00405218"
configurations["run3_num1"]["num"] = "0443"

configurations["run3_num2"] = deepcopy(configurations_default)
configurations["run3_num2"]["run"] = "00405218"
configurations["run3_num2"]["num"] = "0886"


configurations["run4_num1"] = deepcopy(configurations_default)
configurations["run4_num1"]["run"] = "00405396"
configurations["run4_num1"]["num"] = "0211"

configurations["run4_num2"] = deepcopy(configurations_default)
configurations["run4_num2"]["run"] = "00405396"
configurations["run4_num2"]["num"] = "0473"


configurations["run5_num1"] = deepcopy(configurations_default)
configurations["run5_num1"]["run"] = "00405514"
configurations["run5_num1"]["num"] = "0296"

configurations["run5_num2"] = deepcopy(configurations_default)
configurations["run5_num2"]["run"] = "00405514"
configurations["run5_num2"]["num"] = "0647"


configurations["run6_num1"] = deepcopy(configurations_default)
configurations["run6_num1"]["run"] = "00405543"
configurations["run6_num1"]["num"] = "1826"

configurations["run6_num2"] = deepcopy(configurations_default)
configurations["run6_num2"]["run"] = "00405543"
configurations["run6_num2"]["num"] = "1988"


configurations["run7_num1"] = deepcopy(configurations_default)
configurations["run7_num1"]["run"] = "00405604"
configurations["run7_num1"]["num"] = "0360"

configurations["run7_num2"] = deepcopy(configurations_default)
configurations["run7_num2"]["run"] = "00405604"
configurations["run7_num2"]["num"] = "0705"

#############################################################
#################### SIGNAL & BACKGROUND ####################
#############################################################

basic_config = {
    "num": None, # All of them !
    "drop_samples": False,
    "folder": "general",
}

basic_config_lim = {
    "num": None, # All of them !
    "drop_samples": False,
    "folder": "general",
}

cats = ["signal", "pos_peak", "neg_peak", "oscillating"]

for n, run in run_number_dict.items():
    for cat in cats:
        configurations[f"run{n}_{cat}"] = deepcopy(basic_config)
        configurations[f"run{n}_{cat}"]["run"] = run
        configurations[f"run{n}_{cat}"]["after_cuts"] = \
            cuts.dict_cuts[cat]
        
        # Columns to load
        configurations[f"run{n}_{cat}"]["list_voi"] = \
            cuts.dict_columns[cat].get("voi")
        configurations[f"run{n}_{cat}"]["columns_to_compute"] = \
            cuts.dict_columns[cat].get("columns_to_compute")
        configurations[f"run{n}_{cat}"]["list_functions_run"] = \
            cuts.dict_columns[cat].get("list_functions_run")

        configurations[f"run{n}_{cat}_lim"] = deepcopy(
            configurations[f"run{n}_{cat}"]
        )
        configurations[f"run{n}_{cat}_lim"]["max_nb_events"] = 656000000

        configurations[f"run{n}_{cat}_lim_back"] = deepcopy(
            configurations[f"run{n}_{cat}_lim"]
        )
        configurations[f"run{n}_{cat}_lim_back"]["backward"] = True

    configurations[f"run{n}_small"] = {
        "run": run,
        "num": None, # All of them !
        "frac": 1e-3,
        "seed": 15,
        "drop_samples": False,
        "folder": "general"
    }
    configurations[f"run{n}_small_lim"] = deepcopy(configurations[f"run{n}_small"])
    configurations[f"run{n}_small_lim"]["max_nb_events"] = 656000000

    configurations[f"run{n}_small_lim_back"] = deepcopy(configurations[f"run{n}_small_lim"])
    configurations[f"run{n}_small_lim_back"]["backward"] = True
 