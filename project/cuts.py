""" Define the cuts to select different
categories of events, i.e., signal
and different categories of background.
Define the minimal set of columns to load 
or compute in order to perform these cuts.
"""

dict_cuts = {}
dict_columns = {}

#############################################################
########################### SIGNAL ##########################
#############################################################

dict_cuts["signal"] = [
    "best_chi2 < 0.015",
    "max - min > 3"
]

dict_columns["signal"] = {
    "voi": ["max", "min", "pos_max"],
    "list_functions_run": [
        "chi2"
    ]
}

#############################################################
######################### BACKGROUND ########################
#############################################################

dict_cuts["pos_peak"] = [
    "max - min > 3",
    "best_chi2 > 0.013", # Bad chi2 to remove signal
    "relative_mean < -0.05",
    "std_without_pos_peaks_d_max_m_min < 0.11"
]
dict_columns["pos_peak"] = {
    "voi": ["max", "min", "mean", "pos_max"],
    "columns_to_compute": [
        "max_m_min",
        "std_without_pos_peaks_d_max_m_min",
        "relative_mean"
    ],
    "list_functions_run": [
        "chi2", "std_peaks"
    ]
}

dict_cuts["neg_peak"] = [
    "max - min > 3",
    "best_chi2 > 0.013",
    "relative_mean > 0.05",
    "std_without_neg_peaks_d_max_m_min < 0.11"
]
dict_columns["neg_peak"] = {
    "voi": ["max", "min", "mean", "pos_max"],
    "columns_to_compute": [
        "max_m_min",
        "std_without_neg_peaks_d_max_m_min",
        "relative_mean"
    ],
    "list_functions_run": [
        "chi2", "std_peaks"
    ]
}

dict_cuts["oscillating"] = [
    "max - min > 3",
    "std_period_d_mean_period < 0.18"
]
dict_columns["oscillating"] = {
    "voi": ["max", "min"],
    "columns_to_compute": [
        "std_period_d_mean_period",
    ],
    "list_functions_run": [
        "period"
    ]
}
