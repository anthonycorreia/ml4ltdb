"""
VARIABLES USED IN THIS ANALYSIS

Author: Anthony Correia
Date: 10 Jan. 2021
"""

import os.path as op

from .loc import root_folder

# VARIABLES OF DATAFRAMES ============================================

unproc_kept_variables = [
    'channelId',  # 'channel',
    'layer', 'eta', 'phi', 'ieta', 'iphi',
    'detector', "region",
    'samples',  # , 'n_samples'
    "IEvent"
]

computed_variables = ['mean', 'std', 'max', 'min',
                      'pedestal', 'median', 'pos_max', 'pos_min']

n_samples = 32

layers = [0, 1, 2, 3]

# Nom des 32 colonnes samples,
# correspond au valeur d'ADC à différents instants
# pour un même évènement

# Nom par défaut:
list_samples_columns_old = [f'samples[{i}]' for i in range(n_samples)]
# Renommage:
list_samples_columns = [f's{i}' for i in range(n_samples)]


# PATHS ==========================================================
# unprocessed
unproc_tree_name = "LARDIGITS"

unproc_file_format = f"data21_900GeV.{'[0-9]'*8}"\
    ".physics_L1Calo.merge.RAW."\
    f"_lb{'[0-9]'*4}"\
    "._SFO-ALL._0001.root"

# Files processed
proc_file_name = "run_{run}"
proc_folder = op.join(root_folder, "processed")

proc_tree_name = "tree"

# COLORS ==========================================================

dict_color_clu_labels = {
    -1: 'grey',
    0: 'purple',
    1: 'r',
    2: 'b',
    3: 'orange',
    4: 'g',
    5: "darkolivegreen",
    6: 'aqua',
    7: 'sienna',
    8: 'darkgoldenrod',
    9: 'lime',
    10: 'gold',
    11: 'crimson',
    12: 'indigo',
    13: 'salmon',
    14: 'maroon',
    15: 'black',
    16: "deepskyblue",
    17: "fuchsia"
}
