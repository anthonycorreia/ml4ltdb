""" Labels of the columns 
"""

from tkinter import N


column_labels = {
    'max': r'$\max{e}$',
    'min': r'$\min{e}$',
    'mid': r'$\text{mid}\left(e\right)$',
    'mean': r'$\langle e \rangle$',
    'median': r'$m_e$',
    'std': r'$\sigma_{e}$',
    'mean_m_pedestal': r'$\langle e \rangle - e_0$',
    'lower_than_mean_cons': r"$L_{+}$",
    'larger_than_mean_cons': r"$L_{-}$",
    'pos_max': r"$p_{\text{max}}$",
    'pos_min': r"$p_{\text{min}}$",
    'nb_pos': r"$n_{+}$",
    'nb_neg': r"$n_{-}$",
    'std_pos': r"$\sigma_{e}^{+}$",
    'std_neg': r"$\sigma_{e}^{-}$",
    'pedestal': r"$e_{0}$",
    'min_std_pos_neg': r"$\min{\left(\sigma^{+}_e, \sigma^{-}_e\right)}$",
    'max_std_pos_neg': r"$\max{\left(\sigma^{+}_e, \sigma^{-}_e\right)}$",
    'mean_before_end_peak': r"$\langle e\rangle_{\text{start}}$",
    'mean_after_end_peak': r"$\langle e\rangle_{\text{end}}$",
    'pos_first_neg_after_max': r"$p_{\text{end}}$",
    "nb_max": r"$n_{\text{max}}$",
    "nb_max_m_min_67": r"$\# {\text{max}}$",
    "best_chi2": r"$\chi^2$",
    "eta": r"$\eta$",
    "phi": r"$\phi$",
    "std_d_max_m_min": r"$\frac{\sigma_e}{\max{e} - \min{e}}$"
}

# mean_diff
column_labels["mean2_diff_max"] = r"$\langle \left(e_{\text{max, loc}} - e_{\text{max}}\right)^2\rangle$"
column_labels["mean2_diff_min"] = r"$\langle \left(e_{\text{min, loc}} - e_{\text{min}}\right)^2\rangle$"
column_labels["mean2_diff"] = r"$\langle \left(e_{\text{extr, loc}} - e_{\text{extr}}\right)^2\rangle$"

column_labels["mean_period_max"] = r"$T_{e, \text{max}}$"
column_labels["mean_period_min"] = r"$T_{e, \text{min}}$"
column_labels["std_period_max"] = r"$\sigma_{T_{e, \text{max}}}$"
column_labels["std_period_min"] = r"$\sigma_{T_{e, \text{min}}}$"

column_labels["mean_period"] = r"$\langle T_e \rangle$"
column_labels["std_period"] = r"$\sigma_{T_e}$"

column_labels["nb_pos_peaks"] = r"$\# \text{ up peaks}$"
column_labels["nb_neg_peaks"] = r"$\# \text{ down peaks}$"

column_labels["std_without_pos_peaks"] = r"$\sigma_e \text{ without up peaks}$"
column_labels["std_without_neg_peaks"] = r"$\sigma_e \text{ without down peaks}$"

column_labels["relative_mean"] = r"${\frac{\langle e \rangle - e_{\text{min}}}{e_{\text{max}}- e_{\text{min}}} - 0.5}$"

column_labels["std_period_d_mean_period"] = \
    r"$\frac{" + column_labels["std_period"].replace('$', '') \
        + r"}{" + column_labels["mean_period"].replace('$', '')  \
            + r"}$"

columns_to_compute = [
    'max_m_mean', 'min_m_mean', 'pos_max_m_pos_min',
    'mean_m_pedestal', 'mean_m_pedestal',
    'median_m_mean', 'mean_m_std_m_pedestal',
    'std_pos_m_std_neg', 'nb_pos_m_nb_neg'
]

# How to interprete some "delimiters" in the name of the columns
dict_delimiters = {
    '_m_': ' - '
}

detector_names = {
    0: "Barrel",
    1: "EMEC",
    2: "HEC",
    4: "FCAL",
    3: "Detector 3?"
}