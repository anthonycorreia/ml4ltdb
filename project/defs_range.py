"""Range of some variables for the plus
"""

from numpy import pi

# columns which have 32 possible values
list_time_columns = [
    "pos_max",
]

column_ranges = {
    "eta": [-4.9, 4.9],
    "phi": [-pi, pi]
}
column_nbins = {
    "default": 40,
    "max_m_min": "discretised",
    "nb_pos_peaks": "discretised",
    "nb_neg_peaks": "discretised",    
}

for column in list_time_columns:
    column_ranges[column] = [0, 31]
    column_nbins[column] = 32