"""
Functions used to deal with dataframes.

Author: Anthony Correia
Date: 10 Jan. 2021
"""

from typing import Union, Optional, List, Dict
import time
import glob
import os.path as op

import pandas as pd
import numpy as np
from numba import jit

from lib import data, rootdata as lrd
from . import defs, col
from .configuration import configurations


# PROCESSING ==========================================================

def get_cut(**kwargs) -> Optional[str]:
    """ Get a string that indicates the cut to apply to data.
    It can be directly used with the functions to load a sample.

    Args:
        **kwargs: couple (key, value), such that the cut
            applied to data is `"key==value"`, `key` being
            a column of a ROOT dataframe. If `value` is `None`,
            the cut is not applied.

    Returns:
        cut: cut to apply to data. Can be passed to `uproot4`
            functions.

    Example:
        >>> get_cut(eta=0.05, layer=0)
        "(eta==0.05)&(layer==0)"
    """
    list_cuts = []
    for (column, value) in kwargs.items():
        if value is not None:
            list_cuts.append(f"({column}=={value})")

    if len(list_cuts) >= 1:
        return '&'.join(list_cuts)
    else:
        return None


def process_dataframe(dataframe: pd.DataFrame, verbose: bool = True):
    """ Basic cuts and change of names
    performed on most of the dataframes.

    * Change of variables: `'samples[i]'` becomes `si`
    * Change of values: `-1` becomes `NaN`
    * Remove events with only `NaN` values
    * Remove the defectuous channel `997326336`

    Args:
        dataframe: dataframe to which the processes
            are applied
        verbose: print time and cut efficiency

    Returns:
        processed_dataframe: processed dataframe
    """

    start = time.time()
    if "samples[0]" in dataframe:
        # `'samples[i]'` -> `'si'`
        dataframe = dataframe.rename(
            columns={
                old: new
                for old, new in zip(defs.list_samples_columns_old,
                                    defs.list_samples_columns)
            }
        )

    if "s0" in dataframe:
        # -1 -> NaN
        dataframe[defs.list_samples_columns] = \
            dataframe[defs.list_samples_columns].replace(
            to_replace=-1, value=np.nan
        )

        # Remove NaN
        len_with_nan = len(dataframe)
        dataframe = dataframe.dropna(
            how="all", subset=defs.list_samples_columns)
        len_without_nan = len(dataframe)       

        if verbose:
            print("Remove NaN events")
            print(
                f"{len_with_nan-len_without_nan} events removed out of {len_with_nan}",
                f"-> efficiency {(len_with_nan - len_without_nan) / len_with_nan}"
            )


    # Remove channel Id 997326336
    assert "channelId" in dataframe
    dataframe = dataframe.query("channelId!=997326336")

    if verbose:
        print(f"Time taken: {time.time() - start}")

    return dataframe


def randomise_discretisation(
    dataframe: pd.DataFrame,
    list_samples_columns: List[str] = None,
    rng: Optional[np.random.Generator] = None
):
    """ Add a random number between -0.5
    and 0.5 to each sample. In-place.

    Args:
        dataframe: data sample with the
            32 samples
        list_samples_columns: list of
            column names of the
            32 samples.
        rng : random generator
    """
    if list_samples_columns is None:
        list_samples_columns = defs.list_samples_columns
    if rng is None:
        rng = np.random.default_rng()

    dataframe[list_samples_columns] += \
        rng.uniform(
            low=-0.5, high=0.5,
            size=(
                len(dataframe),
                len(list_samples_columns)
            )
    )

def compute_voi(
        dataframe: pd.DataFrame,
        variables: Optional[Union[str, List[str]]] = None,
        verbose: bool = True):
    """ Compute the variables of interest
    and store them in the dataframe. For each event and cell:

    * `'mean'`: time average
    * `'std'`: standard deviation
    * `'max'` maximal value recorded
    * `'min'`: minimal value recorded
    * `'median'`: median
    * `'pos_min'`: index of the min value
    * `'pos_max'`: index of the max value
    * `'mid'`: mid point

    A time comparison has been performed between different way
    of computing theses variables. This function uses
    the optimal choices.

    Args:
        dataframe: dataframe which the variables of interest
            are computed for
        variables: variables to compute and add to the dataframe
        verbose: print computation time

    Returns:
        processed_dataframe: dataframe with the new variables
    """
    if variables is None:
        variables = defs.computed_variables
    start = time.time()

    df_values = dataframe[defs.list_samples_columns]
    array_values = np.asarray(df_values, dtype='float64')

    if isinstance(variables, str):
        variables = [variables]

    if 'pedestal' in variables:
        dataframe['pedestal'] = df_values[
            defs.list_samples_columns[0]
        ]

    if 'mean' in variables or 'std' in variables:
        dataframe['mean'] = df_values.mean(axis=1)

    if 'std' in variables:
        # Computation of the std by hand (using numpy functions)
        # is far faster
        # Probably because we don't need to recompute the mean
        dataframe['std'] = np.sqrt(
            np.mean(
                np.square(array_values), axis=1
            )
            - dataframe['mean']**2
        )

    if 'std_pos' in variables or 'nb_pos' in variables \
            or 'std_neg' in variables or 'nb_neg' in variables:

        array_values0 = array_values - \
            data.get_1D_to_2D(array_values[:,0])

    if 'std_pos' in variables or 'nb_pos' in variables:
        array_values0_pos = array_values0.copy()
        array_values0_pos[array_values0 < 0] = 0

    if 'std_neg' in variables or 'nb_neg' in variables:
        array_values0_neg = array_values0.copy()
        array_values0_neg[array_values0 > 0] = 0

    if 'std_pos' in variables:
        dataframe['std_pos'] = np.sqrt(
            np.mean(
                np.square(array_values0_pos), axis=1
            )
        )

    if 'std_neg' in variables:
        dataframe['std_neg'] = np.sqrt(
            np.mean(
                np.square(array_values0_neg), axis=1
            )
        )

    if 'nb_pos' in variables:
        dataframe['nb_pos'] = np.count_nonzero(array_values0_pos, axis=1)
    if 'nb_neg' in variables:
        dataframe['nb_neg'] = np.count_nonzero(array_values0_neg, axis=1)

    if 'max' in variables:
        dataframe['max'] = df_values.max(axis=1)
    if 'min' in variables:
        dataframe['min'] = df_values.min(axis=1)

    # Computation of the index of min and max undeniably faster with numpy (/10)
    if 'pos_max' in variables:
        dataframe['pos_max'] = np.argmax(array_values, axis=1)
    if 'pos_min' in variables:
        dataframe['pos_min'] = np.argmin(array_values, axis=1)

    if 'median' in variables:
        # Computation of the median far faster with numpy (/4)
        dataframe['median'] = np.median(array_values, axis=1)

    if 'larger_than_mean_cons' in variables:
        mask = array_values > data.get_1D_to_2D(dataframe['mean'])
        dataframe['larger_than_mean_cons'] = compute_max_consec_true_mask(mask)

    if 'lower_than_mean_cons' in variables:
        mask = array_values < data.get_1D_to_2D(dataframe['mean'])
        dataframe['lower_than_mean_cons'] = compute_max_consec_true_mask(mask)

    if 'mid' in variables:
        dataframe['mid'] = (dataframe['max'] + dataframe['min']) / 2

    if verbose:
        print(f"Time taken to compute the voi: {time.time() - start}")

    return dataframe


# PATHS ===========================================================

def get_paths_run(run: str, root_folder: str = defs.root_folder,
                  file_format: str = defs.unproc_file_format
) -> List[str]:
    """ Get the list of file paths to open for a given RUN.

    Args:
        run: run to load
        root_folder: where the ROOT files are located.
            The folder should contain folders whose names
            are the names of the runs.
        file_format: format of the files to look for
            in `{root_folder}/{run}/atlas/`

    Returns:
        list_paths: list of the paths of the ROOT files
            for a given `run`.
    """
    file_path = f"{run}/atlas/"
    list_paths = glob.glob(
        op.join(root_folder, file_path, file_format))

    return list_paths


def get_path_run_num(run: str, num: str, **kwargs) -> str:
    """ Get the file path of a given run and given number

    Args:
        run: run to load
        num: number of ROOT file to load
        *args, **kwargs: passed to  :py:func:`get_paths_run`

    Returns:
        path: list of the path of the ROOT file for
            a given `run` and `num`

    Raises:
        AssertionError: If more than file is found,
            or no file is found.
    """
    file_format = f"data21_900GeV.{'[0-9]'*8}"\
        ".physics_L1Calo.merge.RAW."\
        f"_lb{num}._SFO-ALL._0001.root"
    print(defs.root_folder)
    list_paths = get_paths_run(
        run=run,
        file_format=file_format,
        **kwargs
    )

    assert len(list_paths) != 0, f"No files was found: {file_format}"

    assert len(list_paths) == 1, "The number of files that match "\
        f"the description is larger than 1: {list_paths}"

    return list_paths[0]


def get_path_run_processed(run: str, end: str = "") -> str:
    """ Path of a processed ROOT file

    Args:
        run: name of the run
        end: to add at the end of the name of
            the file

    Returns:
        path: path of the processed ROOT file
    """

    return op.join(defs.proc_folder,
                   defs.proc_file_name.format(run=run) + end + ".root"
                   )


def get_path_processed_root(name: str) -> str:
    """Get the path of a ROOT file saved under the name
    `name`, and whose configuration is given in
    `project/configuration.py`

    Args:
        name: name of the ROOT file
            (also gives its configuration via
            the dictionnary `configurations`
            in `project/configuration.py`)

    Returns:
        path of the ROOT file
    """
    configuration = configurations[name]
    folder = configuration.get("folder")

    if folder is not None:
        full_folder = op.join(defs.proc_folder, folder)
    else:
        full_folder = defs.proc_folder

    return op.join(full_folder, name + ".root")


@jit(nopython=True)
def compute_max_consec_true_mask(mask: np.ndarray) -> np.array:
    """ Compute the maximum number of consecutive occurences of
    `True` in each row of an array. It is run with :py:module:`numba`

    Args:
        mask: array of 0 and 1 (or `True` and `False`)

    Returns:
        max_length_array: array, whose element `i` is the
            the maximum number of consecutive occurences of
            `True` in the row `i` of `mask`.
    """

    max_length_array = np.empty(shape=mask.shape[0])

    for i in range(mask.shape[0]):
        sample = mask[i]
        max_length = 1
        current_length = 0
        for j in range(sample.shape[0]):
            if sample[j]:
                current_length += 1
            else:
                max_length = max(max_length, current_length)
                current_length = 0

        max_length_array[i] = max_length

    return max_length_array


## DISCRIMINATIVE VOI ============================================

@jit(nopython=True)
def calc_pos_first_neg_after_max(
    samples_array0: np.ndarray, pos_max: np.array
) -> np.array:
    """ For each event, look for the position of the first zero
    after the max.

    Args:
        samples_array0: two-dimensional array whose
            rows are events, and columns are the signal shape in the
            form of 32 values at different time step.
            The pedestal must have been substracted from the sample,
            so that 0 corresponds to a true zero.
        pos_max: Position of the maximum for each event.

    Returns:
        pos_zero_after_max: position (0 between 31) of the first
            negative value after the maximum.
            The value is set to -1 if no zero was found
            (for a given event)
    """
    pos_first_neg_after_max = np.zeros(len(samples_array0), dtype=np.int_)
    for i in range(len(samples_array0)): # loop over the events
        j = pos_max[i] + 1
        while samples_array0[i][j] > 0 and j < defs.n_samples:
            j+=1

        if j == defs.n_samples:
            pos_first_neg_after_max[i] = -1
        else:
            pos_first_neg_after_max[i] = j

    return pos_first_neg_after_max

@jit(nopython=True)
def calc_nb_peaks_above_limit(
    samples_array: np.ndarray, limit: np.array,
    large_peaks: bool = True

)->np.array:
    """ For each event, compute the number
    of peaks reaching a limit value.

    Args:
        samples_array: two-dimensional array whose
            rows are events, and columns are the signal shape in the
            form of 32 values at different time step.
            The pedestal must have been substracted from the sample,
            so that 0 corresponds to a true zero.
        limit: limit value for each event
        large_peaks: whether a large peak (several time points)
            is considered as one peak or or several peaks


    Returns:
        nb_events_above_limit: number of peaks above
            (or equal to) a limit value, for each
            event.
    """
    nb_events_above_limit = np.zeros(len(samples_array), dtype=np.int_)
    for i in range(len(samples_array)): # loop over the events
        j = 0
        while j < defs.n_samples:
            if samples_array[i][j] >= limit[i]:
                nb_events_above_limit[i] += 1
                j += 1
                if large_peaks:
                    while samples_array[i][j] >= limit[i] and j < defs.n_samples:
                        j += 1
            else:
                j += 1

    return nb_events_above_limit



@jit(nopython=True)
def calc_mean_around_pos(
    samples_array: np.ndarray, pos: np.array, before: bool
)->np.array:
    """ Compute the mean of the events (in `samples_array`)
    before or after some position `pos`.

    Args:
        samples_array: two-dimensional array whose
            rows are events, and columns are the signal shape in the
            form of 32 values at different time step.
        pos: array of positions (between 0 and 31)
        before: whether to compute the mean before a position.
            if `False`, the mean is computed after the position
            given in `pos`

    Returns:
        mean_around_pos: mean of each event before or after
            the specified position
    """
    mean_around_pos = np.zeros(len(samples_array), dtype=np.float_)

    for i in range(len(samples_array)): # loop over events

        if pos[i] != -1:
            if before:
                start = 0
                stop = pos[i] # excluded
            else:
                start = pos[i]
                stop = defs.n_samples # excluded

            mean_around_pos[i] = np.mean(samples_array[i][start:stop])
        else:
            mean_around_pos[i] = np.nan

    return mean_around_pos

def compute_peak_voi(
    dataframe: pd.DataFrame,
    zero_column: str = "pedestal",
    red: bool = False
):
    """ Compute new sorts of variable of interest,
    related to the position of the end of the peak.

    Args:
        dataframe: dataframe where to add the new voi
        zero_column: column to substract the samples with,
            before computation of the vois. It is considered
            as the "zero" of the event.
            It is basically the pedestal, but could be
            the mean.
        red: whether to compute the variables with the samples
            divided by the standard deviation
    """

    ## Compute required variables ===================================
    columns_to_compute = []
    if 'pos_max' not in dataframe:
        columns_to_compute.append('pos_max')
    if zero_column not in dataframe:
        columns_to_compute.append('pedestal')
    if red and 'std' not in dataframe:
        columns_to_compute.append('std')
    compute_voi(dataframe, variables=columns_to_compute)

    ## Numpy arrays =================================================
    samples_array = np.array(
        dataframe[defs.list_samples_columns],
        dtype=np.float_
    )
    if zero_column is not None:
        samples_array0 =  samples_array - \
            np.array(dataframe[zero_column], dtype=np.float_).reshape(
                len(samples_array), 1
            )
    else:
        samples_array0 = samples_array

    if red:
        samples_array0_red = samples_array0 / np.array(
            dataframe['std'], dtype=np.float_
        ).reshape(len(samples_array), 1)
    else:
        samples_array0_red = samples_array0

    pos_max_array = np.array(dataframe['pos_max'], dtype=np.int_)

    ## Compute new variables ===========================================
    pos_first_neg_after_max = calc_pos_first_neg_after_max(
        samples_array0=samples_array0_red,
        pos_max=pos_max_array
    )

    dataframe['pos_first_neg_after_max'] = pos_first_neg_after_max

    dataframe['mean_before_end_peak'] = calc_mean_around_pos(
        samples_array=samples_array0_red,
        pos=pos_first_neg_after_max,
        before=True
    )

    dataframe['mean_after_end_peak'] = calc_mean_around_pos(
        samples_array=samples_array0_red,
        pos=pos_first_neg_after_max,
        before=False
    )

def compute_threshold_voi(
    dataframe: pd.DataFrame,
    ref_column: str = "pedestal", threshold: float = 0.67):
    """ Compute new voi, related to a threshold set w.r.t.
    the amplitude.

    Args:
        dataframe: dataframe where to add the new voi
        ref_column: reference to which the threshold is set
            (w.r.t. to the max)
        threshold: value of the threshold, between 0 and 1.
    """
    ## Compute required variables =================================
    columns_to_compute = []
    if ref_column not in dataframe:
        columns_to_compute.append(ref_column)
    compute_voi(dataframe, variables=columns_to_compute, verbose=False)

    ## Numpy arrays ================================================
    samples_array_ref = np.array(
        dataframe[defs.list_samples_columns], dtype=np.float_) \
            - np.array(dataframe[ref_column], dtype=np.float_).reshape(
                len(dataframe), 1
            )

    max_m_ref = np.array(
        dataframe['max'] - dataframe[ref_column],
        dtype=np.float_
    )

    threshold_percent = round(threshold*100)

    dataframe[f'nb_peaks_sup_{threshold_percent}_wrt_{ref_column}'] = calc_nb_peaks_above_limit(
        samples_array=samples_array_ref,
        limit=max_m_ref * threshold,
    )

def load_and_process_dataframe(
    path: str,
    list_voi: Optional[List[str]] = None,
    columns: Optional[List[str]] = None,
    other_columns: List[str] = None,
    branches: List[str] = None,
    rd: bool = False,
    verbose: bool = False,
    rng: Optional[np.random.Generator] = None,
    tree_name: Optional[str] = defs.proc_tree_name,
    process: bool = False,
    **kwargs
)-> Union[pd.DataFrame, Dict[str, str]]:
    """ Load a ROOT dataframe and compute
    all the required variables. Don't compute the
    ones already computed.

    Args:
        path: path of the ROOT file (only one)
        list_voi: list of the variables of interest,
            and other variables already included in the
            dataframe (s.t. `region`, etc.)
        columns: list of the computed columns
            (from the voi)
        other_columns: threshold or peak voi, or
            :math:`\\chi^2`
        branches: list of the variables already in the
            dataframe since the dawn of time
        rd: whether a random number between -0.5 and
            0.5 is added to each of the `samples`
        verbose: print what is going on
        rng : random generator (for randomising the discretisation)
        kwargs: passed to
            :py:func:`lib.data.load_ROOT_into_dataframe`

    Returns:
        dataframe: data with the required columns
        column_labels: associates a column name with its (latex)
            label
    """

    if list_voi is None:
        list_voi = []
    if columns is None:
        columns = []
    if other_columns is None:
        other_columns = []
    if branches is None:
        branches = []

    for branch in branches:
        assert branch in branches, f"{branch} is not in the dataframe"

    if rd:
        # Need to compute everything again !
        variables_to_load = branches
        list_voi_to_compute = list_voi
        columns_to_compute = columns

        # Compute peak_voi, threshold_voi or chi2, if
        # they are in `other_columns`
        _, list_functions_run = col.get_other_columns_to_compute(
            other_columns, []
        )
    else:
        # Only compute what is not already in the dataframe
        df_columns = lrd.get_ROOT_file_branches(path, tree_name=tree_name)

        # Voi ------
        list_voi_to_load, list_voi_to_compute = \
            lrd.get_columns_to_compute_and_computed(
                list_voi, df_columns
            )

        columns_to_load, columns_to_compute = \
            lrd.get_columns_to_compute_and_computed(
                columns, df_columns
            )

        other_columns_to_load, list_functions_run = \
            col.get_other_columns_to_compute(
                other_columns, df_columns
            )


        variables_to_load = branches \
            + list_voi_to_load + columns_to_load \
                + other_columns_to_load


    dataframe = lrd.load_ROOT_into_dataframe(
        path,
        tree_name=tree_name,
        variables=variables_to_load,
        verbose=verbose,
        **kwargs
    )

    if process:
        dataframe = process_dataframe(dataframe, verbose=verbose)

    if rd:
        if verbose:
            print("RANDOMISE DISCRETISATION")
        randomise_discretisation(dataframe, rng=rng)

    if verbose:
        print("COMPUTE VOIs")
    if "std" not in list_voi_to_compute:
        list_voi_to_compute.append("std")
    compute_voi(
        dataframe,
        variables=list_voi_to_compute
    )
    if verbose:
        print("Remove events with std == 0")
    dataframe.query("std != 0", inplace=True)

    ## COLUMNS ==========================================
    if verbose:
        print("COMPUTE COLUMNS (not already computed)")

    column_labels = col.compute_all_columns(
        dataframe,
        columns_to_compute,
        list_functions_run=list_functions_run
    )

    ## COLUMN LABELS ====================================
    column_labels_computed  = col.get_formula_labels(
        columns, column_labels
    )
    column_labels = {**column_labels, **column_labels_computed}

    return dataframe, column_labels
