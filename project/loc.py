"""
Locations that the end user can tangle with.

Author: Anthony Correia
Date: 12 January 2021
"""

import os
ANAROOT = os.environ.get('ANAROOT')

## Path where the ROOT files are located
root_folder = "/mnt/c/900GeV/"

## Path where the output files (pictures, tables) are saved in
out_folder = "/mnt/d/900GeV//output/"
images_folder = f"{out_folder}/images/"
pickle_folder = f"{out_folder}/pickle/"
json_folder = f"{out_folder}/json/"
table_folder = f"{out_folder}/table/"
