"""
Functions used to deal with dataframes.

Author: Enzo Merzoug
Date: 24 Feb. 2022
"""

from typing import Union, Optional, List
from lib.utils import array_like
from lib.data import get_1D_to_2D
import time
import glob
import os.path as op

import pandas as pd
import numpy as np
from numba import jit

from . import defs
from .configuration import configurations

from . import files, defs, col
from numba import jit


def normalise_samples(
    df: pd.DataFrame,
    list_samples_columns: List[str] = defs.list_samples_columns,
    zero_column: str = "pedestal"
)-> pd.DataFrame:
    """ Merely gets the mean to 0 and std to 1.
    The function is inplace.

    Args:
        df: dataframe which needs to be normalised
        list_samples_columns: list of the column names for
            the signal.
        zero_column: column name of the value correspond to 0,
            this is basically the `pedestal`
    """

    df[list_samples_columns] -= np.array(df[zero_column]).reshape(
        len(df), 1
    )
    df[list_samples_columns] /= np.array(df["std"]).reshape(
        len(df), 1
    )
        

def normalise_df(
    df: pd.DataFrame
)-> pd.DataFrame:
    """ Normalise a dataframe : gets the mean to 0 and std to 1.
    The function is inplace.

    Args:
        df: dataframe which needs to be normalised

    Returns:
        df: dataframe normalised
    """
    df = files.compute_voi(df, variables = ['mean', 'std'])
    df.query('std != 0')
    # df['mean'] = -df['mean']

    # for s in defs.list_samples_columns :
    #     df[s] = df[[s, 'mean']].sum(axis=1)
    #     df[s] = df[s]/df['std']
    normalise_samples(df, zero_column="mean")

    df = df.rename(columns = {'mean' : 'old_mean', 'std' : 'old_std'})
    # df['old_mean'] = -df['old_mean']


    return df

@jit(nopython=True)
def get_array_pos_max_lined_up(
    samples_array: np.ndarray, pos_max: np.array,
    pos_max_opt: np.int_
)->np.ndarray:
    """ Line up the positions of the max 
    by circular permutation of the samples. the common
    position of the max is the average of all the 
    `pos_max` of the `dataframe`.

    The position of the max must be already computed and
    given in `pos_max`.

    Args:
        samples array: with position of max not the same
            for each event.
        pos_max: computed positions of the max for each event.
            (in order not to have to recompute it)
        pos_max_opt: where to line up the `pos_max`

    Returns:
        samples_array_rolled: with position of max aligned.
    """
    samples_array_rolled = np.zeros_like(samples_array, dtype=np.float_)
    
    for i in range(len(samples_array)):
        samples_array_rolled[i] = np.roll(
            samples_array[i], pos_max_opt - pos_max[i]
        )

    return samples_array_rolled

def get_dataframe_pos_max_lined_up(
    dataframe: pd.DataFrame, inplace: bool = True,
    pos_max_opt: Optional[int] = None
)->Optional[pd.DataFrame]:
    """ Line up the positions of the max 
    by circular permutation of the samples. the common
    position of the max is the average of all the 
    `pos_max` of the `dataframe`.

    Args:
        dataframe: dataframe that contains the samples
        inplace: whether to make the change directly in
            the dataframe, or return a copied dataframe
            with the new samples.
        pos_max_opt: where to line up the `pos_max`
        
    Returns:
        dataframe_rolled: only if not `inplace`
    """
    sample_array = np.array(
        dataframe[defs.list_samples_columns], dtype=np.float_
    )

    if 'pos_max' not in dataframe:
        files.compute_voi(dataframe, variables=['pos_max'])
    
    if pos_max_opt is None:
        pos_max_opt = round(np.mean(dataframe['pos_max']))

    samples_array_rolled = get_array_pos_max_lined_up(
        sample_array,
        pos_max=np.array(dataframe['pos_max'], dtype=np.int_),
        pos_max_opt=pos_max_opt
    )
    
    if inplace:
        dataframe[defs.list_samples_columns] = samples_array_rolled

    else:
        dataframe_rolled = dataframe.copy(deep=True)
        dataframe_rolled[defs.list_samples_columns] = samples_array_rolled
        return dataframe_rolled



def get_red(
    array: array_like, 
    mean: Union[float, array_like], 
    std: Union[float, array_like]
)->array_like:
    """Substract an array by the mean and divide it by
    the standard deviation.

    Args:
        array: a 1D or 2D array-like
        mean: the mean of the 1D array or a 1D array
            or the mean of each line of the 2D array
        std: the std of the 1D array a 1D of the
            std of each line of the 2D array
    """
    array = np.asarray(array)
    if len(np.shape(array)) == 2:        
        mean = get_1D_to_2D(mean)
        if std is not None:
            std = get_1D_to_2D(std)

    if std is None:
        std = 1
    return (array - mean) / std

def get_red_df(
    df: Union[pd.DataFrame, pd.Series],
    return_pd: bool = False,
    divide_by: str = "std"
)->Union[np.ndarray, np.array]:
    """Substract a dataframe or Series 
    by the mean and divide it by the standard deviation.
    The mean and standard deviation are stored in the
    dataframe.

    Args:
        df: a dataframe or pandas series. The mean and
        standard deviation are stored in the columns 
        `mean` and `std`, respectively.
        return_pd: if a pandas object is returned, or
            simply a numpy array
        divide_by: what to divide the samples by.
    
    Returns:
        reduced_df: reduced dataframe
    """

    red_df = get_red(
        df[defs.list_samples_columns],
        df["mean"],
        df[divide_by]
    )

    if return_pd:
        if len(np.shape(red_df)) == 1:
            red_df = get_1D_to_2D(red_df).reshape(1, len(red_df))

        red_df = pd.DataFrame(red_df, columns=defs.list_samples_columns)
        if isinstance(df, pd.Series):
            return red_df.iloc[0]
    return red_df