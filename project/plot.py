"""
Data-oriented plotting functions.
"""

from lib.utils import array_like, cut_into_name
from typing import Union, Optional, Dict

import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd

from lib import plot as lp
from project import defs_label

def plot_hist_each_layer(
    dataframes,
    var1: array_like,
    var2: Optional[array_like] = None,
    column_labels: Optional[Dict]= None,
    column_ranges: Optional[Dict] = None,
    column_nbins: Optional[Dict] = None,    
    same_fig: bool = True,
    path: Optional[str] = None,
    name: Optional[str] = None,
    cut: Optional[str] = None,
    **kwargs
)->Union[mpl.figure.Figure, mpl.axes.Axes]:
    """ Plot 2D histograms for each layer (in separate plots)
    with a title indicating which layer is being plotted.

    Args:
        dataframes: associates a layer (`0`, `1`, `2` or `3`)
            with a dataframe
        var1, var2: the 2 variables to plot.If  `var2` not
            is not given, a 1D histogram is plotted
        column_labels: associates a column with its label
        column_ranges, column_nbins: associates a column with
            its range and number of bins.
        same_fig: plot all the histograms in the same figure,
            and put titles to distinguish them
        kwargs: passed to :py:func:`lib.plot.plot_hist` or
            :py:func:`lib.plot.plot_hist2d`
        path, name: path and name of the file to save.
            If `name` is not given, the figure is not saved.
        cut: cut to apply to the dataframes before plotting
        kwargs: passed to :py:func:`lib.plot.plot_hist`
            or :py:func:`lib.plot.plot_hist2d`

    Returns:
        fig, axes
    """

    if column_labels is None:
        column_labels = defs_label.column_labels

    layers = list(dataframes.keys())


    if len(layers)==1:
        axes = [axes]

    if same_fig:
        fig, axes = plt.subplots(
            1, len(layers), figsize=(8*len(layers), 6)
        )

    kwargs_suppl = {}
    if isinstance(column_ranges, dict):
        if var2 is None:
            if var1 in column_ranges:
                kwargs_suppl["min_max"] = column_ranges[var1]
            if var1 in column_nbins:
                kwargs_suppl["bins"] = column_nbins[var1]
        else:
            if var1 in column_ranges or var2 in column_ranges:
                kwargs_suppl["min_max"] = [None, None]
                if var1 in column_ranges:
                    kwargs_suppl["min_max"][0] = column_ranges[var1]
                if var2 in column_ranges:
                    kwargs_suppl["min_max"][1] = column_ranges[var2]

            if var1 in column_nbins or var2 in column_nbins:
                kwargs_suppl["bins"] = [None, None]
                if var1 in column_nbins:
                    kwargs_suppl["bins"][0] = column_nbins[var1]
                if var2 in column_nbins:
                    kwargs_suppl["bins"][1] = column_nbins[var2]

    if not same_fig:
        axes = []

    if cut is not None:
        cut_name = cut_into_name(cut)

    def get_cut_var(
        var: str, dataframe: pd.DataFrame, cut: str = None
    ) -> pd.DataFrame:
        """
        Get the variable in the cut dataframe 
        """
        if cut is None:
            return dataframe[var]
        else:
            return dataframe.query(cut)[var]

    for i, layer in enumerate(layers):
        if len(dataframes[layer])!=0:
            if not same_fig:
                fig, ax = plt.subplots(
                    figsize=(8, 6)
                )
                axes.append(ax)
            else:
                ax = axes[i]

            if var2 is None:

                lp.plot_hist(
                    get_cut_var(var1, dataframes[layer], cut), 
                    column_labels[var1],
                    ax=ax,
                    **kwargs,
                    **kwargs_suppl
                )
            else:
                lp.plot_hist2d(
                    get_cut_var(var1, dataframes[layer], cut),
                    get_cut_var(var2, dataframes[layer], cut),
                    column_labels[var1], column_labels[var2],
                    ax=ax,
                    fig=fig,
                    **kwargs,
                    **kwargs_suppl
                )
            if same_fig:
                ax.set_title(f"Layer {layer}")
            else:
                if name is not None:
                    full_name = name + f"_layer{layer}"
                    if cut is not None:
                        full_name += "_" + cut_name
                    lp.save_fig(fig, path, full_name)
                    plt.close(fig)

    if same_fig and name is not None:
        if cut is not None:
            full_name = f"{name}_{cut_name}"
        else:
            full_name = name
        lp.save_fig(fig, path, full_name)
        plt.close(fig)


    plt.tight_layout()
    return fig, axes
