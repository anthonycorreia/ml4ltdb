""" Compute variables of interest
specialise to isolate some
background components (oscillating, squared, peaked)
"""

##############################################################
########################## LIBRARIES #########################
##############################################################

from typing import List, Union

from numba import jit
import numpy as np
import pandas as pd

from lib import data

from . import defs, files


##############################################################
####################### GENERAL-PURPOSE ######################
##############################################################

@jit(nopython=True)
def is_local_extremum(
    sample_array: np.array,
    index: np.ndarray,
    wrt_max: bool = True
)-> bool:
    """ Check whether the index `index` of an array
    is a local extremum (max or min).

    Args:
        sample_array: one-dimension array
        index: index in `sample_array`
        wrt_max: whether to check for local maxima
            (or local minima)

    Returns:
        is_extremum: whether `index` is indeed a local
            maximum or minimum of `sample_array`
    """
    is_extremum = False
    j = index
    if j == 0:
        if wrt_max:
            is_extremum = sample_array[j] >= sample_array[j+1]
        else:
            is_extremum = sample_array[j] <= sample_array[j+1]
    elif j == len(sample_array) - 1:
        if wrt_max:
            is_extremum = sample_array[j] >= sample_array[j-1]
        else:
            is_extremum = sample_array[j] <= sample_array[j-1]
    else:
        if wrt_max:
            is_extremum = sample_array[j] >= sample_array[j-1]
            is_extremum = is_extremum and \
                sample_array[j] >= sample_array[j+1]
        else:
            is_extremum = sample_array[j] <= sample_array[j-1]
            is_extremum = is_extremum and \
                sample_array[j] <= sample_array[j+1]

    return is_extremum

##############################################################
########################### SQUARED ##########################
##############################################################

@jit(nopython=True)
def calc_mean_diff_extremum(
    samples_array: np.ndarray,
    extremum_array: np.ndarray,
    wrt_max: bool = True
)->np.array:
    """ Compute the average squared difference of the
    local maxima w.r.t. the global maximum.

    Args:
        samples_array: two-dimensional array whose
            rows are events, and columns are the signal shape in the
            form of 32 values at different time step.
        extremum_array: value of the global extremum of each event.
            (one could change this function to take the position of the
            max of the min)
        wrt_max: whether the maxima, or the minima are
            considered

    Returns:
        mean_diff_extremum: average squared difference between
            the local maxima and the global maximum, for each event.
    """
    mean_diff_extremum = np.zeros(len(samples_array), dtype=np.float_)


    for i in range(len(samples_array)): # loop over events
        nb = 0
        for j in range(len(samples_array[i])):

            local_extremum = is_local_extremum(
                samples_array[i], j, wrt_max=wrt_max
            )

            if local_extremum:
                if wrt_max:
                    mean_diff_extremum[i] += (extremum_array[i] \
                        - samples_array[i][j])**2
                else:
                    mean_diff_extremum[i] += (samples_array[i][j] \
                        - extremum_array[i])**2

                nb += 1

        if nb != 0:
            mean_diff_extremum[i] /= nb

    return mean_diff_extremum

def compute_mean2_diff_extremum_df(
    dataframe: pd.DataFrame,
    red_column: str = 'max_m_min',
    wrt: Union[List[str], str] = None
):
    """ Compute the mean squared difference between
    the global extremum and the local extrema.
    In-place: the variable `mean_diff_max` (if `wrt_max`) or
    `mean_diff_min` (otherwise) is directly added to the
    dataframe.

    Args:
        dataframe: dataframe where to add the new voi
        red_column: column to divide the samples with,
            before computation of this voi. This
            is used for standardisation accross the events
        wrt: list, can contain `"max"` (the extrema are
            the max), `"min"` (the extrema are the min) and
            "mean" (the mean result between "max" and `"min"`
            is evaluated). By default, compute all  of
            those.
    """

    if wrt is None:
        wrt = ["max", "min", "mean"]
    if isinstance(wrt, str):
        wrt = [wrt]

    if red_column == "max_m_min" and "max_m_min" not in dataframe:
        dataframe["max_m_min"] = dataframe["max"] - dataframe["min"]
    ## Numpy arrays =====================================
    samples_array = np.array(
        dataframe[defs.list_samples_columns],
        dtype=np.float_
    )

    samples_array_red = samples_array / data.get_1D_to_2D(
        dataframe[red_column]
    )

    ## Compute new variables =============================

    for wrt_column in wrt:
        if wrt_column == "max" or wrt_column == "min":
            wrt_max = (wrt_column == "max")

            dataframe[f'mean2_diff_{wrt_column}'] = \
                calc_mean_diff_extremum(
                    samples_array=samples_array_red,
                    extremum_array=np.array(
                        dataframe[wrt_column] / \
                            dataframe[red_column],
                        dtype=np.float_
                    ),
                    wrt_max=wrt_max
                )

        elif wrt_column == "mean":
            dataframe["mean2_diff"] = \
                (dataframe["mean2_diff_max"] + \
                    dataframe["mean2_diff_min"]) / 2

        else:
            raise ValueError(f"wrt contains {wrt_column}")

##############################################################
######################### OSCILLATING ########################
##############################################################

@jit(nopython=True)
def calc_mean_period_extremum(
    samples_array: np.ndarray,
    wrt_max: bool
)->np.array:
    """ Compute the average difference of the position of
    local extrema (i.e., a "mean period").

    Args:
        samples_array: two-dimensional array whose
            rows are events, and columns are the signal shape
            in the form of 32 values at different time step.
        wrt_max: whether the maxima, or the minima are
            considered

    Returns:
        mean_period_extremum: mean difference between nearest
            local maxima
    """
    mean_period_extremum = np.zeros(
        len(samples_array), dtype=np.float_
    )

    for i in range(len(samples_array)): # loop over events
        nb = 0
        previous_extremum_pos = -1
        for j in range(len(samples_array[i])):
            local_extremum = is_local_extremum(
                samples_array[i], j, wrt_max=wrt_max
            )

            if local_extremum:
                if local_extremum and previous_extremum_pos!=-1:
                    mean_period_extremum[i] += \
                        (j - previous_extremum_pos)
                    nb += 1

                previous_extremum_pos = j

        if nb != 0:
            mean_period_extremum[i] /= nb

    return mean_period_extremum


@jit(nopython=True)
def calc_mean_discrepancy_period_extremum(
    samples_array: np.ndarray,
    mean_period_extremum : np.array,
    wrt_max: bool
)->np.array:
    """ Compute the standard deviation from the
    mean period.

    Args:
        samples_array: two-dimensional array whose
            rows are events, and columns are the signal
            shape in the form of 32 values at different
            time step.
        mean_period_extremum: for each event, its average
            period.

    Returns:
        mean2_discrepancy_period_extremum: mean squared
            difference between the mean period and the
            local periods, for each event
    """
    mean2_discrepancy_period_extremum = np.zeros(
        len(samples_array), dtype=np.float_
    )

    for i in range(len(samples_array)): # loop over events
        nb = 0
        previous_extremum_pos = -1
        for j in range(len(samples_array[i])):
            local_extremum = is_local_extremum(
                samples_array[i], j, wrt_max=wrt_max
            )

            if local_extremum:
                if local_extremum and previous_extremum_pos!=-1:
                    period_extremum = (j - previous_extremum_pos)
                    mean2_discrepancy_period_extremum[i] += \
                        np.square(
                            period_extremum - mean_period_extremum[i]
                        )
                    nb += 1


                previous_extremum_pos = j

        if nb != 0:
            mean2_discrepancy_period_extremum[i] /= nb

    return mean2_discrepancy_period_extremum


def compute_period_extremum_df(
    dataframe: pd.DataFrame,
    wrt: Union[List[str], str] = None
):
    """ Compute the mean period of an event (by computing
    the difference between each extrema). Then, compute
    the standard deviation of this period.
    In-place: the variable `mean_period_max` and
    `std_period_max` (if `wrt_max`) or
    `mean_period_min` and `std_period_min`
    (otherwise) is directly added to the dataframe.

    Args:
        dataframe: dataframe where to add the new voi
        wrt: list, can contain `"max"` (period computed
            via the max), `"min"` (period computed via
            min) and "mean" (the mean result between "max"
            and `"min"` is evaluated). By default, compute
            all  of those.
    """

    if wrt is None:
        wrt = ["max", "min", "mean"]
    if isinstance(wrt, str):
        wrt = [wrt]

    ## Numpy arrays ====================================
    samples_array = np.array(
        dataframe[defs.list_samples_columns],
        dtype=np.float_
    )


    ## Compute new variables ==========================
    for wrt_column in wrt:
        if wrt_column == "max" or wrt_column == "min":
            wrt_max = (wrt_column == "max")

            mean_period_extremum = calc_mean_period_extremum(
                samples_array=samples_array,
                wrt_max=wrt_max
            )
            dataframe[f'mean_period_{wrt_column}'] = \
                mean_period_extremum

            dataframe[f'std_period_{wrt_column}'] = \
                calc_mean_discrepancy_period_extremum(
                    samples_array=samples_array,
                    mean_period_extremum=mean_period_extremum,
                    wrt_max=wrt_max
            )
        elif wrt_column == "mean":
            dataframe["mean_period"] = \
                (dataframe["mean_period_max"] + \
                    dataframe["mean_period_min"]) / 2

            dataframe["std_period"] = \
                np.sqrt(dataframe["std_period_max"]**2 + \
                    dataframe["std_period_min"]**2)

        else:
            raise ValueError(f"wrt contains {wrt_column}")

##############################################################
########################### PEAKED ###########################
##############################################################

def compute_nb_peaks(
    dataframe: pd.DataFrame,
    limit=0.7
):
    """ Compute the numbers of peaks in the dataframe.
    Note that a peak spanning two time steps is considered
    as 2 peaks.

    Args:
        dataframe: dataframe where to add the new voi
        limit: number between 0 and 1, with respect to
            the min, above which a value is considered
            as a peak within an event.

    Note:
        The main advantage of defining the limit w.r.t.
        min is that the selection does not depend on
        the number of peaks. It could also be defined
        w.r.t. the midth, also it might be a little too
        much dependent on the amplitude of the peak
        (w.r.t. the noise level)
    """
    ## Numpy arrays =================================================
    samples_array = np.array(
        dataframe[defs.list_samples_columns],
        dtype=np.float_
    )

    samples_array -= data.get_1D_to_2D(dataframe["min"])
    samples_array /= data.get_1D_to_2D(dataframe["max"] \
        - dataframe["min"])
    # -> ranges from 0 to 1
    # value range from -1. to 1.
    # samples_array -= 0.5
    # samples_array *= 2

    limit = np.full(shape=len(samples_array), fill_value=limit)

    ## Compute new variables ===========================================
    dataframe['nb_pos_peaks']  = files.calc_nb_peaks_above_limit(
        samples_array=samples_array,
        limit=limit,
        large_peaks=False
    )
    dataframe['nb_neg_peaks']  = files.calc_nb_peaks_above_limit(
        samples_array=1 - samples_array,
        limit=limit,
        large_peaks=False
    )

@jit(nopython=True)
def calc_std_without_peaks(
    samples_array: np.ndarray,
    samples_array_red: np.ndarray,
    limit: np.array,
)->np.array:
    """ For each event, compute the standard deviation,
    without considering any peaks (above the value
    `limit`).

    Args:
        samples_array: two-dimensional array whose
            rows are events, and columns are the signal shape
            in the form of 32 values at different time step.
        samples_array_red: same as `samples_array`, except that
            this array is standardised. This is use for comparison
            to `limit`
        limit: limit value for a value to be considered as
            a peak (and thus be removed from the computation
            of the standard deviation)

    Returns:
        std_without_peaks: standard deviation without the peaks,
            for each event
    """
    std_without_peaks = np.zeros(len(samples_array), dtype=np.float_)

    for i in range(len(samples_array)): # loop over the events
        j = 0
        mean_without_peaks = 0
        mean2_without_peaks = 0
        nb_samples = 0
        while j < defs.n_samples:
            if not(samples_array_red[i][j] >= limit[i]):
                nb_samples += 1
                mean_without_peaks += samples_array[i][j]
                mean2_without_peaks += samples_array[i][j]**2
            j += 1
        if nb_samples != 0:
            mean_without_peaks /= nb_samples
            mean2_without_peaks /= nb_samples
            std_without_peaks[i] = np.sqrt(mean2_without_peaks - mean_without_peaks**2)

        else:
            std_without_peaks[i] = -1
    return std_without_peaks

def compute_std_without_peaks(
    dataframe: pd.DataFrame,
    limit=0.9
):
    """ For each event, compute the standard deviation,
    without considering any peaks (above the value
    `limit`).

    Args:
        dataframe: dataframe where to add the new voi
        limit: number between 0 and 1, with respect to
            the min, above which a value is considered
            as a peak within an event.
    """
    ## Numpy arrays =================================================
    samples_array = np.array(
        dataframe[defs.list_samples_columns],
        dtype=np.float_
    )
    samples_array_red = samples_array - data.get_1D_to_2D(
        dataframe["min"]
    )
    samples_array_red /= data.get_1D_to_2D(
        dataframe["max"] - dataframe["min"]
    )
    # -> value range from 0 to 1.
    # samples_array_red -= 0.5
    # samples_array_red *= 2


    ## Compute new variables =====================================

    dataframe['std_without_pos_peaks']  = calc_std_without_peaks(
        samples_array,
        samples_array_red,
        np.full(len(samples_array_red), limit)
    )

    dataframe['std_without_neg_peaks']  = calc_std_without_peaks(
        samples_array,
        1 - samples_array_red,
        np.full(len(samples_array_red), limit)
    )
