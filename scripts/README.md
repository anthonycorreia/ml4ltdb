# Analysis

In this `README.md`, we detail how the VOIs are named, and how to use the scripts written during the project in order to cluster and analysis ROOT files. The details of scripts are generally not given (for instance, the functions written for plotting are not detailed).

We are also NOT describing BDTs and simulation of signal and background (noise + peak) (because they were finally not used).

Note that due to time pressure, some names of scripts, python variables, or their locations might look inconsistent. However, all the scripts should work properly.

## Variables
In our analysis, we define
- The **Variables of Interest** (VOIs): variables such that `max`, `min`, `std`, `pos_max`, ...
- Other more complicated variables of interest:
    - `best_chi2`: optimised $\chi^2$ w.r.t. an optimised shape
    - `nb_pos_peaks` and `nb_neg_peaks`: number of positive or negative peaks
    - `std_without_pos_peaks` and `std_without_neg_peaks` : standard deviation without positive or negative peaks
    - `mean_period_max` and `mean_period_min`: mean period between local maxima or minima
    - `std_period_max` and `std_period_min`: standard deviation from these periods
    - `mean_period = (mean_period_max + mean_period_mean) / 2`
    - `std_period = sqrt(std_period_max**2 + std_period_min**2)`
    - `mean2_diff_max` (`mean2_diff_min`): mean squared difference between the global maximum (minimum) and the local maxima (minima)
- Variables computed from these variables of interest
    - `_m_` stands for a minus sign (e.g., `max_m_min = max - min`)
    - `_red` stands for dividing by the standard deviation (e.g., `mean_red = mean / std`)
    - `_d_max_m_min` stands for dividing by `max_m_min` (e.g., `mean_d_max_m_min = mean / (max - min)`)
    - `_rounded` means rounded to the nearest integer, especially used for `mean_rounded`
    - `std_period_d_mean_period`: `std_period / mean_period`
    - `relative_mean`: `(mean - min) / (max - min) - 0.5`

More details are given in the jupyter notebook `README_variables.ipynb` (if you are experiencing problem to open it locally, you can still open it in gitlab: https://gitlab.com/anthonycorreia/ml4ltdb/-/blob/main/scripts/README_variables.ipynb).

## Tools

Within our analysis, we have implemented three tools or scripts that are able to:
1. Save a sub-sample of a run in a new ROOT file, requiring some variables to be computed, and some cuts to be applied $\rightarrow$ script `root_files/save_processed_dataframe.py`
2. Apply a clustering algorithm (HDBSCAN, DBSCAN, OPTICS or BIRCH) to a data sample (see the folder `clustering`) $\rightarrow$ script `clustering/brute_force_clustering.py`
3. Analyse a data sample, separating the 4 layers. This is particularly adapted to study peaked and oscillating background. $\rightarrow$ script `background/analysis_bkg.py`

These 3 tools can be configured to suit your need. In other words, it allows to only have a few scripts to analyse many types of background.

In the following three sections, we will explain in detail how these scripts can be configured and used.

### Save a sub-sample of a run in a ROOT file

if you want to run a clustering on a full run given a specific selection (e.g., $\chi^2 < 0.015$ for signal events), it is better to save the result of the selection in a ROOT file, and then run the clustering on it. With this in mind, the python script `./scripts/root_files/save_processed_dataframe.py` together with the configurations specified in `./project/configuration.py` are dedicated to saving ROOT files with specific selections.

#### Configuration

The ROOT file that is saved can be configured in the `configurations` dictionnary of `./project/configuration.py`

For instance, if we want to define a new file called `run1_num1_low_chi2`, that is the first ROOT file (`num = 0644`) of the first run (`run = 00405127`) with the $\chi^2 < 0.015$ requirement, we must first add this configuration to the `configurations` dictionnary:

```python
configurations["run1_num1_low_chi2"]{ # the name of the 
    # Run
    "run": "00405127",
    # Root file numbre
    # set to None if you want all the ROOT files of a given ru
    "num": "0644",
    # cut to apply USING ONLY THE VARIABLES THAT ARE NOT COMPUTED
    # (e.g., `layer`, `detector`)
    "cut": "layer==2", 
    ## VARIABLES:
    # List of VOIs
    "list_voi": ["max", "min", "std"], 
    # Aliases corresponding to function to run, that compute "complicated" VOIs 
    # like the chi2, or the number of peaks ...
    # The VOIs associated with each alias are given in the table of 
    # the juypter notebook `README_variables.ipynb`
    "list_functions_run" : [ 
        "chi2", "peak_voi", "nb_peaks",
        "std_peaks", "period", "diff_extremum"
    ]
    # columns to compute 
    "columns_to_compute": ["max_m_mean", "relative_mean"],
    # Cut to apply after having computed all the VOIs
    "after_cut": ["max - min > 3"],
    # whether the 32 points are dropped from the saved ROOT file
    # set to False if you want to keep the 32 points
    "drop_samples": False, 
    # In which folder to save the ROOT file
    "folder": "signal",
    # Fraction of the dataframe to keep after selection
    # Set to 0.05 if you want to keep only 5% of the dataframe for instance
    "frac": None, # we take everything
    # Seed for the `frac` argument (since a sub-sample is randomly drawn)
    "seed": 15,
    # Maximum number of events read
    "max_nb_events": None
    # If several ROOT files, whether we start processing the last ROOT file instead
    # of the first one
    "backward": None
}
```

Note that other than the 32 points, the original variables (such as `channelId`, `layer`, ...) that are kept in the ROOT files are given by the list `unproc_kept_variables` defined in `project.defs`.

Using this template, the following ROOT files are defined:
- `run1_num1_layer{l}` for $l\in\left\{0, 1, 2, 3\right\}$, the events of run 1 with $\max{e} - \min{e} > 3$, in different layers $l$
- `run{n}_{cat}`, for $n\in\{1, 2, ..., 7\}$, the run (the corresponding "real" numbers are given in the dictionnary `run_number_dict` in `project/configuration.py`). `cat` is the category of events:
  - `signal`: events for $\chi^2 < 0.015$
  - `pos_peak`: positively peaked background
  - `neg_peak`: negatively peaked background
  - `oscillating`: oscillating background
- `run{n}_small`, for $n\in\{1, 2, ..., 7\}$ is a smaller sub-sample of run $n$, containing only 0.1% of the events (randomly drawn with a seed of 15).

The cuts associated with each category are defined in the python file `project/`, in the dictionnary `dict_cuts`. In this same file, the dictionnary `dict_columns` specify the minimal `list_voi`, `columns_to_compute` and `list_functions_run` in order to apply these cuts.

We also defined and `run{n}_{cat}_lim` and `run{n}_small_lim`, defining analogously, except that a maximum of `max_nb_events = 656000000` events is taken. This is particularly used in runs 1 to 6, where there are many more events than run 1 and 7.

We also defined and `run{n}_{cat}_lim_back` and `run{n}_small_lim_back`, except that the ROOT files are read from the last ROOT file to the first (i.e., `backward = True` in the configuration).

#### Create the ROOT file

Once the configuration has been specified in `./project/configuration.py`, the script `./scripts/root_files/save_processed_dataframe.py` must be used.

In order to run it, you might want to go in the folder `./scripts/root_files/` and run
```
python ./save_processed_dataframe.py --config {configuration_name} (--step_size {step_size})
```
where `configuration_name` is the name of the configuration (i.e., a key of the dictionnary `configurations`). This command will open, process and cut the specified ROOT files, by batches of 1,000,000 events (in order to avoid a memory overflow), but you can change this number by passing the optional argument `--step_size {step_size}`.

For instance, in order to save oscillating events belonging to the first run, you can run the following bash command:
```
python ./save_processed_dataframe.py --config run1_oscillating
```

Bash scripts have been written and saved in the folder `./scripts/root_files/`, that aim to create all the signal and background ROOT files of all the runs (1 to 7). 
- `create_all_signal_dataframe.sh` is used to create the ROOT files of events with $\chi^2 < 0.015$ in run 1 to 7 (the `run{run}_signal` files)
- `create_all_background_dataframe.sh` is used to create the ROOT files of positively and negatively peaked background, and oscillating background in run 1 to 7 (the `run{run}_{cat}` files)
- `create_all_small_dataframe.sh` is used to create the ROOT files of ``run{n}_small` files.

Please feel free to use them, and adapt them. In particular, a bash script (e.g., `create_all_signal_dataframe.sh`) can be run in the following way:
```
sh create_all_signal_dataframe.sh >> output.txt
```
This way, the output of the processing will be saved in the `output.txt` file.

Note that additional scripts, called `count_events.py`, can be used to count the number of events in each run and save the result in a JSON file. `count_events_into_tex.py` converts the JSON file into a `.tex` file.

### Cluster a sub-sample

In order to cluster a sub-sample (i.e., a ROOT file with a given configuration saved using the `./scripts/root_files/save_processed_dataframe.py` script), the script `./scripts/clustering/brute_force_clustering.py` is used. The clustering uses the 32 points only. 

In our project, we used this script to test different clustering algorithms, as well find signal clusters.

First, a clustering strategy must be defined in the dictionnary `clustering_strategies`, located in the `./scripts/clustering/clustering_strategy.py` python file. For instance, to define the configuration `signal_layer2`:
```python
clustering_strategies["signal"] = {
    ## STANDARDISATION: it 
    # What the points are substracted by, for standardisation
    "zero": "mean_rounded",
    # What the points are divided by (after being substracted by `zero`), 
    # for standardisation
    "norm": "max_m_mean_rounded",
    # Whether random numbers are added to each point,
    # in order to smear the discretisation
    "rd": False,
    # Whether the positions of the maximum are aligned across the events
    "apm": True,
    # Clustering algorithm
    # Default (i.e., if not given) is HDBSCAN
    "algo": "HDBSCAN",
    # Hyperparameters of the clustering (HDBSCAN)
    "hyperparams": {
        "min_cluster_size": 10,
        "min_samples": 1,
        # no cluster_selection_epsilon
    },
    # VOI that are loaded
    "voi": ["pos_max", "std", "pedestal", "max", "mean"],
    # Other more complicated VOI (e.g., `best_chi2`, `nb_pos_peaks`, ...)
    "other_columns": ["best_chi2"]
    # Columns that are computed, equivalent to `columns_to_compute`
    "columns": ["max_m_mean", "mean_rounded", "max_m_mean_rounded"],
    # Other cuts to apply to the sample
    "cuts": ["best_chi2 < 0.015", "layer==2"]
    # Maximal number of events clustered in the clustering.
    # The other events are just left out
    "max_nb_events": 75000,
    # seed for `max_nb_events`, as a sub-sample is randomly drawn
    "seed": 30
}
```

Note that if (and only if) a VOI or a column is not in the ROOT file, it is computed.

Following this approach, the following strategies are defined:
- `all_gen_optics`: general clustering, using OPTICS
- `all_gen_dbscan`: general clustering, using DBSCAN
- `all_gen_birch`: general clustering, using BIRCH
- `signal_layer{layer}` for `layer` from 0 to 3, clustering to a sample
    with `best_chi2 < 0.015`, `max - min > 3` and `layer == {layer}`

In order to lauch the clustering, please go to the folder `./scripts/clustering/` and run
```
python ./brute_force_clustering.py --data {config} -cs [--clustering_strategy] {clustering_strategy} (-d [--detector] {detector}) (-p [--preliminary_plots]) (-sr [--save_root])
```
where
- `config` is the name of the configuration of the ROOT file that is clustered (defined in `./project/configuration.py`)
- `clustering_strategy` is the name of the clustering strategy (a key of the dictionnary `clustering_strategies` defined in `./scripts/clustering/clustering_stategy.py`)
- The optional argument `-d {detector}` (or `--detector {detector}`) can be used to only perform clustering on a detector
- `-pp` is a flag. If set, it plots `eta`-`phi` 2D histograms before clustering.
- `-sr` is a flag. If set, the resulting ROOT file is saved.

The outputs of this script are a ton of plots showing the resulting of the clustering, saved in `.pdf` and `.png` in your image output path. The locations where each image was saved is indicated when the script is run.


Clusterings on the signal samples of run from 1 to 7 in each layer (0 to 3), first without distinguising the detectors, and then by distinguishing the detectors (using the `--detector {detector}` argument of `brute_force_clustering.py`) can be run using the bash script `./scripts/clustering/run_clustering.sh`.


### Analyse a data-sample

The last script, located in `./scripts/background/analysis_bkg.py`, was used to analyse peaked and oscillating background without clustering. By analysing, we mainly mean plotting, separating layers 0, 1, 2 and 3. Similarly to the other scripts, a strategy must be defined in the `strategies` dictionary located in the `./scripts/background/strategy.py` file.

For instance, to analyse positively peaked background:
```python
strategies["pos_peak"] = {
    # List of VOI to load from the dataframe
    "list_voi": ["max", "min", "mean", "std", "pedestal"],
    # Other more complicated VOI (such as `best_chi2`, ...)
    "other_columns": [
        "best_chi2",
        "nb_pos_peaks",
        "std_without_pos_peaks"
    ],
    # List of columns to compute (or load) (equivalent to `columns_to_compute`)
    "columns" : [
        'max_m_mean',
        'max_m_min',
        "relative_mean",
        "std_without_pos_peaks_d_max_m_min"
    ],
    # Cuts to isolate positively peaked background
    "cuts": project.cuts.dict_cuts["pos_peak"],

    # 1D and 2D distributions to plot (for each layer separately)
    "what_to_plot": [
        ["nb_pos_peaks"],
        ["nb_pos_peaks", "max_m_min"],
        ["eta", "phi"],
        # This is how you can require a cut just for a lot
        # Here, the 2D histograms of (eta, phi) is saved 
        # after requiring `nb_pos_peaks > 2`
        [["eta", "phi"], "nb_pos_peaks > 2"],
    ],
    
    # Plot histograms for each value of some column 
    # (for each layer separately)
    # For instance, here, we plot 2D histograms in (eta, phi)
    # for events with 1, 2, ... positive peaks
    "plot_clusters" : [ # /!\ NB: only 2D histograms!
        {   
            "columns": ["eta", "phi"],
            "cluster": "nb_pos_peaks",
            "label": "" # Label of the cluster column
            # It might appear in the title of some plots
        }
    ],
    # Plot 2 events for each value of a given column
    # (for each layer separately)
    # Here for instance, we plots 2 events (i.e., the 32 points)
    # For each value of `nb_pos_peaks`, i.e., for 1 peak, 2 peaks, etc.
    "plot_samples" :  "nb_pos_peaks",
    # To plot some distributions for the most predominant channels
    # i.e., the channels with the most events after all the cuts
    "plot_channel_id" : [ 
        ["nb_pos_peaks", "max_m_min"],
    ]
}
```

For instance, we defined the following strategies
- `regular_oscillating` for very regular oscillations ($\sigma_{T_e} = 0$)
- `fast_oscillating` for oscillations of period $\sim 2$ (with $\sigma_{T_e} \neq 0$)
- `medium_oscillating` for oscillations of period $\sim 4$
- `fast_oscillating` for oscillations of period $> 5$
- `pos_peak` for positively peaked background
- `neg_peak` for negatively peaked background
- `pos_peak_low` and `neg_peak_low`, for layer 2, with $\max{e} - \min{e} < 100$
- `pos_peak_high` and `neg_peak_high`, for layer 2, with $\max{e} - \min{e} > 100$

In order to run an analysis, please move to the folder `./scripts/background` and run
```
python analysis_bkg.py --data {config} --strategy {strategy} (--ref {ref}) (-psc [--plot_samples_channels])
```
where
- `config` is the name of the configuration of the ROOT file that is analysed (defined in `./project/configuration.py`)
- `strategy` is the name of the analysis strategy (a key of the dictionnary `strategies` defined in `./scripts/background/strategy.py`)
- `ref` is the name of the configuration of a ROOT file (i.e;, defined in `./project/configuration.py` as always), that is used to compare `IEvent` between the dataframe analysed (given by `config`) and a reference dataframe (given by `ref`). For instance, we often use `--ref run{run}_small`. This way, the regularity of the background that is being analysed can be shown.
- `-psg` (or `--plot_samples_channels`) is a flag. If set, 2 examples of events ($\equiv$ the 32 points) are shown for each of the most predominant channels (the channels with many events).

A bash script to analyse all the background (all the oscillating background categories, and all the peaked background categories) of all the runs (1 to 7) has been written in `./script/background/analyse_all_background.sh`. Please feel free to use it.
