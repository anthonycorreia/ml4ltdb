## Background analysis

Measured data during the 900 GeV test runs contain background events, whose shape is not random (i.e., which does not seem to com from a Gaussian random process). Two kinds of background have been observed:
- The oscillating background
- The peaked background
- The background "between 2 walls"

Stategies to select these background have been developped for tests in many runs in a systematic way. In this folder, the background events are analysed. The analysed dataframe are such that the required cuts to select a given category of background have already been performed.

