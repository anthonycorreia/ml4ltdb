for run in 1 2 3 4 5 6 7
do
	
    python ./analyse_bkg.py --data run${run}_oscillating -cs regular_oscillating --ref run${run}_small
    python ./analyse_bkg.py --data run${run}_oscillating -cs fast_oscillating --ref run${run}_small
    python ./analyse_bkg.py --data run${run}_oscillating -cs medium_oscillating --ref run${run}_small
    python ./analyse_bkg.py --data run${run}_oscillating -cs slow_oscillating --ref run${run}_small
    python ./analyse_bkg.py --data run${run}_pos_peak -cs pos_peak --ref run${run}_small
    python ./analyse_bkg.py --data run${run}_neg_peak -cs neg_peak --ref run${run}_small
done