""" Analyse the most regular oscillating events.
We require the events to be perfectly periodic,
without any deviation from that period.

Another script is devolved in finding other
oscillating events.

1. (Re-)perform the cuts to isolate the oscillating
    events
2. Require no deviation from the mean period

"""

############################################################
######################### LIBRARIES ########################
############################################################

import gc
import os.path as op
import argparse

import numpy as np
import matplotlib.pyplot as plt

from lib import plot as lp, data, clustering, utils

from project import defs, files
from project.defs import dict_color_clu_labels
from project.defs_range import column_ranges, column_nbins

from project.plot import plot_hist_each_layer
from project import channel_id

import var
from strategy import strategies


def plot_list_columns(dataframes, what_to_plot):
    for columns in what_to_plot:
        if isinstance(columns[0], list) or isinstance(columns[0], tuple):
            cut = columns[1]
            columns = columns[0]
        else:
            cut = None
        
        var1 = columns[0]
        if len(columns)==2:
            var2 = columns[1]
            name = f"{var1}_{var2}"
        else:
            var2 = None
            name = f"{var1}"

        plot_hist_each_layer(
            dataframes,
            var1=var1,
            var2=var2,
            column_labels=column_labels,
            same_fig=False,
            path=image_path,
            name=name,
            column_nbins=column_nbins,
            column_ranges=column_ranges,
            logscale=True,
            cut=cut
        )
    


if __name__=="__main__":
    ########################################################
    ########################  PARSER #######################
    ########################################################
    parser = argparse.ArgumentParser(
        "Analyse regular periodic oscillating events"
    )

    parser.add_argument(
        '--data', required=True,
        type=str,
        help="ROOT file configuration as defined in"
        "project/configuration.py."
    )

    parser.add_argument(
        '--strategy', required=True,
        type=str,
        help="Strategy used for the analysis, as defined in"
            "`strategy.py`"
    )
    parser.add_argument(
        '--ref', required=False,
        type=str,
        help="Dataframe of reference of the analysed run"
    )

    parser.add_argument(
        "-psc", "--plot_samples_channels", 
        action="store_true",
        default=False, required=False,
        help="Plot examples of samples in the predominant channels"
    )

    args = parser.parse_args()
    # eps = args.epsilon
    config_name = args.data
    strat = args.strategy
    config_name_ref = args.ref
    plot_samples_channel_id = args.plot_samples_channels
    strategy = strategies[strat]
    plot_samples = strategy.get("plot_samples")

    ## PATHS ===============================================
    full_config = f"{config_name}_{strat}"
    image_path = op.join(
        var.image_path,
        full_config,
    )

    json_path = op.join(
        var.json_path,
        full_config
    )

    table_path = op.join(
        var.table_path,
        full_config
    )

    ########################################################
    ####################  LOAD ROOT FILE ###################
    ########################################################
    ## COLUMNS =============================================
    # NB: only the variables actually used in the script
    # are loaded
    list_voi = strategy.get("list_voi")
    columns = strategy.get("columns")
    other_columns = strategy.get("other_columns")

    ## LOAD THE ROOT FILE ==================================
    print(f"LOAD ROOT FILE {config_name}")

    file_path = files.get_path_processed_root(config_name)

    other_branches = ["eta", "phi", "layer", "channelId", "IEvent", "detector"]

    dataframe, column_labels = files.load_and_process_dataframe(
        file_path, list_voi, columns, other_columns,
        branches=defs.list_samples_columns + other_branches,
        verbose=True
    )
    column_labels["detector"] = "Detector"

    if len(dataframe)==0:
        print("No events in the dataframe")
        quit()

    ## RE-PERFORM THE CUTS ==================================
    # The cuts are (re-)applied to the dataframe
    print("APPLY CUTS")
    for cut in strategy.get("cuts", []):
        data.cut_dataframe(dataframe, cut)

    ## SEPARATE INTO LAYERS ==================================
    dataframes = {}
    print("NUMBER OF EVENTS")
    for layer in defs.layers:
        dataframes[layer] = dataframe.query(
            f"layer == {layer}"
        )
        print(
            "Layer", layer, ":",
            len(dataframes[layer]),
            "events"
        )

    ########################################################
    #######################  PLOTTING ######################
    ########################################################
    print("PLOTS")

    ## HISTOGRAMS ==========================================
    what_to_plot = strategy.get("what_to_plot", [])
    plot_list_columns(dataframes, what_to_plot)
    

    ## CLUSTERS ============================================
    clu_plots = strategy.get("plot_clusters", []) 

    for clu_plot in clu_plots:
        columns = clu_plot["columns"]
        clu_column = clu_plot["cluster"]
        for layer, df in dataframes.items():      
            if len(df) > 0:
                unique_clu_values = np.sort(np.unique(df[clu_column]))
                print(
                    "Layer", layer, ":",
                    clu_column, unique_clu_values)
                dict_color_clu_cats = {
                    channel_id: dict_color_clu_labels[i]
                    for i, channel_id in enumerate(
                        unique_clu_values
                    )
                }

                clustering.plot_clusters_hist2d(
                    df,
                    columns,
                    column_labels,
                    clu_column,
                    logscale=True,
                    dict_color_clu_cats=dict_color_clu_cats,
                    column_ranges=column_ranges,
                    column_nbins=column_nbins,
                    category_name=clu_plot["label"],
                    path=image_path,
                    name=f"{clu_column}_l{layer}",
                    to_plot=("one_ax", "multi_ax"),
                    return_figs=False,
                )
    ########################################################
    ####################  PLOT SAMPLES #####################
    ########################################################
    if plot_samples is not None:
        clustering.plot_samples_clusters(
            dataframe,
            plot_samples,
            dict_color_clu_labels=defs.dict_color_clu_labels,
            title=None,
            name=plot_samples,
            path=op.join(image_path, f"samples"),
            one_fig=False
        )

    ########################################################
    #####################  CHANNEL IDs #####################
    ########################################################
    print("FIND CHANNEL IDS")

    ## Get the channel IDs =================================
    channel_id_counts_layers = {}
    nb_events_layers = {}
    for layer, dataframe in dataframes.items():
        channel_id_counts_layers[layer] = \
            channel_id.get_channel_id_counts(dataframe)
        nb_events_layers[layer] = len(dataframe)

    predominant_channel_id_counts_layers = {}
    utils.try_makedirs(json_path)
    for layer in defs.layers:
        count = min(
            0.1 * nb_events_layers[layer], 30
        )
        first = True
        while first or len(predominant_channel_id_counts_layers[layer]) > 15:
            predominant_channel_id_counts_layers[layer] = \
                channel_id.select_new_predominant_channel_id(
                    channel_id_counts_layers[layer],
                    min_count=count
                )
            count += 10
            first = False
            
        print("Layer", layer, '-'*5)
        print(predominant_channel_id_counts_layers[layer])

        ## Save them in JSON files =========================
    utils.dump_json(
        channel_id_counts_layers,
        path=op.join(
            json_path, "channel_id_counts_layers"
        )
    )
        
    utils.dump_json(
        predominant_channel_id_counts_layers,
        path=op.join(
            json_path, "predominant_channel_id_counts_layers"
        )
    )
    utils.dump_json(
        nb_events_layers,
        path=op.join(
            json_path, "nb_events"
        )
    )
    for layer, channel_id_counts in predominant_channel_id_counts_layers.items():
        table = [["Channel", "Number of events"]]
        for channel_id, counts in channel_id_counts.items():
            table.append([str(channel_id), counts])

        utils.write_table(
            table,
            name=f"predominant_channel_id_counts_layer{layer}",
            path=table_path
        )

    print(predominant_channel_id_counts_layers)

    ## Show where they are in (eta, phi) ===================
    print("WHERE THE CHANNEL IDS ARE")
    
    plot_channel_id = strategy.get("plot_channel_id")

    if len(what_to_plot) >= 1 or plot_channel_id is not None:
        dataframes_predominant = {
            layer: df[df["channelId"].isin(predominant_channel_id_counts_layers[layer].keys())]
            for layer, df in dataframes.items()
        }


    if plot_channel_id is not None:
        for layer, channel_id_counts \
            in predominant_channel_id_counts_layers.items():

            if len(channel_id_counts) > 0:
                dict_color_channel_id = {
                    channel_id: dict_color_clu_labels[i]
                    for i, channel_id in enumerate(channel_id_counts)
                }

                for columns in plot_channel_id:
                    clustering.plot_clusters_hist2d(
                        dataframes[layer][
                            dataframes[layer]["channelId"].isin(
                                channel_id_counts.keys()
                            )
                        ],
                        columns,
                        column_labels,
                        "channelId",
                        column_nbins=column_nbins,
                        logscale=True,
                        dict_color_clu_cats=dict_color_channel_id,
                        column_ranges=column_ranges,
                        category_name="Channel",
                        return_figs=False,
                        name = f"channelid_l{layer}",
                        path=image_path,
                        to_plot=("one_ax","multi_ax"),
                    )

            # for fig_name, figs in dict_figs.items():
            #     fig1, fig2 = figs
            #     title = f"channelid_l{layer}_{fig_name}"
            #     lp.save_fig(fig1, image_path,
            #         f"{title}_one_ax")
            #     plt.close(fig1)
            #     plt.close(fig2)

    ## What to plot =======================================
    what_to_plot = strategy.get("what_to_plot_predominant", [])
    plot_list_columns(dataframes_predominant, what_to_plot)
    
    
    for columns in what_to_plot:
        if isinstance(columns[0], list) or isinstance(columns[0], tuple):
            cut = columns[1]
            columns = columns[0]
        else:
            cut = None
        
        var1 = columns[0]
        if len(columns)==2:
            var2 = columns[1]
            name = f"{var1}_{var2}"
        else:
            var2 = None
            name = f"{var1}"

        plot_hist_each_layer(
            dataframes_predominant,
            var1=var1,
            var2=var2,
            column_labels=column_labels,
            same_fig=False,
            path=image_path,
            name=name,
            column_nbins=column_nbins,
            column_ranges=column_ranges,
            logscale=True,
            cut=cut
        )

    ## Plot 2 of them for each layer ======================
    if plot_samples_channel_id:
        for layer, channel_id_counts \
            in predominant_channel_id_counts_layers.items():

            for channel_id in channel_id_counts.keys():
                fig, ax = plt.subplots(figsize=(8, 6))
                df = dataframes[layer].query(
                    f"channelId=={channel_id}")
                for i in range(min(2,len(df))):
                    lp.plot_event(
                        df.iloc[i],
                        minus="pedestal",
                        ax=ax
                    )

                
                lp.save_fig(
                    fig,
                    op.join(image_path, "samples"),
                    f"l{layer}_c{channel_id}"
                )
                plt.close(fig)

    # fig, ax = plt.subplots(figsize=(8, 6))
    # for i in range(2):
    #     lp.plot_event(
    #         dataframes[0].query("eta>0 and phi > 0").iloc[i],
    #         minus="pedestal",
    #         ax=ax
    #     )  
    # lp.save_fig(
    #     fig,
    #     op.join(image_path, "samples"),
    #     f"random_layer0"
    # )
    # plt.close(fig)

    ########################################################
    ##################  TIME CORRELATION ###################
    ########################################################
    print("TIME CORRELATION")
    ## LOAD REFERENCE SAMPLE ===============================
    if config_name_ref is not None:
        ref_file_path = files.get_path_processed_root(
            config_name_ref
        )
        ref_dataframe, _ = files.load_and_process_dataframe(
            ref_file_path,
            branches=defs.list_samples_columns + ["layer", "IEvent"],
            list_voi=["max", "min"],
            verbose=True
        )
        # Just make sure this cut is applied
        data.cut_dataframe(ref_dataframe, "max - min > 3")

        for layer, channel_id_counts \
            in predominant_channel_id_counts_layers.items():

            if len(channel_id_counts) > 1:
                ## Plot for all the channels ======================
                df = dataframes[layer][
                    dataframes[layer]["channelId"].isin(
                        channel_id_counts.keys()
                    )
                ]
                
                ref_df = ref_dataframe.query(f"layer=={layer}")

                fig, ax = lp.multi_plot_hist_same_col(
                    [df, ref_df],
                    "IEvent",
                    column_label="Event index",
                    list_labels=["Background", "Reference"],
                    alpha=0.5,
                    normalise=True,
                    method="one_ax",
                    list_colors=["b", "g"],
                    with_err=[True, False]
                )

                lp.save_fig(
                    fig,
                    image_path,
                    f"IEvent_check_l{layer}"
                )
                plt.close(fig)

                ## Plot for each channel =========================

                fig, ax = plt.subplots(figsize=(12, 6))
                _, bins, _ = ax.hist(
                    ref_df["IEvent"],
                    label="Reference",
                    facecolor="None",
                    ls='-', lw=3,
                    edgecolor="k",
                    weights=np.ones_like(ref_df["IEvent"]) / len(ref_df),
                    bins=50
                )

                lp.multi_plot_hist_same_col(
                    [
                        df.query(f"channelId=={channel_id}")
                        for channel_id in channel_id_counts.keys()
                    ],
                    "IEvent",
                    column_label="Event index",
                    list_labels=[
                        f"Channel {channel_id}"
                        for channel_id in channel_id_counts.keys()
                    ],
                    alpha=0.5,
                    normalise=True,
                    method="one_ax",
                    list_colors=list(dict_color_clu_labels.values())[1:],
                    return_bins=True,
                    axes=ax,
                    bins=bins,
                    with_err=True
                )
                ax.legend(bbox_to_anchor=(1., 1.))
                lp.save_fig(
                    fig,
                    image_path,
                    f"IEvent_check_channel_id_l{layer}"
                )
                plt.close(fig)

