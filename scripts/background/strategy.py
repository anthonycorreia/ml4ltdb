""" Strategy of analysis
"""

from copy import deepcopy

from project import cuts
from project.defs_label import column_labels
strategies = {}


## OSCILLATING ================================
strategies["regular_oscillating"] = {
    "list_voi": ["max", "min", "mean", "pedestal"],
    "columns" : [
        'max_m_mean',
        'max_m_min',
        "std_period_d_mean_period"
    ],
    "other_columns": [
        "mean_period_max",
        "mean_period_min",
        "mean_period",
        "std_period_max",
        "std_period_min",
        "std_period"
    ],
    "cuts": cuts.dict_cuts["oscillating"] + [
        "std_period_d_mean_period == 0"
    ],
    "plot_channel_id" : [ # to plot for each predominant channel id
        ["max_m_min"],
    ]
}

strategies["medium_oscillating"] = {
    "list_voi": ["max", "min", "mean", "pedestal"],
    "columns" : [
        'max_m_mean',
        'max_m_min',
        "std_period_d_mean_period",
        "relative_mean"
    ],
    "other_columns": [
        "mean_period_max",
        "mean_period_min",
        "mean_period",
        "std_period_max",
        "std_period_min",
        "std_period"
    ],
    "cuts": cuts.dict_cuts["oscillating"] + [
        "std_period_d_mean_period != 0",
        "mean_period > 3 and mean_period < 5",
        "abs(relative_mean) < 0.2",
        "max - min > 4"
    ],
    "what_to_plot": [
        ["mean_period"],
        ["max_m_min"],
        ["eta", "phi"],
        ["detector"]
    ],
    "plot_channel_id" : [ # to plot for each predominant channel id
        ["max_m_min"],
    ]
}

strategies["fast_oscillating"] = {
    "list_voi": ["max", "min", "mean", "pedestal"],
    "columns" : [
        'max_m_mean',
        'max_m_min',
        "std_period_d_mean_period",
        "relative_mean"
    ],
    "other_columns": [
        "mean_period_max",
        "mean_period_min",
        "mean_period",
        "std_period_max",
        "std_period_min",
        "std_period"
    ],
    "cuts": cuts.dict_cuts["oscillating"] + [
        "std_period_d_mean_period != 0",
        "mean_period < 3",
        "abs(relative_mean) < 0.2",
        "max - min > 5"
    ],
    "what_to_plot": [
        ["mean_period"],
        ["max_m_min"],
        ["eta", "phi"],
        ["detector"]
    ],
    "plot_channel_id" : [ # to plot for each predominant channel id
        ["max_m_min"],
    ]
}

strategies["slow_oscillating"] = {
    "list_voi": ["max", "min", "mean", "pedestal"],
    "columns" : [
        'max_m_mean',
        'max_m_min',
        "std_period_d_mean_period",
        "relative_mean"
    ],
    "other_columns": [
        "mean_period_max",
        "mean_period_min",
        "mean_period",
        "std_period_max",
        "std_period_min",
        "std_period"
    ],
    "cuts": cuts.dict_cuts["oscillating"] + [
        "std_period_d_mean_period != 0",
        "mean_period > 5 and mean_period < 8",
        "abs(relative_mean) < 0.2",
        "max - min > 4"
    ],
    "what_to_plot": [
        ["mean_period"],
        ["max_m_min"],
        ["eta", "phi"],
        ["detector"]
    ],
    "plot_channel_id" : [ # to plot for each predominant channel id
        ["max_m_min"],
    ]
}

for sign in ["pos", "neg"]:
    strategies[f"{sign}_peak"] = {
        "list_voi": ["max", "min", "mean", "std", "pedestal"],
        "columns" : [
            'max_m_mean',
            'max_m_min',
            "relative_mean",
            f"std_without_{sign}_peaks_d_max_m_min"
        ],
        "other_columns": [
            "best_chi2",
            f"nb_{sign}_peaks",
            f"std_without_{sign}_peaks"
        ],
        "cuts": cuts.dict_cuts[f"{sign}_peak"],
        "what_to_plot": [
            [f"nb_{sign}_peaks"],
            [f"nb_{sign}_peaks", "max_m_min"],
            ["eta", "phi"],
            [["eta", "phi"], f"nb_{sign}_peaks > 1"],
            [["eta", "phi"], f"nb_{sign}_peaks > 2"],
            [["eta", "phi"], f"nb_{sign}_peaks <= 1"],
            ["detector"]
        ],
        "plot_clusters" : [ # NB: only 2D plots!
            { 
                "columns": ["eta", "phi"],
                "cluster": f"nb_{sign}_peaks",
                "label": ""
            }
        ],
        # "plot_samples" :  f"nb_{sign}_peaks",
        "plot_channel_id" : [ # to plot for each predominant channel id
            [f"nb_{sign}_peaks", "max_m_min"],
        ]
    }

    strategies[f"{sign}_peak_low"] = deepcopy(strategies[f"{sign}_peak"])
    strategies[f"{sign}_peak_low"]["cuts"] += ["layer==2", "max - min < 100"]
    strategies[f"{sign}_peak_high"] = deepcopy(strategies[f"{sign}_peak"])
    strategies[f"{sign}_peak_high"]["cuts"] += ["layer==2", "max - min > 100"]