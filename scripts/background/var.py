""" Global variables used to analyse
background. 
"""
from project import loc, defs
import os.path as op

image_path = op.join(
    loc.images_folder, "bkg_analysis"
)

json_path = op.join(
    loc.json_folder, 
    "bkg_analysis"
)

table_path = op.join(
    loc.table_folder, 
    "bkg_analysis"
)