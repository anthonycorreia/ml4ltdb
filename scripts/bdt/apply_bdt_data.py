""" Apply a BDT (save in a .pickle file beforehand) to data.
Analyse the result by outputing some plots.

The ROOT data must be generated beforehand, using the script
`scripts/root_files/save_processed_dataframe.py`. Their
configuration (run, num, cuts, etc.) are specified in
`project/configuration.py`, under a `root_name` defined by the
dictionnary `configurations`.

To launch this script, please run

```
python apply_bdt_data.py --data {root_name} --bdtN {bdtN_name} (--bdtP {bdtP_name}) (--cutP)
```
where the bdt name is defined in `scripts/simulation/params_bdt.py`.

If `--cutP` is set, a mere cut 
`min_std_pos_neg_red > 0.02` is performed in order to remove
peaked background.

Note that events with standard deviations = 0 are removed.
"""

###############################################################
#                          LIBRARIES                          #
###############################################################

import os.path as op
import argparse
import matplotlib.pyplot as plt
import numpy as np

from scripts.simulation.var import pickle_path as pickle_path_bdt
from scripts.simulation.params_bdt import bdt_configurations

from lib import plot as lp, rootdata as lrd, data
from lib.bdt import add_bdt_dataframe
from lib.utils import load_pickle
from project import defs, files, loc, col
import var

if __name__ == "__main__":
    ###########################################################
    #                          PARSER                         #
    ###########################################################
    parser = argparse.ArgumentParser(
        "Process and save ROOT files"
    )

    parser.add_argument(
        '--data', required=True,
        type=str,
        help="ROOT file configuration as defined in"
        "project/configuration.py"
    )

    parser.add_argument(
        '--bdtN', required=False,
        type=str, default=None,
        help="Name of the BDT that tells apart"
        "the signal vs noise"
    )

    parser.add_argument(
        '--bdtP', required=False,
        type=str, default=None,
        help="Name of the BDT that tells apart"
        "the signal vs peak background"
    )

    parser.add_argument(
        '--cutN', default=False,
        action="store_true",
        help="Perform a cut to remove noise" 
        "background"
    )


    parser.add_argument(
        '--cutP', default=False,
        action="store_true",
        help="Perform a cut to remove peaked" 
        "background"
    )

    parser.add_argument(
        '--plot_samples', default=False,
        action="store_true",
        help="Plot the signal samples" 
        "(according to the BDT)"
    )

    args = parser.parse_args()
    bdtN_name = args.bdtN
    bdtP_name = args.bdtP
    data_name = args.data
    cutN = args.cutN
    cutP = args.cutP
    plot_samples_flag = args.plot_samples

    image_path = op.join(
        var.image_path,
        f"{data_name}_bdtN{bdtN_name}_bdtP{bdtP_name}_cutN_{cutN}_cutP{cutP}"
    )

    ###########################################################
    #                         LOAD DATA                       #
    ###########################################################

    ## LOAD BDT ================================================
    print("LOAD BDTs")
    dict_bdts = {}
    dict_train_columns = {}
    dict_bdt_labels = {}
    
    list_bdt_names = []
    if bdtP_name:
        list_bdt_names.append(bdtP_name)
    if bdtN_name:
        list_bdt_names.append(bdtN_name)

    columns_to_compute = []
    voi = ["pedestal", "max"]

    for bdt_name in list_bdt_names:
        # Path
        columns_to_compute += bdt_configurations[bdt_name]["columns_to_compute"]
        voi += bdt_configurations[bdt_name]["voi"]
        pickle_path = op.join(
            pickle_path_bdt, 
            bdt_name + ".pickle"
        )

        # Load
        dic_pickle = load_pickle(pickle_path)
        dict_train_columns[bdt_name] = dic_pickle['columns']
        dict_bdts[bdt_name] = dic_pickle['bdt']
        
        dict_bdt_labels[bdt_name] = bdt_configurations[bdt_name]["label"]

    if cutP:
        voi += ["std_pos", "std_neg", "std"]
        columns_to_compute += ["min_std_pos_neg", "min_std_pos_neg_red"]
    if cutN:
        voi += ["std", "mean"]
        columns_to_compute.append("max_m_mean")

    voi = list(dict.fromkeys(voi))
    columns_to_compute = list(dict.fromkeys(columns_to_compute))


    print("LOAD DATA")
    file_path = files.get_path_processed_root(data_name)
    dataframe = lrd.load_ROOT_into_dataframe(
        file_path,
        tree_name=defs.proc_tree_name,
        variables=voi + defs.list_samples_columns
    )

    ###########################################################
    #                         APPLY BDT                       #
    ###########################################################
    formula_columns = col.get_formula_columns(columns_to_compute)
    dataframe = data.compute_vals(dataframe, formula_columns)

    dataframe = dataframe.query("std!=0")

    ## APPLY ==================================================
    print("APPLY BDTs")
    for bdt_name, bdt in dict_bdts.items():
        print(f"Features of {bdt_name}", dict_train_columns[bdt_name])
        add_bdt_dataframe(
            dataframe, bdt, dict_train_columns[bdt_name],
            bdt_name=bdt_name
        )
    
    ###########################################################
    #                         PLOTS BDT                       #
    ###########################################################
    print("PLOT BDTs")
    ## 1D BDT distributions ===================================
    nb_bdts = len(list_bdt_names)

    if nb_bdts>=1 :
        fig, axes = plt.subplots(
            1, nb_bdts, 
            figsize=(8 * nb_bdts, 6)
        )
        if nb_bdts==1:
            axes = [axes]

        for i, bdt_name in enumerate(list_bdt_names):
            lp.plot_hist(
                dataframe[bdt_name], 
                label_var=dict_bdt_labels[bdt_name], 
                logscale=True,
                ax=axes[i]
            )
        
        lp.save_fig(fig, image_path, "BDTs_1D")
        plt.close(fig)
    
    ## 2D BDT distribution ====================================
    if nb_bdts >= 2:
        lp.plot_hist2d(
            var1=dataframe[bdtN_name], var2=dataframe[bdtP_name],
            label_var1=dict_bdt_labels[bdtN_name], 
            label_var2=dict_bdt_labels[bdtP_name],
            logscale=True
        )
        lp.save_fig(fig, image_path, "BDTs_2D")
        plt.close(fig)

    ## 1D BDT noise ===========================================
    list_cuts = []
    if bdtP_name:
        no_peak_cut = f"{bdtP_name} > 0.2"
        dataframe_no_peak = dataframe.query(no_peak_cut)
        list_cuts.append(no_peak_cut)
    
    if cutP:
        cut_remove_peaked = "min_std_pos_neg_red > 0.02"
        dataframe_no_peak = dataframe.query(cut_remove_peaked)
        list_cuts.append(cut_remove_peaked)
    
    total_cut = " and ".join(list_cuts)
    
    if bdtN_name is not None:
        no_noise_cut_value = 0.82

        fig, ax = lp.plot_hist(
            dataframe_no_peak[bdtN_name], 
            label_var=dict_bdt_labels[bdtN_name],
            logscale=True,
        )
        ax.axvline(x=0.82, linestyle='--')
        lp.save_fig(
            fig, image_path, 
            f"BDTN_1D_with_{total_cut.replace(' > ', '_sup_')}"
        )
        plt.close(fig)


    ## Amp_S vs BDT ===========================================
    list_no_noise_cuts = []
    if bdtN_name:
        list_no_noise_cuts.append(f'{bdtN_name} > {no_noise_cut_value}')
    if cutN:
        list_no_noise_cuts.append(f'max_m_mean > 5')

    no_noise_cut = " and ".join(list_no_noise_cuts)
    
    if len(list_no_noise_cuts)>=1:
        df_high_BDT = dataframe_no_peak.query(
            no_noise_cut
        )

    fig, ax = lp.plot_hist(
        df_high_BDT["max"] - df_high_BDT["pedestal"],
        r"$\max(e) - e_0$", 
        logscale=True
    )
    lp.save_fig(fig, image_path, "amp_S")
    plt.close(fig)

    ###########################################################
    #                       PLOTS SAMPLES                     #
    ###########################################################
    if plot_samples_flag:
        print("PLOT SAMPLES")
        from matplotlib.backends.backend_pdf import PdfPages
        full_path = op.join(
            image_path, "signal_samples.pdf"
        )

        step = 2
        minus_mean = True

        with PdfPages(full_path) as pdf:
            print("path:", full_path)
            for i in range(len(df_high_BDT)):
                print(f"step {i} / {len(df_high_BDT)}")
                if (i % step) == 0:
                    fig, ax = plt.subplots(figsize=(8, 6))
                    ax.set_rasterized(True)

                samples = df_high_BDT.iloc[i]

                # samples -> samples - pedestal
                
                samples_plot = \
                    samples[defs.list_samples_columns] \
                        - samples['pedestal']

                lp.plot_event(
                    samples_plot,
                    ax=ax,
                    label=str(i)
                )
                
                if (i % step) != 0:
                    ax.legend()
                    fig.tight_layout()
                    pdf.savefig(fig)
                    plt.close(fig)
            
        
        






    