""" Global variables used for applying BDTs.
"""

import os.path as op
from project import loc

image_path = op.join(
    loc.images_folder, "apply_bdt_data"
)