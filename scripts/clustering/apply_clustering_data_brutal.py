""" (deprecated)
 Apply a brutal clustering in data
"""

############################################################
########################  LIBRARIES ########################
############################################################

import argparse
import os.path as op
import time

import numpy as np
import hdbscan
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

import data_cuts
import var

from lib import plot as lp, clustering
from lib.utils import try_makedirs, dump_pickle

from project import defs, files, normalize
from project.defs import dict_color_clu_labels


if __name__=="__main__":
    ########################################################
    ########################  PARSER #######################
    ########################################################

    parser = argparse.ArgumentParser(
        "Apply a brutal clustering algorithm"
    )

    parser.add_argument(
        '--data', required=True,
        type=str,
        help="ROOT file configuration as defined in"
        "project/configuration.py"
    )

    parser.add_argument(
        "-rd", '--randomise_discretisation',
        default=False, action="store_true",
        help="Randomise the samples to smear discretisation"
    )

    parser.add_argument(
        "-eps", '--epsilon', type=float,
        default=None,
        help="Epsilon parameter of HDBSCAN"
    )

    parser.add_argument(
        "--seed", type=int,
        default=15,
        help="Random seed"
    )

    parser.add_argument(
        "-zc", "--zero_column", type=str,
        default="mean",
        help="What column name the zero corresponds to"
    )

    parser.add_argument(
        "-apm", '--align_pos_max',
        default=False, action="store_true",
        help="Align the maximal positions"
    )

    parser.add_argument(
        "-ksa", '--kill_small_amplitude',
        default=False, action="store_true",
        help="max - mean > 3 cut"
    )

    parser.add_argument(
        "-c", '--cuts',
        default=False, action="store_true",
        help="Jut cuts on pos_max and min_std_pos_neg_red"
    )
    parser.add_argument(
        "-nzc", '--zero_cuts',
        default=False, action="store_true",
        help="Jut cuts on min_std_pos_neg_red"
    )

    args = parser.parse_args()
    randomise_discretisation = args.randomise_discretisation
    eps = args.epsilon
    config_name = args.data
    seed = args.seed
    zero_column = args.zero_column
    align_pos_max = args.align_pos_max
    ksa = args.kill_small_amplitude
    nc = args.no_cuts

    # PATHS -----------------------------------------------
    rng = np.random.default_rng(seed)
    name = (f"data_{config_name}_eps_{eps}"
        f"_rd_{randomise_discretisation}")
    if align_pos_max:
        name += "_apm"
    if ksa:
        name += "_ksa"
    if nc:
        name += "_nc"
    image_path = op.join(
        var.image_path,
        name
    )

    pickle_path = var.pickle_path
    root_path = var.root_path

    ########################################################
    ####################  LOAD ROOT FILE ###################
    ########################################################

    ## COLUMNS =============================================

    list_voi = [
        'pos_max', #'pos_min',
        'std', 'pedestal',
        'std_pos', 'std_neg',
        'max', 'min', 'mean'
    ]

    columns = [
        'max_m_mean',
        'min_std_pos_neg',
        'min_std_pos_neg_redpn',
        'max_m_mean_red',
    ]

    other_columns = [
        "pos_first_neg_after_max",
        "nb_peaks_sup_80_wrt_min"
    ]

    ## LOAD THE ROOT FILE ==================================

    print(f"LOAD ROOT FILE {config_name}")

    file_path = files.get_path_processed_root(config_name)

    dataframe, column_labels = files.load_and_process_dataframe(
        file_path, list_voi, columns, other_columns,
        branches=defs.list_samples_columns+["eta", "phi"],
        verbose=True,
        rng=rng
    )

    ## CUTS ==============================================
    print("Remove events with std == 0")
    dataframe = dataframe.query("std!=0")

    if not nc:
        list_cut_columns = list(data_cuts.dict_signal_cuts.keys())
    else:
        list_cut_columns = ["min_std_pos_neg_redpn", "pos_max"]


    print("CUTS EFFICIENT ON SIGNAL")
    data_cuts.apply_signal_cuts(dataframe, list_cut_columns)

    if ksa:
        print("Kill small amplitude: max - mean > 3")
        dataframe = dataframe.query("max - min > 3")

    ## NORMALISATION ====================================
    normalize.normalise_samples(
        dataframe, zero_column=zero_column
    )

    ## Align the pos_max ==================================
    if align_pos_max:
        print("LINE UP MAX POSITIONS")
        normalize.get_dataframe_pos_max_lined_up(
            dataframe, pos_max_opt=6
        )

    ########################################################
    ######################## DBSCAN ########################
    ########################################################

    CLU_LABEL_NAME = "HDBSCAN"
    CLU_LABEL_ALIAS = "HDBSCAN"

    features = defs.list_samples_columns

    ## Clustering ---------------------------------
    print("CLUSTERING ...")
    start = time.time()
    if eps is None:
        clusterer = hdbscan.HDBSCAN(
            min_cluster_size=20, min_samples=1,
            prediction_data=True
        )
    else:
        clusterer = hdbscan.HDBSCAN(
            min_cluster_size=20, min_samples=1,
            cluster_selection_epsilon=eps,
            prediction_data=True
        )
    clusterer.fit(dataframe[features])
    time_taken = time.time() - start
    print(f"Time taken: {time_taken}")
    dataframe[CLU_LABEL_NAME] = clusterer.labels_

    clu_labels = np.unique(dataframe[CLU_LABEL_NAME])

    print("Labels:", clu_labels)
    nb_labels = len(clu_labels)

    ## Plots --------------------------------------
    print("PLOT CLUSTERS")
    # Clustering labels
    bins = clustering.get_edges_from_clust_labels(
        clu_labels
    )
    fig, ax = lp.plot_hist(
        dataframe[CLU_LABEL_NAME],
        CLU_LABEL_ALIAS,
        logscale=True,
        bins=bins,
    )
    lp.save_fig(fig, image_path, "clu_labels")
    plt.close(fig)

    if nb_labels <= 15:
        # 2D plots
        print("PLOT 2D HISTOGRAMMES")

        dict_figs = clustering.plot_clusters_hist2d(
            dataframe,
            ['max_m_mean', "pos_max"],
            column_labels,
            CLU_LABEL_NAME,
            logscale=True,
            dict_color_clu_cats=dict_color_clu_labels
        )

        for fig_name, figs in dict_figs.items():
            fig1, fig2 = figs
            lp.save_fig(fig1, image_path, f"{fig_name}_one_ax")
            lp.save_fig(fig2, image_path, f"{fig_name}_sev_ax")
            plt.close(fig1)
            plt.close(fig2)

    if nb_labels <= 40:
        # Plot samples
        print("PLOT EXAMPLES OF SAMPLES IN CLUSTERS")
        full_path = op.join(
            image_path, "samples.pdf"
        )

        with PdfPages(full_path) as pdf:
            for i in range(20):
                fig, _ = clustering.plot_samples_clusters(
                    dataframe,
                    CLU_LABEL_NAME,
                    dict_color_clu_labels=dict_color_clu_labels
                )
                fig.tight_layout()
                pdf.savefig(fig)
                plt.close(fig)

        full_path = op.join(
            image_path, "samples_with_max_m_mean_sup_3.pdf"
        )
        with PdfPages(full_path) as pdf:
            for i in range(20):
                fig, _ = clustering.plot_samples_clusters(
                    dataframe.query("max_m_mean > 3"),
                    CLU_LABEL_NAME,
                    dict_color_clu_labels=dict_color_clu_labels
                )
                fig.tight_layout()
                pdf.savefig(fig)
                plt.close(fig)

    ########################################################
    ##################### SAVE RESULT ######################
    ########################################################

    # Save ROOT file
    import uproot4
    full_output_path = op.join(root_path,
        f"{name}.root")
    print(f"SAVE ROOT FILE in {full_output_path}")
    try_makedirs(root_path)
    out_file = uproot4.recreate(full_output_path)
    out_file[defs.proc_tree_name] = dataframe

    # Save clusterer
    full_pickle_path = op.join(pickle_path, f"{name}.pickle")
    try_makedirs(pickle_path)

    print("SAVE CLUSTERER")
    dump_pickle(
        {
            'clusterer': clusterer,
            'features': features,
            'time': time_taken
        },
        full_pickle_path
    )
