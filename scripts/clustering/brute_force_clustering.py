""" Clustering is applied to the 32 points of each
sample.

The position of the max are always aligned.
"""

############################################################
########################  LIBRARIES ########################
############################################################

import argparse
import os.path as op
from copy import deepcopy
import time

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import pandas as pd
import hdbscan

import var
from clustering_strategy import clustering_strategies

from lib import plot as lp, data, clustering, utils
from lib.utils import try_makedirs
from project import defs, files, normalize
from project.defs_range import column_ranges, column_nbins
from project.defs_label import detector_names



if __name__=="__main__":
    ########################################################
    ########################  PARSER #######################
    ########################################################

    parser = argparse.ArgumentParser(
        "Apply clustering algorithm to the 32 points"
    )

    parser.add_argument(
        '--data', required=True,
        type=str,
        help="ROOT file configuration as defined in"
        "project/configuration.py"
    )

    parser.add_argument(
        '-cs', '--clustering_strategy', required=True,
        type=str,
        help="clustering stategy, as defined in "
        "clustering_strategy"
    )

    parser.add_argument(
        '-d', '--detector', required=False,
        type=int, default=None,
        help="Detector where the clustering is performed"
        " , if not specified, the whole dataframe is"
        " is taken"
    )

    parser.add_argument(
        "-pp", "--preliminary_plots", 
        action="store_true",
        default=False, required=False,
        help="Preliminary plots before clustering"
    )

    parser.add_argument(
        "-sr", "--save_root", 
        action="store_true",
        default=False, required=False,
        help="Save the clustered dataframe in a ROOT file"
    )

    args = parser.parse_args()
    # eps = args.epsilon
    config_name = args.data
    detector = args.detector
    preliminary_plots = args.preliminary_plots
    save_root = args.save_root

    # Clustering strategy ----------------------------------
    cs_name = args.clustering_strategy
    clustering_strategy = clustering_strategies[cs_name]
    zero = clustering_strategy.get("zero")
    norm = clustering_strategy.get("norm")
    rd = clustering_strategy.get("rd", False)
    apm = clustering_strategy.get("apm", False)
    hyperparams = clustering_strategy.get("hyperparams")
    cuts = clustering_strategy.get("cuts", [])
    max_nb_events = clustering_strategy.get("max_nb_events")
    seed = clustering_strategy.get("seed")
    algo = clustering_strategy.get("algo", "HDBSCAN")

    if hyperparams is None:
        hyperparams = {
            "min_cluster_size": 10,
            "min_samples": 1,
            # no cluster_selection_epsilon
        }

    # columns ---------------------------------------------
    list_voi = clustering_strategy.get("voi", [])
    columns = clustering_strategy.get("columns", [])
    other_columns = clustering_strategy.get("other_columns", [])

    # PATHS -----------------------------------------------
    rng = np.random.default_rng(seed)
    name = (f"data_{config_name}_strategy_{cs_name}")

    if detector is not None:
        name += f"_detector{detector}"
    
    if max_nb_events is not None:
        name += f"_seed{seed}"

    image_path = op.join(
        var.image_path,
        name
    )
    table_path = op.join(
        var.table_path,
        name
    )
    json_path = op.join(
        var.json_path,
        name
    )

    root_path = var.root_path

    ########################################################
    ####################  LOAD ROOT FILE ###################
    ########################################################

    ## LOAD THE ROOT FILE ==================================
    print(f"LOAD ROOT FILE {config_name}")

    file_path = files.get_path_processed_root(config_name)

    other_branches = ["eta", "phi", "region", "detector", "layer"]

    dataframe, column_labels = files.load_and_process_dataframe(
        file_path, list_voi, columns, other_columns,
        branches=defs.list_samples_columns+other_branches,
        verbose=True,
        rng=rng,
        cut=f"detector=={detector}" if detector is not None \
            else None
    )
    dataframe.dropna(inplace=True)
    if len(dataframe)==0:
        print("No events in the dataframe")
        quit()

    ## CUTS ==============================================
    print("Remove events with std == 0")
    dataframe = dataframe.query("std!=0")

    if "min_std_pos_neg_redpn" in dataframe:
        fig, _ = lp.plot_hist2d(
            dataframe["min_std_pos_neg_redpn"], 
            dataframe["max_m_mean"],
            column_labels["min_std_pos_neg_redpn"], column_labels["max_m_mean"],
            logscale=True,
        )
        lp.save_fig(fig, image_path, "min_std_pos_neg_redpn_vs_max_m_mean")
        plt.close(fig)

        if "best_chi2" in dataframe:
            fig, _ = lp.plot_hist2d(
                dataframe.query("min_std_pos_neg_redpn > 0.02")["best_chi2"], 
                dataframe.query("min_std_pos_neg_redpn > 0.02")["max_m_mean"],
                column_labels["best_chi2"], column_labels["max_m_mean"],
                logscale=True,
            )
            lp.save_fig(fig, image_path, "best_chi2_vs_max_m_mean")
            plt.close(fig)

    print("CUTS")
    # data_cuts.apply_signal_cuts(dataframe)
    for cut in cuts:
        data.cut_dataframe(dataframe, cut)

    if len(dataframe)==0:
        print("No events in the dataframe")
        quit()

    ########################################################
    ################### PRELIMINARY PLOTS ##################
    ########################################################

    if preliminary_plots:
        def plot(dfr: pd.DataFrame, end_name=""):
            """ Plot and save:
            * `best_chi2` vs `max_m_mean`
            * `eta` vs `phi`
            * `eta` vs `phi` for different part of the detector
                (if the clustering is performed on the all
                detector)

            Args:
                dfr: dataframe
                end_name: what to add to the name of the plot
                    (to tell the diference between different
                    dataframes)
            """
            # eta vs phi
            fig, _ = lp.plot_hist2d(
                dfr["eta"], dfr["phi"],
                column_labels["eta"], column_labels["phi"],
                logscale=True
            )
            lp.save_fig(fig, image_path, "pre_eta_vs_phi" + end_name)
            plt.close(fig)

            if detector is None:
                dict_figs = clustering.plot_clusters_hist2d(
                    dataframe,
                    ['eta', "phi"],
                    column_labels,
                    "detector",
                    logscale=True,
                    dict_color_clu_cats=defs.dict_color_clu_labels,
                    category_names=detector_names,
                    return_figs=True
                )
                for fig_name, figs in dict_figs.items():
                    fig1, fig2 = figs
                    lp.save_fig(fig1, image_path, 
                        f"pre_{fig_name}_one_ax" + end_name
                    )
                    lp.save_fig(fig2, image_path, 
                        f"pre_{fig_name}_sev_ax" + end_name)
                    plt.close(fig1)
                    plt.close(fig2)

        plot(dataframe)
    
    if max_nb_events is not None:
        nb_events = len(dataframe)
        if nb_events > max_nb_events:
            print(f"Select a sub-sample from the {nb_events} events")
            dataframe = dataframe.sample(
                n=max_nb_events,
                random_state=rng.integers(2**32),
            )
    
    ########################################################
    ######################## DBSCAN ########################
    ########################################################

    
    CLU_LABEL_NAME = algo
    CLU_LABEL_ALIAS = algo

    ## Features -------------------------------------------
    features = defs.list_samples_columns

    features_plot = [
        # "best_chi2",
    ]

    ## Prepare the dataframe ------------------------------
    # The dataframe which the clustering is applied to
    # undergoes some change
    # but we want to keep the original dataframe
    # for visualising the result of the clustering
    
    data.cut_dataframe(
        dataframe, "max != mean_rounded"
    )

    clustering_dataframe = deepcopy(dataframe)

    if rd:
        files.randomise_discretisation(clustering_dataframe)

    if zero is not None:
        clustering_dataframe[defs.list_samples_columns] -= \
            data.get_1D_to_2D(
                clustering_dataframe[zero]
            )
    
    plt.show()
    if norm is not None:
        clustering_dataframe[defs.list_samples_columns] /= \
            data.get_1D_to_2D(
                clustering_dataframe[norm]
            )

    if apm:
        normalize.get_dataframe_pos_max_lined_up(
            clustering_dataframe,
            pos_max_opt=6
        )


    ## Several clustering are performed
    # One clustering on the full sample
    # and one clustering for each detector part
    # For each of them, we'll plot the result of the clustering

    ## Clustering on the full data ------------------------
    print("Number of events in the dataframe:", len(dataframe))
    print("CLUSTERING ...")
    start_time = time.time()

    if algo=="HDBSCAN":
        clusterer = hdbscan.HDBSCAN(
            **hyperparams
        )
    elif algo=="OPTICS":
        from sklearn.cluster import OPTICS
        clusterer = OPTICS(
            **hyperparams
        )
    elif algo=="DBSCAN":
        from sklearn.cluster import DBSCAN
        clusterer = DBSCAN(
            **hyperparams
        )
    elif algo=="Birch":
        from sklearn.cluster import Birch
        clusterer = Birch(
            **hyperparams
        )

    clusterer.fit(clustering_dataframe[features])
    print("Time for clustering:", time.time() - start_time, 's')
    dataframe[CLU_LABEL_NAME] = clusterer.labels_

    clu_labels = np.unique(dataframe[CLU_LABEL_NAME])
    print("Labels:", clu_labels)
    nb_labels = len(clu_labels)

    ########################################################
    #################### VISUALISATION #####################
    ########################################################

    bins = clustering.get_edges_from_clust_labels(
        clu_labels
    )

    ## Clusters -------------------------------------------
    print("Plot the clusters")
    fig, ax = lp.plot_hist(
        dataframe[CLU_LABEL_NAME],
        CLU_LABEL_ALIAS,
        logscale=True,
        bins=bins
    )
    lp.save_fig(
        fig, image_path,
        "clusters"
    )
    plt.close(fig)

    ## # events in each cluster --------------------------------------
    nb_events_clusters = {}
    nb_events_clusters["total"] = len(dataframe)
    for clu_label in clu_labels:
        nb_events_clusters[int(clu_label)] = len(dataframe.query(
            f"{CLU_LABEL_NAME}=={clu_label}"
        ))
    

    utils.try_makedirs(json_path)
    utils.dump_json(
        nb_events_clusters, 
        op.join(json_path, "nb_events_clusters")
    )
    table = []
    for clu_label, nb_events in nb_events_clusters.items():
        table.append([clu_label, nb_events])
    table = np.array(table).T 

    utils.write_table(
        table,
        title="column",
        name="nb_events_clusters",
        path=table_path
    )

    ## Clusters w.r.t. detectors ---------------------------
    if detector is None:
        print("Plot the clusters w.r.t. the detectors")
        atlas_columns = [
            "detector", 
            # "region"
        ]
        
        atlas_column_values = {}
        atlas_column_name = {}
        atlas_column_labels = {}
        for column in atlas_columns:
            atlas_column_values[column] = np.unique(dataframe[column])
            atlas_column_name[column] = column.capitalize()
        
        atlas_column_labels["detector"] = detector_names

        for column in atlas_columns:
            dict_labels = atlas_column_labels.get(column)

            if dict_labels is None:            
                list_labels = [
                    f"{atlas_column_name[column]} {col_val}"
                    for col_val in atlas_column_values[column]
                ]
            else:
                list_labels = [
                    dict_labels[col_val]
                    for col_val in atlas_column_values[column]
                ]

            for method in ["multi_ax", "one_ax_stacked_norm"]:

                fig, _ = lp.multi_plot_hist_same_col(
                    [dataframe.query(f"{column}=={col_val}")
                    for col_val in atlas_column_values[column]],
                    CLU_LABEL_NAME,
                    CLU_LABEL_ALIAS,
                    list_labels,
                    list_colors=["r", 'darkorange', 
                    'green', 'blue', 'purple', 'yellow'],
                    bins=bins,
                    method=method,
                    bin_width_scale=0.8 if method == "one_ax_stacked_norm"\
                        else 1.,
                    max_ncols=2,
                )
                lp.save_fig(
                    fig, image_path,
                    f"clusters_{column}_{method}"
                )
                plt.close(fig)

    if nb_labels <= 15:
        ## Where the clusters are ---------------------------
        print("Plot the 2D histograms")
        clustering.plot_clusters_hist2d(
            dataframe,
            features_plot + ['max_m_mean', "pos_max"],
            column_labels,
            CLU_LABEL_NAME,
            logscale=True,
            dict_color_clu_cats=defs.dict_color_clu_labels,
            column_ranges=column_ranges,
            column_nbins=column_nbins,
            path=op.join(image_path, "clusters"),
            name="result",
            return_figs=False
        )
        # for fig_name, figs in dict_figs.items():
        #     fig1, fig2 = figs
        #     lp.save_fig(fig1, image_path, f"{fig_name}_one_ax")
        #     lp.save_fig(fig2, image_path, f"{fig_name}_sev_ax")
        #     plt.close(fig1)
        #     plt.close(fig2)
        clustering.plot_clusters_hist2d(
            dataframe,
            ['eta', "phi"],
            column_labels,
            CLU_LABEL_NAME,
            logscale=True,
            dict_color_clu_cats=defs.dict_color_clu_labels,
            column_ranges=column_ranges,
            column_nbins=column_nbins,
            return_figs=False,
            path=op.join(image_path, "clusters"),
            name="result"
        )
        # for fig_name, figs in dict_figs.items():
        #     fig1, fig2 = figs
        #     lp.save_fig(fig1, image_path, f"{fig_name}_one_ax")
        #     lp.save_fig(fig2, image_path, f"{fig_name}_sev_ax")
        #     plt.close(fig1)
        #     plt.close(fig2)

    if nb_labels <= 60:
        ## What the clusters look like -----------------------
        print("Plot examples of samples in clusters")
        full_path = op.join(
            image_path, "samples.pdf"
        )

        with PdfPages(full_path) as pdf:
            for i in range(20):
                fig, _ = clustering.plot_samples_clusters(
                    dataframe,
                    CLU_LABEL_NAME,
                    dict_color_clu_labels=defs.dict_color_clu_labels,
                    one_fig=True
                )
                fig.tight_layout()
                pdf.savefig(fig)
                plt.close(fig)

        for i in range(2):
            clustering.plot_samples_clusters(
                dataframe,
                CLU_LABEL_NAME,
                dict_color_clu_labels=defs.dict_color_clu_labels,
                title=None,
                name=f"HDBSCAN",
                path=op.join(image_path, f"samples_{i}"),
                one_fig=False
            )
        
    
        print("Plot the sum of several samples in a cluster")
        # We need to normalise the dataframe again before summing
        # So that their amplitude matches
        normalised_df = deepcopy(dataframe)
        normalised_df[defs.list_samples_columns] -= \
            data.get_1D_to_2D(normalised_df["mean_rounded"])
        normalised_df[defs.list_samples_columns] /= \
            data.get_1D_to_2D(normalised_df["max_m_mean_rounded"])

        clustering.plot_samples_clusters(
            normalised_df,
            CLU_LABEL_NAME,
            dict_color_clu_labels=defs.dict_color_clu_labels,
            method="sum",
            title=None,
            path=op.join(image_path, f"samples_summed"),
            name="HDBSCAN",
            one_fig=False
        )

        clustering.plot_samples_clusters(
            normalize.get_dataframe_pos_max_lined_up(
                    normalised_df, inplace=False
            ),
            CLU_LABEL_NAME,
            dict_color_clu_labels=defs.dict_color_clu_labels,
            method="sum",
            title=None,
            path=op.join(image_path, f"samples_summed_apm"),
            name="HDBSCAN",
            one_fig=False
        )

    ## Save ROOT file ------------------------------------------------
    if save_root:
        import uproot4
        full_output_path = op.join(
            root_path,
            f"{name}.root")
        print(f"SAVE ROOT FILE in {full_output_path}")
        try_makedirs(root_path)
        out_file = uproot4.recreate(full_output_path)
        out_file[defs.proc_tree_name] = dataframe