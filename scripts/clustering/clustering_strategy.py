""" Define the clustering strategies
to be applied to the dataframe.
"""

from copy import deepcopy
from data_cuts import dict_signal_cuts


clustering_strategies = {}

list_voi = [ # basic boi
    'pos_max',
    'std', 'pedestal',
    'std_pos', 'std_neg',
    'max', 'min', 'mean'
]

columns = [ # basic computed columns
    'max_m_mean',
    'min_std_pos_neg',
    'min_std_pos_neg_redpn',
    'mean_rounded', # we'll often need this one as well
    'max_m_mean_rounded'
]

clustering_strategies["default"] = {
    # the zero column is round(mean)
    "zero": "mean_rounded",
    # the distributions are divided by max - mean before clustering
    "norm": "max_m_mean_rounded",
    # No adding random numbers to smear the discretisation
    "rd": False,
    # Align the position of the max
    "apm": True,
    # Hyperparameters of the clustering (HDBSCAN)
    "hyperparams": {
        "min_cluster_size": 10,
        "min_samples": 1,
        # no cluster_selection_epsilon
    },
    "voi": list_voi,
    "columns": columns,
}

####################################################################
############################## SIGNAL ##############################
####################################################################

# for the cuts
other_columns = [
    "pos_first_neg_after_max",
    "nb_peaks_sup_80_wrt_min",
    "best_chi2"
]

## Signal --------------------------------------------

clustering_strategies["signal"] = deepcopy(
    clustering_strategies["default"]
)
clustering_strategies["signal"]["other_columns"] = other_columns
clustering_strategies["signal"]["cuts"] = list(dict_signal_cuts.values()) + [
    "best_chi2 < 0.015",
    'max - min > 3'
]

layers = [0, 1, 2, 3]
for layer in layers:
    clustering_strategies[f"signal_layer{layer}"] = deepcopy(
        clustering_strategies["signal"]
    )

    clustering_strategies[f"signal_layer{layer}"]["cuts"] += [f"layer=={layer}"]    
    clustering_strategies[f"signal_layer{layer}"]["max_nb_events"] = 75000
    clustering_strategies[f"signal_layer{layer}"]["seed"] = 30



# clustering_strategies["signal_lim"] = deepcopy(
#     clustering_strategies["signal"]
# )
# clustering_strategies["signal_lim"]["max_nb_events"] = 75000
# clustering_strategies["signal_lim"]["seed"] = 30


## signal_best_chi2 ------------------------------------

clustering_strategies["signal_best_chi2"] = deepcopy(
    clustering_strategies["default"]
)
clustering_strategies["signal_best_chi2"]["other_columns"] = other_columns
clustering_strategies["signal_best_chi2"]["cuts"] = [
    dict_signal_cuts["min_std_pos_neg_redpn"],
    "best_chi2 < 0.015",
    'max - min > 3'
]

## all ------------------------------------

clustering_strategies["all"] = deepcopy(
    clustering_strategies["default"]
)
clustering_strategies["all"]["other_columns"] = ["best_chi2"]
clustering_strategies["all"]["cuts"] = [
    "min_std_pos_neg_redpn > 0.02",
    'max - min > 3'
]

## all_posmax_inf ------------------------------------


clustering_strategies["all_noapm"] = deepcopy(
    clustering_strategies["default"]
)
clustering_strategies["all_noapm"]["cuts"] = [
    "min_std_pos_neg_redpn > 0.02",
    'max - min > 3'
]

clustering_strategies["all_noapm"]["apm"] = False

## all_posmax_inf ------------------------------------

clustering_strategies["all_posmax_inf"] = deepcopy(
    clustering_strategies["default"]
)
clustering_strategies["all_posmax_inf"]["cuts"] = [
    "pos_max <= 20",
    "min_std_pos_neg_redpn > 0.02",
    'max - min > 3'
]

## all_gen ------------------------------------

clustering_strategies["all_gen"] = deepcopy(
    clustering_strategies["default"]
)
clustering_strategies["all_gen"]["cuts"] = [
    'max - min > 3'
]

clustering_strategies["all_gen"]["max_nb_events"] = 75000
clustering_strategies["all_gen"]["seed"] = 15

clustering_strategies["all_gen2"] = deepcopy(
    clustering_strategies["all_gen"]
)

clustering_strategies["all_gen2"]["hyperparams"] = {
    'min_samples': 3,
    "min_cluster_size": 10,
    
}



clustering_strategies["all_gen_optics"] = deepcopy(
    clustering_strategies["all_gen"]
)

clustering_strategies["all_gen_optics"]["algo"] = "OPTICS"
clustering_strategies["all_gen_optics"]["hyperparams"] = {
    'min_samples': 3,
    # 'eps': 1.,
    "xi": 0.15
}

clustering_strategies["all_gen_dbscan"] = deepcopy(
    clustering_strategies["all_gen"]
)

clustering_strategies["all_gen_dbscan"]["algo"] = "DBSCAN"
clustering_strategies["all_gen_dbscan"]["hyperparams"] = {
    'min_samples': 3,
    'eps': 0.5
}

clustering_strategies["all_gen_birch"] = deepcopy(
    clustering_strategies["all_gen"]
)

clustering_strategies["all_gen_birch"]["algo"] = "Birch"
clustering_strategies["all_gen_birch"]["hyperparams"] = {
    "n_clusters": 10
}


## Noise ---------------------------------------------

clustering_strategies["noise"] = deepcopy(
    clustering_strategies["default"]
)
clustering_strategies["noise"]["other_columns"] = other_columns
clustering_strategies["noise"]["cuts"] = [
    "best_chi2 > 0.015",
    "min_std_pos_neg_redpn > 0.02",
    'max - min > 3'
]

## Small ------------------------------------

clustering_strategies["small"] = deepcopy(
    clustering_strategies["default"]
)
clustering_strategies["small"]["other_columns"] = ["best_chi2"]
clustering_strategies["small"]["cuts"] = [
    "min_std_pos_neg_redpn > 0.02",
    "max - min <= 3"
]

clustering_strategies["small"]["hyperparams"]["cluster_selection_epsilon"] = 1.5

## peaked ------------------------------------
clustering_strategies["peaked"] = deepcopy(
    clustering_strategies["default"]
)
clustering_strategies["peaked"]["other_columns"] = ["best_chi2"]
clustering_strategies["peaked"]["cuts"] = [
    "min_std_pos_neg_redpn <= 0.02",
    "max - min > 3"
]

# clustering_strategies["small"]["hyperparams"]["cluster_selection_epsilon"] = 1.5


## Noise ---------------------------------------------

clustering_strategies["noise_layer03"] = deepcopy(
    clustering_strategies["default"]
)
clustering_strategies["noise_layer03"]["other_columns"] = other_columns
clustering_strategies["noise_layer03"]["cuts"] = [
    "best_chi2 > 0.015",
    'max - min > 3'
]

 