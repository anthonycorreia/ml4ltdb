""" (deprecated)
Cut applied to data, efficient in signal 
"""

import pandas as pd

dict_signal_cuts = {
    # Events which don't reach zero after reaching the max
    "pos_first_neg_after_max": "pos_first_neg_after_max!=-1",
    # Peaked background
    "min_std_pos_neg_redpn": "min_std_pos_neg_redpn > 0.02",
    # Events with many peaks w.r.t. the max
    "nb_peaks_sup_80_wrt_min": "nb_peaks_sup_80_wrt_min == 1",
    # Events with maximum at the end of the 32 samples
    "pos_max": "pos_max <= 20"
}

def apply_signal_cuts(
    dataframe: pd.DataFrame, 
    list_cut_columns=list(dict_signal_cuts.keys())):
    """
    Apply cuts to remove obvious background or signal
    hard to tell apart from background. In-place.

    Args:
        dataframe: the dataframe which cuts are applied to
        list_cut_columns: list of columns which cuts are
            applied to. Each column correspond to a cut
            defined in `dict_signal_cuts`
    """

    for cut_column in list_cut_columns:
        cut = dict_signal_cuts[cut_column]
        len_before = len(dataframe)
        dataframe.query(cut, inplace=True)
        len_after = len(dataframe)
        print(f"Cut {cut} -> efficiency {(len_before - len_after)/len_before}")



