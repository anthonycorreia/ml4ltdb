""" Fonctions used in the notebooks 
"""

from lib.utils import array_like
from typing import Union, List, Optional, Dict

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

from lib import plot as lp
from project import defs_label

################################################################
########################### PLOTTING ###########################
################################################################


def plot_hist_each_layer(
    dataframes,
    var1: array_like, 
    var2: Optional[array_like] = None,
    column_labels: Optional[Dict]= None,
    **kwargs
)->Union[mpl.figure.Figure, mpl.axes.Axes]:
    """ Plot 2D histograms for each layer (in separate plots)

    Args:
        var1, var2: the 2 variables to plot.If  `var2` not
            is not given, a 1D histogram is plotted

    Returns:
        fig, axes 
    """

    if column_labels is None:
        column_labels = defs_label.column_labels
    
    layers = list(dataframes.keys())

    fig, axes = plt.subplots(1, len(layers), figsize=(8*len(layers), 6))
    if len(layers)==1:
        axes = [axes]
        
    for i, layer in enumerate(layers):
        if var2 is None:
            lp.plot_hist(
                dataframes[layer][var1], column_labels[var1],
                ax=axes[i],
                **kwargs
            )
        else:
            lp.plot_hist2d(
                dataframes[layer][var1], 
                dataframes[layer][var2],
                column_labels[var1], column_labels[var2],
                ax=axes[i],
                fig=fig,
                **kwargs
            )
        axes[i].set_title(f"Layer {layer}")

    plt.tight_layout()

    return fig, axes


################################################################
########################### COUNTING ###########################
################################################################

def get_channel_id_counts(dataframe):
    """ Find the number of events in each channel.

    Args:
        dataframe: dataframe
    
    Returns:
        channel_id_counts:  associates a channelId 
            with its count
    """
    counts = dataframe[["channelId", "s0"]]\
        .groupby("channelId").count()\
            .sort_values("s0", ascending=False)
    
    return {
        channel_id: count 
        for channel_id, count in zip(
            counts.index.values, 
            counts["s0"].values
        )
    }

def select_new_predominant_channelId(
    channel_id_counts_layers: Dict,
    previous_channel_id_counts_layers: Optional[Dict]=None,
    min_count: int = 20
):
    """ Select the channels that have enough counts,
    and that do not belong to another dictionnary.

    Args:
        channel_id_counts_layers: associates `channelId`
            with their counts, that for each layer:
            `{layer: {channelId: count}}`
        previous_channel_id_counts_layers: the channels that
            are retained must not be in the dictionnary
        min_count: minimal number of counts for a channel to be
            kept
    
    Returns:
        predominant_new_channelId_layers: `{layer: {channelId: count}}`
            for new channels and enough counts
    """
    if previous_channel_id_counts_layers is None:
        previous_channel_id_counts_layers = {}
    predominant_channelId_layers = {}
    predominant_new_channelId_layers = {}
    for layer, channel_id_counts in channel_id_counts_layers.items():
        if layer not in previous_channel_id_counts_layers:
            previous_channel_id_counts_layers[layer] = {}
        
        predominant_channelId_layers[layer] = {
            channel_id: counts
            for channel_id, counts in channel_id_counts_layers[layer].items()
            if channel_id not in previous_channel_id_counts_layers[layer]
        }
        

        predominant_new_channelId_layers[layer] = {
            channel_id: counts
            for channel_id, counts in predominant_channelId_layers[layer].items()
            if counts > min_count
        }
    
    return predominant_new_channelId_layers