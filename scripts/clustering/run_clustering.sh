for run in 1 2 3 4 5 6 7
do
	for layer in 0 1 2 3
    do
        python ./clustering_data_signal.py --data run${run}_signal_lim_back -cs signal_layer$layer
    done
    for d in 0 1 2 3 4
    do
        python ./clustering_data_signal.py --data run${run}_signal_lim_back -cs signal_layer1 -d $d
        python ./clustering_data_signal.py --data run${run}_signal_lim_back -cs signal_layer2 -d $d
    done
done