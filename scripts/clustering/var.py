""" Global variables for clustering
"""


from project import loc, defs
import os.path as op

image_path = op.join(
    loc.images_folder, "clustering"
)

pickle_path = op.join(
    loc.pickle_folder, "clustering"
)
json_path = op.join(
    loc.json_folder, "clustering"
)
table_path = op.join(
    loc.table_folder, "clustering"
)

root_path = op.join(
    defs.proc_folder, 
    "clustering"
)