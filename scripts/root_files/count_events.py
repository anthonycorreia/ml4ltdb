"""Count the number of events in each run"""

import os.path as op

import uproot4
from lib import utils, rootdata  as lrd
from project import configuration, files, defs, loc

if __name__ == '__main__':
    list_runs = list(configuration.run_number_dict.values())

    dict_run_counts = {}
    for run in list_runs:
        print("Run", run)
        dict_run_counts[run] = 0
        list_paths = files.get_paths_run(run=run)

        for path in list_paths:
            print(path)
            dict_run_counts[run] += \
                uproot4.open(path)[defs.unproc_tree_name]["layer"].num_entries

        utils.dump_json(
            dict_run_counts,
            op.join(loc.json_folder, "counts"),
        )
        utils.write_table(
            [list(dict_run_counts.keys()), list(dict_run_counts.values())],
            "counts",
            path=loc.table_folder
        )

