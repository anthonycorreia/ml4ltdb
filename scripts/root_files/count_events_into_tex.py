"""Turn the json file that indicates how much events they are in each run, into a latex table."""

import os.path as op

import uproot4
from lib import utils, rootdata  as lrd
from project import configuration, files, defs, loc

if __name__ == '__main__':

    run_counts = utils.load_json(
        op.join(loc.json_folder, "counts"),
    )

    utils.write_table(
        [list(run_counts.keys()), list(run_counts.values())],
        "counts",
        path=loc.table_folder
    )
