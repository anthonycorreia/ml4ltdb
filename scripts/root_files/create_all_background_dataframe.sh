for run in 1 2 3 4 5 6 7
do
    python save_processed_dataframe.py --config run${run}_oscillating
    python save_processed_dataframe.py --config run${run}_pos_peak
    python save_processed_dataframe.py --config run${run}_neg_peak
done