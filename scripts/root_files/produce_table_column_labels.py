""" Produce a table that gives the notation of few voi.
"""

from project import loc, col
from project.col import get_formula_columns
from project.defs_label import column_labels
from lib.utils import write_table

if __name__=="__main__":
    descriptions = {
        "mean": "Average of the 32 values",
        "mean_rounded": "Rounded average of the 32 values",
        "std" : "Standard deviation of the 32 values",
        "max" : "Maximum among the 32 values",
        "min" : "Minimum among the 32 values",        
    }
    table = [["Notation", "Meaning"]]

    for voi, description in descriptions.items():
        if voi in column_labels:
            label = column_labels[voi]
        else:
            label = col.get_formula_labels([voi], column_labels)[voi]

        table.append(
            [label, description]
        )

    write_table(
        table, name="voi_labels",
        path=loc.table_folder
    )
