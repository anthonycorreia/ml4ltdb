#!/home/anthony/anaconda3/bin/python
"""
Recreate ROOT files for a given run

* only contains the variables of interest
* does not contain non-physical events (only NaN values)
* contains some variables computed such as the mean, std, etc.


Author: Anthony Correia
Date: 10 Jan. 2021
"""

import argparse

import uproot4

from project import defs, files
from lib import rootdata as lrd
from lib.utils import try_makedirs


if __name__ == '__main__':

    # PARSER : RUN =================================================

    parser = argparse.ArgumentParser(
        "Recreate ROOT files"
    )

    parser.add_argument(
        '--run', required=True,
        type=str,
        help="Run")

    parser.add_argument(
        '--step_size', required=False,
        type=int, default=100000,
        help="Size of each batch of dataframe loaded"
        "into memory"
    )

    parser.add_argument(
        '--drop_samples', default=True,
        action="store_true",
        help="Don't save the `samples[i]` columns"
        "of the dataframe"
    )

    args = parser.parse_args()

    run = args.run
    step_size = args.step_size
    drop_samples = args.drop_samples

    # PATH =======================================================
    end = "_without_samples" if drop_samples else ""

    list_paths = files.get_paths_run(run=run)
    output_path = files.get_path_run_processed(run=run, end=end)

    print("OUTPUT ROOT FILE:", output_path)

    try_makedirs(defs.proc_folder)
    # OUTPUT FILE
    out_file = uproot4.recreate(output_path)

    i = 0
    for dataframe in lrd.load_ROOT_into_dataframe(
            paths=list_paths,
            step_size=step_size,
            variables=defs.unproc_kept_variables,
            tree_name=defs.unproc_tree_name,
            verbose=False,
            library="pd"):

        print(f"Batch {i}", '='*10)

        # PROCESSING ===============================================
        print("Processing")
        dataframe = files.process_dataframe(dataframe, verbose=False)

        # COMPUTE VARIABLES OF INTEREST ============================
        print("Compute V.O.I.")
        dataframe = files.compute_voi(
            dataframe=dataframe, verbose=False
        )

        # WRITTING =================================================
        if drop_samples:
            dataframe.drop(columns=defs.list_samples_columns,
                           inplace=True)

        print("Writting")
        if defs.proc_tree_name in out_file:
            out_file[defs.proc_tree_name].extend(dataframe)

        else:
            out_file[defs.proc_tree_name] = dataframe

        i += 1

    out_file.close()
