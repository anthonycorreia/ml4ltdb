""" Save a processed dataframe, in order not to have
to re-process it every time.

The aim of this script is not to lose time during the test of
BDTs, clustering, ... applied to real data.

Specify the configuration using the argument `--config`.
The configurations can be specified in `project/configuration.py`,
in the dictionnary `configurations`.

Author: Anthony Correia
Date: 02 Feb. 2021
"""

import os.path as op
import argparse
import matplotlib

import uproot4
import pandas as pd
import numpy as np

from project.configuration import configurations
from project import defs, files, col, compute_chi2
from lib import rootdata as lrd, data
from lib.utils import try_makedirs

if __name__ == '__main__':

    # PARSER ==================================
    parser = argparse.ArgumentParser(
        "Process and save ROOT files"
    )

    parser.add_argument(
        '--config', required=True,
        type=str,
        help="Name of the ROOT file that will be saved."
        "It is associated with a specific configuration."
    )

    parser.add_argument(
        '--step_size', required=False,
        type=int, default=1000000,
        help="Size of each batch of dataframe loaded"
        "into memory"
    )

    args = parser.parse_args()
    config_name = args.config
    step_size = args.step_size

    configuration = configurations[config_name]
    run = configuration['run']
    num = configuration.get("num")
    cut = configuration.get("cut")
    list_voi = configuration.get("list_voi", [])
    columns_to_compute = configuration.get("columns_to_compute", [])
    drop_samples = configuration["drop_samples"]
    folder = configuration.get("folder")
    frac = configuration.get("frac")
    max_nb_events = configuration.get("max_nb_events")
    entry_start = configuration.get("entry_start")
    backward = configuration.get("backward", False)
    print("Maximal number of events treated:", max_nb_events)
    print("Entry start:", entry_start)

    if "std" not in list_voi:
        list_voi.append("std")

    if frac is not None:
        rng = np.random.default_rng(
            seed=configuration.get("seed")
        )

    # comp_chi2 = configuration.get("compute_chi2", False)
    # comp_peak_voi = configuration.get("peak_voi", False)
    # comp_threshold_voi = configuration.get("threshold_voi", False)
    list_functions_run = configuration.get("list_functions_run", [])

    after_cuts = configuration.get("after_cuts")

    # PATH ===================================
    # output
    if folder is not None:
        output_path = op.join(defs.proc_folder, folder)
    else:
        output_path = defs.proc_folder

    full_output_path = op.join(output_path, config_name + ".root")
    try_makedirs(output_path)
    out_file = uproot4.recreate(full_output_path)

    # input
    if num is None:
        list_paths = files.get_paths_run(run=run)
    else:
        list_paths = [files.get_path_run_num(run=run, num=num)]
    if backward:
        list_paths = list_paths[::-1]
    print("INPUTS", list_paths)
    print("OUTPUT FILE:", full_output_path)

    i_batch = 0
    nb_events = 0
    for dataframe in lrd.load_ROOT_into_dataframe(
            paths=list_paths,
            step_size=step_size,
            variables=defs.unproc_kept_variables,
            tree_name=defs.unproc_tree_name,
            verbose=False,
            cut=cut,
            library="np",
            entry_start=entry_start,
            backward=backward):

        print(f"Batch {i_batch}", '='*10)

        # Deal with the annoying `samples` variables ...
        # Numpy Array -> Pandas DataFrame
        n_samples = 32
        for i in range(n_samples):
            dataframe[f'samples[{i}]'] = np.asarray(
                dataframe['samples'][:, i], dtype='int32'
            )

        del dataframe['samples']

        # Numpy Array -> Pandas DataFrame
        dataframe = pd.DataFrame(dataframe)
        if max_nb_events is not None :
            nb_events += len(dataframe)

        # PROCESSING ===============================================
        print("Processing")
        dataframe = files.process_dataframe(dataframe, verbose=False)

        # COMPUTE VARIABLES OF INTEREST ============================
        print("Compute V.O.I.")
        dataframe = files.compute_voi(
            dataframe,
            variables=list_voi,
            verbose=False
        )

        print("Remove events with std == 0")
        dataframe.query("std != 0", inplace=True)
        if "max" in list_voi and "min" in list_voi:
            dataframe.query("max != min", inplace=True)

        print("Compute other columns")
        column_labels = col.compute_all_columns(
            dataframe,
            columns_to_compute,
            list_functions_run=list_functions_run
            # peak_voi=comp_peak_voi,
            # threshold_voi=comp_threshold_voi
        )

        # print("Compute the chi2")
        # chi2 = compute_chi2.compute_chi2_dataframe_to_sig_optimised(
        #     dataframe
        # )
        # dataframe["best_chi2"] = chi2

        if after_cuts:
            print("Final cuts")
            for cut in after_cuts:
                data.cut_dataframe(
                    dataframe, cut,
                    inplace=True
                )

        if frac is not None:
            print(f"Select only {frac*100} % of the dataframe")
            dataframe = dataframe.sample(
                frac=frac,
                random_state=rng.integers(2**32),
            )
            print("Nb of events", len(dataframe))

        # WRITTING ==================================================
        if drop_samples:
            dataframe.drop(columns=defs.list_samples_columns,
                           inplace=True)

        print("Writing")
        if len(dataframe) > 1:
            if defs.proc_tree_name in out_file:
                out_file[defs.proc_tree_name].extend(dataframe)

            else:
                out_file[defs.proc_tree_name] = dataframe
        else:
            print("No events to save")
        i_batch += 1
        
        if max_nb_events is not None:
            if nb_events > max_nb_events:
                print("The maximum number of events have been reached")
                break
    out_file.close()
