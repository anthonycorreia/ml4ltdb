# Simulation

Toy simulated data is generated:
- Noise is generated with a Gaussian
- Signal is generated using the simulated shape stored in `ATLAS_LAr_signal.csv`

## Python scripts

- `fcts.py` and `var.py` contain global variables and functions used for the simulation.
- `models.py` is a module that contains all the functions to generate the signal and background samples
- `interpolation.py`  is a script that allows to interpolate the simulated signal, in case higher granularity is required. It is not used in this analysis.

## Notebooks

In order to develop and test the code, jupyter notebooks have been used. They are located in the subfolder `notebook`.

In `notebook/simulation/`
- `interpolation.ipynb` allows to visualize the result of interpolation
- `simulation_sig_bkg.ipynb` allows the vizualise the simulated signal and background samples and compare the distribution of variables of interest between signal and background.


