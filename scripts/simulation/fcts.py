""" Functions used for the simulation

Author: Anthony Correia
Date: 13 Janvier 2021
"""

import var

# Utils
from typing import Union, Optional, Tuple
from lib.utils import array_like
import os.path as op

# Data
import pandas as pd
import numpy as np

# Plots
from lib import plot as lp, clustering as lc
import matplotlib.pyplot as plt
import matplotlib as mpl
dirname = op.dirname(__file__)

#####################################################################
############################ SIGNAL MODEL ###########################
#####################################################################


def read_signal_csv() -> Union[np.array, np.array]:
    """ Read the csv file `ATLAS_LAr_signal.csv`.
    Shift the time array so that the signal starts at
    :math:`t=0`.

    Returns:
        signal_model_csv_t, signal_model_csv_A: time and
            amplitude array of the signal stored in
            the CSV file.
    """

    signal_model_csv = np.array(
        pd.read_csv(
            op.join(dirname, 'ATLAS_LAr_signal.csv'),
            delimiter=";", decimal=","
        )
    )

    signal_model_csv_t = signal_model_csv[:, 0] - 9  # time
    signal_model_csv_A = signal_model_csv[:, 1]  # Amplitude

    return signal_model_csv_t, signal_model_csv_A


#####################################################################
################################ PLOTS ##############################
#####################################################################


def plot_model(
    array_t: array_like, array_A: array_like,
    s: float = 8.,
    **kwargs
) -> Union[mpl.figure.Figure, mpl.axes.Axes]:
    """
    Plot the signal model.

    Args:
        array_t: array of time (ns)
        array_A: array of amplitude
        s: marker size
        **kwargs: fed into :py:func:`ax.scatter`

    Returns:
        fig, ax: figure and axis
    """

    fig, ax = plt.subplots(figsize=(8, 6))

    ax.scatter(array_t, array_A, s=s, **kwargs)
    ax.set_xlabel("Time (ns)")
    ax.set_ylabel("Amplitude")
    lp.set_default_grid(ax)
    ax.axhline(y=0, color="k", lw=1.)
    ax.set_xlim([array_t[0], array_t[-1]])

    return fig, ax


def get_list_colors_labels(list_compos=['S', 'N', 'P']):
    """ Get the list of colors and labels in the right order.

    Args:
        list_compos: list of components to include in the
            dataframe

    Returns:
        list_colors, list_labels: list of colors and labels
    """
    list_colors = []
    list_labels = []

    for compo in list_compos:
        list_colors.append(var.dict_df_colors[compo])
        list_labels.append(var.dict_df_labels[compo])

    return list_colors, list_labels


from typing import List, Dict
def multi_plot_hist2d_cats(
    dataframe: pd.DataFrame, 
    name_var1: str, name_var2: str,
    label_var1: str, label_var2: str,
    cats : List[str],
    plot_1d: bool = False,
    dict_colors: Dict[str, str] = var.dict_df_colors,
    dict_labels: Dict[str, str] = var.dict_df_labels,
    **kwargs
    ):
    """ Plot the super-imposed and not super-imposed
    2d histograms of `name_var1` and `name_var2`
    for the various categories listed in `cats`.

    Args:
        dataframe: contains the columns `name_var1` 
            and `name_var2` and the flags corresponding
            to the categories in `cats`.
        name_var1, name_var2: name of the 2 columns
            for the 2d histograms
        label_var1, label_var2 : label of the 2 variables
        cats: list of the categories, either
            `'S'` (signal), `'N'` (Noise) or `'P'` (peaked)
        plot_1d: whether to plot 1d hists as well
    """

    list_var1, list_var2 = [], []
    list_labels = []
    list_colors = []
    list_df = []

    for cat in cats:
        df_cat = dataframe.query(f"{var.dict_df_flags[cat]} == True")
        list_var1.append(df_cat[name_var1])
        list_var2.append(df_cat[name_var2])
        list_labels.append(dict_labels[cat])
        list_colors.append(dict_colors[cat])
        list_df.append(df_cat)

    lp.multi_plot_hist2d(
        list_var1, list_var2,
        label_var1, label_var2,
        list_labels,    
        one_ax=True,
        list_colors=list_colors,
        logscale=True,
        **kwargs
    )

    lp.multi_plot_hist2d(
        list_var1, list_var2,
        label_var1, label_var2,
        list_labels,    
        one_ax=False,
        list_colors=list_colors,
        logscale=True,
        **kwargs
    )

    if plot_1d:
        lp.multi_plot_hist(
            list_df, columns=[name_var1, name_var2],
            column_labels = {name_var1: label_var1, name_var2: label_var2}, 
            list_labels=list_labels, logscale=True,
            list_colors=[dict_colors[cat] for cat in cats],
            **kwargs
        )


def plot_result_clustering(
    *args, 
    cats : List[str] = var.all_cats,
    dict_df_flags: Dict[str, str] = var.dict_df_flags,
    dict_df_labels: Dict[str, str] = var.dict_df_labels,
    dict_df_colors: Dict[str, str] = var.dict_df_colors,
    **kwargs
):
    """ Same as :py:func:`lib.clustering.plot_result_custering`,
    with some default parameters
    """
    return lc.plot_result_clustering(
        *args,
        cats=cats, 
        dict_df_flags=dict_df_flags, 
        dict_df_labels=dict_df_labels,
        dict_df_colors=dict_df_colors,
        **kwargs
    )

def plot_result_clustering_hist1d(
    *args, 
    cats : List[str] = var.all_cats,
    dict_df_flags: Dict[str, str] = var.dict_df_flags,
    dict_df_labels: Dict[str, str] = var.dict_df_labels,
    dict_color_clu_cats: Dict[str, str] = var.dict_color_clu_cats,
    **kwargs
):
    """ Same as :py:func:`lib.clustering.plot_result_custering`,
    with some default parameters
    """
    return lc.plot_result_clustering_hist1d(
        *args,
        cats=cats, 
        dict_df_flags=dict_df_flags, 
        dict_df_labels=dict_df_labels,
        dict_color_clu_cats=dict_color_clu_cats,
        **kwargs
    )


def plot_result_clustering_hist2d(
    *args, 
    cats : List[str] = var.all_cats,
    dict_df_flags: Dict[str, str] = var.dict_df_flags,
    dict_df_labels: Dict[str, str] = var.dict_df_labels,
    dict_color_clu_cats: Dict[str, str] = var.dict_color_clu_cats,
    **kwargs
):
    """ Same as :py:func:`lib.clustering.plot_result_custering`,
    with some default parameters
    """
    return lc.plot_result_clustering_hist2d(
        *args,
        cats=cats, 
        dict_df_flags=dict_df_flags, 
        dict_df_labels=dict_df_labels,
        dict_color_clu_cats=dict_color_clu_cats,
        **kwargs
    )