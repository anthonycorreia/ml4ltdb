""" Interpolate the signal model.
Save the result of the interpolation in a `.pickle`
file.

Author: Anthony Correia
Date: 13 January 2022
"""


import var
import fcts

# Data
import numpy as np
import os.path as op
import pickle

# Utils
import argparse

# Plots
from lib import plot as lp
from lib.utils import try_makedirs

# Interpolation
from scipy.interpolate import splrep, splev

if __name__ == "__main__":

    # PARSER ==========================================================
    parser = argparse.ArgumentParser(
        "Interpolate the signal stored in the .CSV file."
    )

    parser.add_argument(
        '--no_plot', default=False,
        action='store_true',
        help="Do not produce plots"
    )

    args = parser.parse_args()
    no_plot = args.no_plot

    # CSV file =========================================================
    signal_model_csv_t, signal_model_csv_A = fcts.read_signal_csv()

    # Interpolated spline ===============================================
    tck = splrep(
        x=signal_model_csv_t, y=signal_model_csv_A,
    )

    # Save in a pickle file =============================================
    path_pickle = op.join(var.pickle_path)

    try_makedirs(path_pickle)
    with open(op.join(path_pickle, var.interpol_pickle_name), 'wb') as f:
        pickle.dump(tck, f)

    # Plot ==============================================================
    if not no_plot:
        # Every DeltaT = 25 ns
        t_max = var.deltaT * var.Nsamples
        signal_model_t = np.arange(0, t_max, var.deltaT)
        print("Array of time:", signal_model_t)
        signal_model_A = splev(signal_model_t, tck)

        fig, ax = fcts.plot_model(
            signal_model_csv_t, signal_model_csv_A,
            label="Simulation"
        )

        array_t_fine = np.linspace(
            signal_model_t[0], signal_model_t[-1], num=1000)
        array_A_fine_interpol = splev(array_t_fine, tck)
        ax.plot(
            array_t_fine, array_A_fine_interpol,
            label="Interpolation", color="red"
        )
        ax.scatter(
            signal_model_t, signal_model_A,
            label=f"Interpolated, with $\Delta T = {var.deltaT}\\text{{ ns}}$",
            color="green"
        )

        ax.legend()

        lp.save_fig(fig, path=var.image_path,
                    name="interpolated_simulated_signal")
