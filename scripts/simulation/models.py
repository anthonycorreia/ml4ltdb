""" Obtains the signal model with the
required time step.
"""





from . import var
from . import fcts

from numba import jit


from project import defs
from lib.utils import array_like
from lib import plot as lp

import numpy as np
import pandas as pd

from typing import Union, Optional, Tuple, Dict

# ========================= PDF 1/x ===================================


def recip_inverse_cdf(
    x: np.array, A_max: float, A_min: float = 1.
) -> np.array:
    """ Implements the function:

    .. math::
        F^{-1} (x)
        = A_{\\text{min}}
        \\exp{\left[
            \ln{\left(\\frac{A_{\\text{max}}}{A_{\\text{min}}}\\right)} x 
            \\right]}

    Args:
        x: where the inverse CDF is evaluated
        A_min, A_max: mininum and maximum values of `x`

    Returns:
        inverse_cdf_x: inverse CDF evaluated at `x`
    """

    return A_min*np.exp(np.log(A_max/A_min)*x)


def inverse_pdf(x: np.array, A_max: float, A_min: float = 1.) -> np.array:
    """ Evaluates the PDF $f$:

    .. math::
        f(x) =  \\frac{1}
        { x \\ln{
            \\left(\\frac{A_{\\text{max}}}{A_{\\text{min}}}\\right)}
        }

    Args:
        x (np.array): where to evuate the pdf $f$
        A_max, A_min : maximum and minimum values of `x`

    Returns:
        pdf_x : pdf `f` evaluated at `x`
    """
    return 1 / (x * np.log(A_max / A_min))


def generate_random_inverse(
        N: int, A_max: float, A_min: float = 1.,
        rng: Optional[np.random.Generator] = None) -> np.array:
    """Generate the distribution whose p.d.f. is:

    .. math:: 
        f(x) = \\frac{1}{x \\ln{\\left(\\frac{A_{\\text{max}}}
        {A_{\\text{min}}}\\right)}}

    Args:
        N : number of events
        A_min, A_max: mininum and maximum values of `x`
        rng: random generator for reproducible randomness

    Returns:
        np.array: Sample drawn from the 
            P.D.F. :math:`\\frac{1}{x}` (after normalisation)

    """
    if rng is None:
        rng = np.random.default_rng()

    uniform = rng.uniform(size=N)

    return recip_inverse_cdf(uniform, A_min, A_max)


# ========================= SIGNAL ===========================


def get_sig_model(
    deltaT: float = var.deltaT,
    n_samples: int = var.n_samples,
    add_zeros=True
) -> Union[np.array, np.array]:
    """Get the signal model for a given time step.
    It is done so WITHOUT INTERPOLATION. Thus, `DeltaT`
    needs to be divisible by the time step used
    in the CSV file `ATLAS_LAr_signal.csv`.

    Args:
        DeltaT: time step
        n_samples: number of time steps
        add_zeros: add zeros so that the length of the
            signal model matches `n_samples`

    Returns:
        array_t, array_A: array of time and amplitude
    """

    signal_model_csv_t, signal_model_csv_A = fcts.read_signal_csv()

    # Characteristics -----------------------------------------
    # of the signal model in the csv file
    n_points_csv = len(signal_model_csv_t)  # number of points

    # of the signal model that will be returned
    delta_step = int(deltaT / var.deltaT_csv)  # time step

    # Arrays --------------------------------------------------
    # Indices of step `deltaT` within the CSV range
    indices_csv = np.arange(0, n_points_csv, delta_step)

    if add_zeros:
        # of time
        array_t = np.arange(0, n_samples*deltaT, deltaT)

        # of amplitude
        n_step_in_csv = len(indices_csv)
        n_step_out_csv = max(0, n_samples - n_step_in_csv)

        array_A = np.concatenate(
            [signal_model_csv_A[indices_csv],
             np.zeros(shape=n_step_out_csv)]
        )

        return array_t, array_A

    else:
        array_A = signal_model_csv_A[indices_csv]
        array_t = np.arange(0, len(array_A)*deltaT, deltaT)

        return array_t, array_A


def get_sig_dataframe(
    S: int, amp_S: Union[array_like, Tuple[float], int],
    amp_N: Union[array_like, float] = 1.,
    std_N: float = 1.,
    random_time_shift: bool = True,
    true_pedestal: bool = False,
    rng: Optional[np.random.Generator] = None,
    
) -> pd.DataFrame:
    """ Get the signal dataframe, with columns
    named `s0`, ..., `s31`.
    Also add 3 columns `is_sig`, `is_noise` and `is_peaked`,
    `True`, `False` and `False`.

    Args:
        S: number of signal events
        amp_S: amplitude of each signal event,
            or tuple `(mean, std)`, which contains 
            the mean and standard variation of
            the normal distribution to generate, or integer
            `A_max` of  the inverse distribution to
            generate.

        amp_N: amplitude of the background in each
            of the signal events
        random_time_shift: randomly shift the signal.
            Note that the all signal will always be
            encompassed within the `n_samples` values.
        true_pedestal: whether noise is also added to the
            pedestal
        rng: random generator for reproducible randomness

    Returns: 
        signal_df: signal dataframe of sig + bkg, with
            columns `'s0'` to `'s{n_samples-1}'` 
    """

    n_samples = len(defs.list_samples_columns)

    if rng is None:
        rng = np.random.default_rng()

    # Signal ==========================================
    if random_time_shift:
        # 1 signal -------------------------------------
        _, signal_array = get_sig_model(add_zeros=False)

        # Empty signal array ---------------------------
        signal_ndarray = np.zeros(
            shape=(S, n_samples)
        )

        # Random shifts -------------------------------
        nb_zeros_to_add = n_samples - len(signal_array)

        # Random start indices for each of the S events.
        indices_start = rng.integers(
            low=0,
            high=nb_zeros_to_add,
            size=(S, 1)
        )

        indices_end = indices_start + len(signal_array)

        # Create the mask of array that will contain
        # the signal --------------------------------
        indices = np.arange(n_samples).reshape(1, n_samples).repeat(
            S, axis=0
        )
        mask = (indices >= indices_start) & (indices < indices_end)

        # Apply the signal ------------------------
        signal_ndarray[mask] = np.tile(signal_array, S)

    else:
        # 1 signal -------------------------------------
        _, signal_array = get_sig_model()

        # Repeat the same signal over and over ---------
        signal_ndarray = np.repeat(
            signal_array.reshape(1, n_samples), S, axis=0
        )

    # Background ======================================
    noise_ndarray = rng.normal(
        scale=std_N,
        size=(S, n_samples)
    )
    if true_pedestal: # cancel the noise at the pedestal
        noise_ndarray[:, 0] = 0

    # Signal + background =============================
    if isinstance(amp_S, tuple) and len(amp_S) == 2:
        amp_S = np.abs(rng.normal(loc=amp_S[0], scale=amp_S[1], size=S))
    elif isinstance(amp_S, int):
        amp_S = generate_random_inverse(N=S, A_max=amp_S)

    signal_df = pd.DataFrame(
        np.round(
            amp_S[:, np.newaxis] * signal_ndarray
            + amp_N * noise_ndarray
        ),
        columns=defs.list_samples_columns
    )
    signal_df['amp_S'] = amp_S

    for compo, flag in var.dict_df_flags.items():
        signal_df[flag] = (compo == "S")

    return signal_df

# ========================= NOISE ===========================


def get_noise_dataframe(
    N: int, amp_N: float = 1., std_N: float = 1.,
    true_pedestal: bool = False,
    rng: Optional[np.random.Generator] = None,
) -> pd.DataFrame:
    """ Generate a dataframe of `N` noisy events,
    that is, generated from a gaussian of standard deviation
    `std_N`. The dataframe contains the columns `'s0'` to
    `'s{n_samples}'`.

    Also add 3 columns `is_sig`, `is_noise` and `is_peaked`,
    `False`, `True` and `False`.

    Args:
        N: number of noisy event
        std_N: standard deviation
        amp_N: background amplitude
        true_pedestal: whether there is noise in the pedestal
        rng: random generator for reproducible randomness

    Returns:
        noise_dataframe: background dataframe
    """

    if rng is None:
        rng = np.random.default_rng()

    n_samples = len(defs.list_samples_columns)

    noise_ndarray = np.random.normal(
        scale=std_N,
        size=(N, n_samples)
    )

    if true_pedestal:
        noise_ndarray[:, 0] = 0

    noise_dataframe = pd.DataFrame(
        np.round(amp_N * noise_ndarray),
        columns=defs.list_samples_columns
    )

    for compo, flag in var.dict_df_flags.items():
        noise_dataframe[flag] = (compo == "N")

    return noise_dataframe


# =========================== PEAKED ============================


@jit(nopython=True)
def generate_mask_peaks(
    n_peaks: np.array,
    length_peaks: np.array,
    seed: int = 0,
    n_samples: int = len(defs.list_samples_columns)
) -> np.array:
    """Generate a mask of where the peaks are supposed to
    be. A peak can start at t = 0.

    Args:
        n_peaks: for each event, number of peaks
        length_peaks: length of every peak
        seed: Seed of the random generation
        n_samples: number of samples for each event

    Returns:
        mask : mask of size `P` * `Nsamples`, where `P` 
            is the number of events.
    """
    # INIT ==============================================
    np.random.seed(seed)
    P = len(n_peaks)  # Number of events
    mask = np.zeros(shape=(P, n_samples), dtype=np.int_)
    list_poss_pos = np.arange(n_samples, dtype=np.int_)

    # LOOP OVER EVENTS ==================================
    total_index = 0
    for i in range(P):
        # Loop over each peak
        j = 0
        while j < n_peaks[i] and mask[i].sum() < n_samples:
            length_peak = length_peaks[total_index]
            list_poss_pos_free = list_poss_pos[~np.asarray(
                mask[i], dtype=np.bool_)]

            # Draw the start of the peak
            start_pos_peak_index = np.random.randint(
                low=0, high=len(list_poss_pos_free))
            start_pos_peak = list_poss_pos_free[start_pos_peak_index]
            end_pos_peak = min(start_pos_peak + length_peak, n_samples)

            mask[i][start_pos_peak:end_pos_peak] = 1

            j += 1
            total_index += 1

    return mask


# def generate_peaked_bkg_array(
#         P: int,
#         pos_peaks: Tuple[int, int],
#         amp_P: np.array) -> np.array:
#     """Generate an array representing `W` events,
#     each event having peaks at positions given by
#     `pos_peaks` and amplitude given by `ampl_peaks`

#     Args:
#         P: number of peaked background events
#         pos_peaks: `(rows, columns)` giving the
#             position of the peaks in the array
#         ampl_P: amplitude of the peaks
#             for each background event

#     Returns:
#         peaked_bkg_array: Array of peaked background
#     """
#     n_samples = len(defs.list_samples_columns)

#     peaked_bkg_array = np.zeros(shape=(P, n_samples))
#     peaked_bkg_array[pos_peaks] = 1
#     peaked_bkg_array *= amp_P.reshape(len(amp_P), 1)

#     return peaked_bkg_array


def get_peaked_dataframe(
    P: int,
    n_peaks: Union[np.array, Tuple[int, int]],
    amp_P: Union[np.array, Tuple[int, int]],
    length_peaks: Union[np.array, Tuple[int, int], int] = 1,
    rng: Optional[np.random.Generator] = None
) -> pd.DataFrame:
    """Get the peaked background dataframe with
    columns named  `s0`, ..., `s31`.

    Also add 3 columns `is_sig`, `is_noise` and `is_peaked`,
    `False`, `False` and `True`.

    Args:
        P: number of peaked background events
        n_peaks: for each event, number of peaks,
            or `(low, high)` bound of the uniform law
            which the number of peaks is drawn from
        length_peaks : Length of a peak. Can be given
            directly with an array. If a tuple `(min, max)`
            is given, each length is drawn randomly
            between `min` and `max`. If an integer 
            is given, the peaks have the same length,
            given by `length_peaks`.        
        amp_P: amplitude of the peaks 
            for each background event, 
            or `(low, high)` bound of the uniform law
            which the number of amplitude is drawn from

    Returns:
        peaked_df: peaked background datframe
    """

    # INIT ==========================================
    if rng is None:
        rng = np.random.default_rng()

    if isinstance(n_peaks, tuple):
        n_peaks = rng.integers(
            low=n_peaks[0], high=n_peaks[1], size=P,
            endpoint=True
        )

    if isinstance(amp_P, tuple):
        amp_P = rng.integers(
            amp_P[0], amp_P[1], size=P,
            endpoint=True
        )

    if isinstance(length_peaks, tuple):
        length_peaks = rng.integers(
            length_peaks[0], length_peaks[1],
            size=n_peaks.sum(),
            endpoint=True
        )
    elif isinstance(length_peaks, int):
        length_peaks = np.full(
            shape=n_peaks.sum(),
            fill_value=length_peaks
        )

    # Generation ===================================

    peaked_bkg_array = generate_mask_peaks(
        n_peaks=n_peaks,
        seed=rng.integers(2**32),
        length_peaks=length_peaks
    )

    peaked_bkg_array *= amp_P.reshape(len(amp_P), 1)

    peaked_dataframe = pd.DataFrame(
        np.round(peaked_bkg_array),
        columns=defs.list_samples_columns
    )

    # Flags ==========================================
    for compo, flag in var.dict_df_flags.items():
        peaked_dataframe[flag] = (compo == "P")

    return peaked_dataframe


# ========================= SIG + BKG ===========================

""" 
compos = {"S": {"S": S, "amp_S": amp_S}}
]
"""


def get_full_dataframe(
    # S: int, B: int,
    # amp_S: float, amp_N: float = 1.,
    # std_N: float = 1.,
    components: Dict[str, Dict],
    random_time_shift=True,
    shift_pedestal: Optional[Tuple[float, float]] = None,
    rng: Optional[np.random.Generator] = None
) -> pd.DataFrame:
    """Generate a full signal + background dataframe.
    The columns `is_sig` and `is_bkg` allow to tell
    signal and background apart.

    Args:
        components: Associates a type of background
            (`'S'` as signal, `'N'` as noise or `'P'` as peaked)
            with its set of parameters given by a dictionnary.

            * For the signal, `S` (number of events)
                `amp_S` (amplitude) 
                and `std_S` (standard deviation)
            * For the noise, `N` (number of events)
                `amp_N` (amplitude) 
                and `std_N` (standard deviation)
            * For the peaks, `P` (number of events)
                `n_peaks` (number of peaks)
                `amp_P` (amplitude of the peaks) 
                and `length_peaks` (length of the peaks)

        amp_S, amp_N: signal and background amplitude
        std_N: standard deviation of the noise
        random_time_shift: randomly shift the signal.
            Note that the all signal will always be
            encompassed within the `n_samples` values.
        shift_pedestal: couple `(mean, std)` to shift
            the position of the pedestal
        rng: random generator for reproducible randomness

    Returns:
        pd.DataFrame: [description]
    """
    if rng is None:
        rng = np.random.default_rng()

    list_dataframes = []
    # Signal ====================================
    if 'S' in components:
        compo_S = components['S']
        if 'N' in components:
            compo_N = components['N']
        else:
            compo_N = {}

        sig_dataframe = get_sig_dataframe(
            S=compo_S['S'],
            amp_S=compo_S['amp_S'],
            amp_N=compo_N.get('amp_N', 1),
            std_N=compo_N.get('std_N', 1),
            random_time_shift=random_time_shift,
            rng=rng,
            true_pedestal=compo_S.get("true_pedestal", False)
        )

        list_dataframes.append(sig_dataframe)

    # Noise ====================================
    if 'N' in components:
        compo_N = components['N']
        noise_dataframe = get_noise_dataframe(
            N=compo_N['N'],
            amp_N=compo_N.get('amp_N', 1),
            std_N=compo_N.get('std_N', 1),
            true_pedestal=compo_N.get("true_pedestal", False),
            rng=rng,
        )

        list_dataframes.append(noise_dataframe)

    # Peaks =====================================
    if 'P' in components:
        compo_P = components['P']
        peaked_dataframe = get_peaked_dataframe(
            P=compo_P['P'],
            n_peaks=compo_P['n_peaks'],
            amp_P=compo_P['amp_P'],
            length_peaks=compo_P.get('length_peaks', 1),
            rng=rng
        )

        list_dataframes.append(peaked_dataframe)

    dataframe = pd.concat(list_dataframes)

    # PEDESTAL ==================================
    if shift_pedestal is not None:
        pedestal = rng.normal(
            loc=shift_pedestal[0],
            scale=shift_pedestal[1],
            size=(len(dataframe), 1)
        )

        dataframe[defs.list_samples_columns] += np.round(
            pedestal.reshape(len(pedestal), 1))

    return dataframe


def get_dict_df(
    dataframe: pd.DataFrame
) -> Dict[str, pd.DataFrame]:
    """Separate the signal (S), noise (N) and peaked (P)
    components of a dataframe.

    Args:
        dataframe: dataframe of signal and background
            events

    Returns:
        dict_df: associates a component
            (`'S'`, `'N'` and/or `'P'`)
            with its dataframe
    """

    dict_df = {}

    for compo, flag in var.dict_df_flags.items():
        df_compo = dataframe.query(f"{var.dict_df_flags[compo]}==True")

        if len(df_compo) > 0:
            dict_df[compo] = df_compo

    return dict_df


if __name__ == "__main__":
    # Test ====================================

    print("Test")

    array_csv_t, array_csv_A = fcts.read_signal_csv()
    array_t, array_A = get_sig_model()

    fig, ax = fcts.plot_model(
        array_csv_t, array_csv_A,
        s=3, color="red",
        label="From the csv file"
    )
    ax.scatter(
        array_t, array_A,
        color="blue",
        label=f"With $\\Delta T = {var.deltaT} \\text{{ ns}}$"
    )
    ax.legend()

    lp.save_fig(
        fig, path=var.image_path,
        name=f"test_signal_model_DeltaT={var.deltaT}"
    )
