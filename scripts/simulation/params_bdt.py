""" Parameters of the BDT training 
"""

bdt_configurations = {}

# Variables -----------------------------------------
N = 100000
S = 100000
P = 100000


shift_pedestal = (600, 150)
amp_S = 30  # (25, 5)
std_N = 1.

# Peaked
n_peaks = (1, 6)  # between 1 and 6 peaks / event
amp_P = (1, 30)  # peak amplitude between 1 and 15
length_peaks = (1, 3)  # length of a peak between 1 and 3

shift_pedestal = (600, 150)

# BDT configurations --------------------------------

# bdt_configurations["BDT_SP"] = {
#     "voi": ["std_neg", "mean", "pedestal", "std"],
#     "columns_to_compute": ["mean_m_pedestal"],
#     "columns": ['std_neg', 'mean_m_pedestal', 'std'],
#     "components" :{
#         "S": {"S": S, "amp_S": amp_S},
#         "N": {"N": 0, "std_N": std_N},
#         "P": {
#             "P": P, "n_peaks": n_peaks,
#             "amp_P": amp_P, "length_peaks": length_peaks
#         }
#     },
#     "shift_pedestal": shift_pedestal,
#     "label": "BDT between signal and peaked"
# }

# bdt_configurations["BDT_SP2"] = {
#     "voi": ["std_neg", "mean", "pedestal", "std"],
#     "columns_to_compute": ["mean_m_pedestal"],
#     "columns": ['std_neg', 'mean_m_pedestal'],
#     "components" :{
#         "S": {"S": S, "amp_S": amp_S},
#         "N": {"N": 0, "std_N": std_N},
#         "P": {
#             "P": P, "n_peaks": n_peaks,
#             "amp_P": amp_P, "length_peaks": length_peaks
#         }
#     },
#     "shift_pedestal": shift_pedestal,
#     "label": "BDT between signal and peaked"
# }

# bdt_configurations["BDT_SP3"] = {
#     "voi": ["std_pos", "std_neg", "mean", "pedestal", "std"],
#     "columns_to_compute": ["mean_m_pedestal", "min_std_pos_neg"],
#     "columns": ['min_std_pos_neg', 'mean_m_pedestal'],
#     "components" :{
#         "S": {"S": S, "amp_S": amp_S},
#         "N": {"N": 0, "std_N": std_N},
#         "P": {
#             "P": P, "n_peaks": n_peaks,
#             "amp_P": amp_P, "length_peaks": length_peaks
#         }
#     },
#     "shift_pedestal": shift_pedestal,
#     "label": "BDT between signal and peaked"
# }

# bdt_configurations["BDT_SP4"] = {
#     "voi": ["std_pos", "std_neg", "mean", "pedestal", "std"],
#     "columns_to_compute": ["mean_m_pedestal", "mean_m_pedestal_red", "min_std_pos_neg"],
#     "columns": ['min_std_pos_neg', 'mean_m_pedestal_red'],
#     "components" :{
#         "S": {"S": S, "amp_S": amp_S},
#         "N": {"N": 0, "std_N": std_N},
#         "P": {
#             "P": P, "n_peaks": n_peaks,
#             "amp_P": amp_P, "length_peaks": length_peaks
#         }
#     },
#     "shift_pedestal": shift_pedestal,
#     "label": "BDT between signal and peaked"
# }

bdt_configurations["BDT_SP5"] = { 
    "voi": ["std_pos", "std_neg", "mean", "pedestal", "std"],
    "columns_to_compute": ["mean_m_pedestal", "min_std_pos_neg", "min_std_pos_neg_red"],
    "columns": ['min_std_pos_neg_red', 'mean_m_pedestal'],
    "components" :{
        "S": {"S": S, "amp_S": amp_S},
        "N": {"N": 0, "std_N": std_N},
        "P": {
            "P": P, "n_peaks": n_peaks,
            "amp_P": amp_P, "length_peaks": length_peaks
        }
    },
    "shift_pedestal": shift_pedestal,
    "label": "BDT between signal and peaked"
}

bdt_configurations["BDT_SP_true_pedestal"] = {  # useless
    "voi": ["std_pos", "std_neg", "mean", "pedestal", "std"],
    "columns_to_compute": ["mean_m_pedestal", "min_std_pos_neg", "min_std_pos_neg_red"],
    "columns": ['min_std_pos_neg_red'],
    "components" :{
        "S": {"S": S, "amp_S": amp_S, "true_pedestal": True},
        "N": {"N": 0, "std_N": std_N},
        "P": {
            "P": P, "n_peaks": n_peaks,
            "amp_P": amp_P, "length_peaks": length_peaks
        }
    },
    "shift_pedestal": shift_pedestal,
    "label": "BDT between signal and peaked"
}

bdt_configurations["BDT_SN"] = { # Good
    "voi": ["max", "mean", "lower_than_mean_cons", "std"],
    "columns_to_compute": ["max_m_mean"],
    "columns": ['max_m_mean', 'lower_than_mean_cons', 'std'],
    "components" :{
        "S": {"S": S, "amp_S": amp_S},
        "N": {"N": N, "std_N": std_N}
    },
    "shift_pedestal": shift_pedestal,
    "label": "BDT between signal and noise"
}

bdt_configurations["BDT_SN_true_pedestal"] = { # Good, better than BDT_SN
    "voi": ["max", "mean", "lower_than_mean_cons", "std"],
    "columns_to_compute": ["max_m_mean"],
    "columns": ['max_m_mean', 'lower_than_mean_cons', 'std'],
    "components" :{
        "S": {"S": S, "amp_S": amp_S, "true_pedestal": True},
        "N": {"N": N, "std_N": std_N}
    },
    "shift_pedestal": shift_pedestal,
    "label": "BDT between signal and noise"
}

bdt_configurations["BDT_SN_true_pedestal2"] = { # bad : weird BDT structure + lose lots of events
    "voi": ["max", "mean", "nb_pos", "nb_neg", "std"],
    "columns_to_compute": ["max_m_mean", 'nb_pos_m_nb_neg'],
    "columns": ['max_m_mean', 'nb_pos_m_nb_neg', 'std'],
    "components" :{
        "S": {"S": S, "amp_S": amp_S, "true_pedestal": True},
        "N": {"N": N, "std_N": std_N}
    },
    "shift_pedestal": shift_pedestal,
    "label": "BDT between signal and noise"
}

bdt_configurations["BDT_SN_true_pedestal3"] = { # even better than `BDT_SN_true_pedestal`! `lower_than_mean_cons` might be misleading!!
    "voi": ["max", "mean", "std"],
    "columns_to_compute": ["max_m_mean"],
    "columns": ['max_m_mean', 'std'],
    "components" :{
        "S": {"S": S, "amp_S": amp_S, "true_pedestal": True},
        "N": {"N": N, "std_N": std_N}
    },
    "shift_pedestal": shift_pedestal,
    "label": "BDT between signal and noise"
}

bdt_configurations["BDT_SN2"] = {
    "voi": ["max", "mean", "std", "pos_max", "pos_min"],
    "columns_to_compute": ['pos_max_m_pos_min', 'max_m_mean'],
    "columns": ['pos_max_m_pos_min', 'max_m_mean', 'std'],
    "components" :{
        "S": {"S": S, "amp_S": amp_S},
        "N": {"N": N, "std_N": std_N}
    },
    "shift_pedestal": shift_pedestal,
    "label": "BDT between signal and noise"
}