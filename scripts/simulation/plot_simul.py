""" Plot the simulated sample. 
"""
import os.path as op
import sys
sys.path.append(op.abspath('../'))

import lib.plot as lp
from simulation import fcts, models, var


if __name__=="__main__":
    array_t, array_A = models.get_sig_model()
    array_t_full, array_A_full = fcts.read_signal_csv()
    fig, ax = fcts.plot_model(array_t, array_A)
    ax.plot(
        array_t_full, array_A_full,
        linestyle='--', color='g',
        alpha=0.6        
    )
    lp.save_fig(
        fig, var.image_path,
        "simulated_signal"
    )

