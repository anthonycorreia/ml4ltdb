""" Train a BDT on a simulated sample.

The simulated sample, and the training is parametrised
by the parameter `--config {config}`, where `config` is
a key of the dictionnary `bdt_configurations` defined in
`params_bdt.py`
"""

import argparse
import os.path as op

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.model_selection import train_test_split

import models
import var
import fcts
from params_bdt import bdt_configurations

from project import files, defs, col
from lib import data, plot as lp, bdt as lBDT
from lib.utils import dump_json, try_makedirs, dump_pickle


if __name__ == "__main__":

    # PARSER =========================================
    parser = argparse.ArgumentParser(
        "Train a BDT on a simulated sample"
    )

    parser.add_argument(
        '--config', required=True,
        type=str,
        help="Name of the configuration"
    )

    args = parser.parse_args()
    config_name = args.config

    # CONFIGURATION ==================================
    configuration = bdt_configurations[config_name]

    # dataframe
    components = configuration["components"]
    shift_pedestal = configuration["shift_pedestal"]

    # columns
    train_columns = configuration["columns"]
    list_voi = configuration["voi"]
    columns_to_compute = configuration["columns_to_compute"]

    # output
    bdt_name = config_name

    # Paths
    image_path = op.join(var.image_path, bdt_name)
    json_path = op.join(var.json_path, bdt_name)

    # PREPARE DATAFRAME =============================
    rng = np.random.default_rng(12345)

    print("GENERATE SIMULATED DATAFRAME")

    dataframe = models.get_full_dataframe(
        components,
        shift_pedestal=shift_pedestal,
        random_time_shift=True,
        rng=rng
    )

    dataframe = files.compute_voi(
        dataframe,
        variables=list_voi,
        verbose=False
    )

    # Get the formula of the columns to compute
    formula_columns = col.get_formula_columns(columns_to_compute)
    formula_column_labels = col.get_formula_labels(
        columns_to_compute, defs.column_labels
    )
    column_labels = {**defs.column_labels, **formula_column_labels}

    # Compute them
    dataframe = data.compute_vals(dataframe, formula_columns)

    dict_df = models.get_dict_df(dataframe)
    dict_df.keys()

    list_compos = list(dict_df.keys())
    list_df = list(dict_df.values())
    list_colors, list_labels = fcts.get_list_colors_labels(list_compos)

    # PLOTS BEFORE TRAINING ============================================
    print("PLOTS BEFORE TRAINING")
    # 1D histograms --------------------------------------
    fig, _ = lp.multi_plot_hist(
        list_df,
        train_columns, column_labels,
        list_colors=list_colors,
        list_labels=list_labels,
        logscale=True,
        nbins={
            'default': 40,
            'lower_than_mean_cons': 25,
            'larger_than_mean_cons': 25,
            'pos_max': 32,
            'pos_min': 32,
            'nb_pos': 32,
            'nb_neg': 32,
            'nb_pos_m_nb_neg': 32,
        },
        range_columns={
            'lower_than_mean_cons': [0, 24],
            'larger_than_mean_cons': [0, 24],
            'pos_max': [0, 31],
            'pos_min': [0, 31],
            'nb_pos': [0, 31],
            'nb_neg': [0, 31],
            'nb_pos_m_nb_neg': [0, 31],
        }
    )

    lp.save_fig(fig, image_path, "compare_1D")
    plt.close(fig)

    # Correlations -------------------------------------------
    for i, compo in enumerate(list_compos):
        fig, ax = plt.subplots(figsize=(12, 10))
        lp.correlations(dict_df[compo][train_columns], column_labels, ax=ax)

        ax.set_title(var.dict_df_labels[compo])
        plt.tight_layout()
        lp.save_fig(fig, image_path, f"correlations_{compo}")
        plt.close(fig)

    # TRAINING =========================================================
    # Prepare train and test dataframe -----------------------
    list_df_train = []
    list_df_test = []
    for compo in list_compos:
        df_compo_train, df_compo_test = \
            train_test_split(dict_df[compo], test_size=0.5,
                             random_state=rng.integers(2**32)
                             )
        list_df_train.append(df_compo_train)
        list_df_test.append(df_compo_test)

    df_train = pd.concat(list_df_train)
    df_test = pd.concat(list_df_test)

    # Training (adaboost) -----------------------------------
    print("TRAINING")

    hyperparams = {}
    dt = DecisionTreeClassifier(max_depth=3, min_samples_leaf=0.05)
    bdt = AdaBoostClassifier(dt, algorithm='SAMME', **hyperparams)
    bdt.fit(
        df_train[train_columns],
        df_train['is_sig'],
    )

    # ANALYSIS =========================================================
    print("TEST THE TRAINING")
    # Feature importance -------------------------------------
    dict_importances = lBDT.get_importances(bdt, train_columns)
    try_makedirs(json_path)
    dump_json(dict_importances, path=op.join(json_path, "importances.json"))

    # ROC curve ---------------------------------------------
    fig, _ = lBDT.plot_roc(
        df_test[train_columns],
        df_test["is_sig"],
        bdt
    )

    lp.save_fig(fig, image_path, "ROC")
    plt.close(fig)

    # Overtraining check ------------------------------------
    fig, _ = lBDT.compare_train_test(
        bdt,
        df_train[train_columns],
        df_train['is_sig'],
        df_test[train_columns],
        df_test['is_sig']
    )

    lp.save_fig(fig, image_path, "train_vs_test")
    plt.close(fig)

    # Check what is at low BDT ------------------------------
    lBDT.add_bdt_dataframe(df_train, bdt, train_columns)
    lBDT.add_bdt_dataframe(df_test, bdt, train_columns)

    list_BDT_cuts = [-1., 0., 0.6, 0.8, 2.]
    amp_S = components['S']['amp_S']
    amp_S_range = [1, amp_S]

    n_intervals = len(list_BDT_cuts) - 1

    fig, ax = plt.subplots(figsize=(8, 6))

    for i in range(len(list_BDT_cuts) - 2, -1, -1):
        BDT_cut_low = list_BDT_cuts[i]
        BDT_cut_high = list_BDT_cuts[i+1]

        list_amp_S = df_test.query(
            (f"bdt > {BDT_cut_low} and bdt < {BDT_cut_high}"
             " and is_sig==1")
        )['amp_S']

        ax.hist(
            list_amp_S,
            label=f"{BDT_cut_low} $<$ BDT $<$ {BDT_cut_high}",
            alpha=0.3,
            range=amp_S_range,
            bins=50
        )

    ax.set_xlabel('Signal amplitude')
    ax.set_ylabel('Number of events')
    ax.legend()

    ylim = ax.get_ylim()
    ax.set_xlim((1, amp_S))
    ax.set_ylim((1, ylim[1]*100))
    ax.set_yscale('log')

    lp.save_fig(fig, image_path, "BDT_categories")
    plt.close(fig)

    # Ampl VS BDT value for signal ------------------------
    df_test_sig = df_test.query("is_sig==True")

    fig, ax = lp.plot_hist2d(
        df_test_sig["amp_S"], df_test_sig["bdt"],
        "Signal Amplitude",  configuration["label"],
        logscale=True
    )

    lp.save_fig(fig, image_path, "amp_S_vs_bdt")
    plt.close(fig)

    # Purity vs BDT cut value -------------------------
    list_BDT_cuts = np.arange(-0.4, 1.2, 0.2)
    
    fig, axes = plt.subplots(
        len(list_compos), 1, 
        figsize=(8, len(list_compos)*6)
    )

    for i, compo in enumerate(list_compos):
        proport = np.empty_like(list_BDT_cuts)
        
        df_test_compo = df_test.query(
            f'{models.dict_df_flags[compo]}==True'
        )

        total_len = len(df_test_compo)

        for j, BDT_cut in enumerate(list_BDT_cuts):
            dataframe_bdt_cut = df_test_compo.query(f'bdt > {BDT_cut}')
            proport[j] = len(dataframe_bdt_cut) / total_len

        axes[i].plot(list_BDT_cuts, proport)
        axes[i].set_ylabel(
            f"{var.dict_df_labels[compo]} proportion "\
            + f"$\\frac{{{compo}(\\text{{BDT}} > c)}}{{{compo}}}$")
        lp.set_default_grid(axes[i])

    axes[-1].set_xlabel('c')
    lp.save_fig(fig, image_path, "yield_variations")


    # SAVE ======================================================
    print("SAVE THE BDT IN A PICKLE FILE")

    pickle_path = op.join(var.pickle_path, bdt_name + ".pickle")

    dump_pickle(
        {
            'bdt': bdt,
            'columns': train_columns
        },
        pickle_path
    )
