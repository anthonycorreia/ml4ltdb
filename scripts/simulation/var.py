""" Variables used for the simulation

Author: Anthony Correia
Date: 12 January 2021
"""

# Time step ===================
import os.path as op
from project import loc
deltaT = 25  # ns, in the ROOT files (`samples[i]` columns)
deltaT_csv = 5  # ns
n_samples = 32

# Paths =======================

image_path = op.join(
    loc.images_folder, "simulation"
)

pickle_path = op.join(
    loc.pickle_folder, "simulation"
)

json_path = op.join(
    loc.json_folder, "simulation"
)

interpol_pickle_name = "signal_spline.pickle"
trained_bdt_pickle_name = "trained_bdt.pickle"
trained_bdt_pickle_name_optimised = "trained_bdt_optimised.pickle"


# Columns =====================

plot_columns = [
    'max_m_mean', 'min_m_mean',
    'pos_max_m_pos_min',
    'mean_m_pedestal',
    'std', 'median_m_mean',
    'lower_than_mean_cons', 'larger_than_mean_cons'
]


# Plots =======================

all_cats = ['S', 'N', 'P']

dict_df_colors = {
    "S": 'g',
    "N": 'r',
    'P': 'b'
}

dict_df_labels = {
    "S": "Signal",
    "N": "Noise",
    'P': "Peaked"
}

dict_df_flags = {
    "S": "is_sig",
    "N": "is_noise",
    'P': "is_peaked"
}

dict_color_clu_cats = {
    f"S": {True:'g', False: 'k'},
    f"N": {True:'r', False: 'magenta'},
    f"P": {True:'b', False: 'purple'},
}

colormap_cats = {
    "S": "Greens",
    "P": "Blues",
    "N": "Reds",
}

dict_color_clu_cats = {
    f"S": {True: dict_df_colors['S'], False: 'grey'},
    f"N": {True: dict_df_colors['N'], False: 'orange'},
    f"P": {True: dict_df_colors['P'], False: 'purple'},
}


list_df_labels = ["Signal", "Noise", "Peaked"]
list_df_colors = list(dict_df_colors.values())
