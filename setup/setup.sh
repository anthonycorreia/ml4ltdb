#!/bin/bash
## Path of the repository added to the PYTHONPATH
export ANAROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export PYTHONPATH=$ANAROOT:$ANAROOT/python:$PYTHONPATH


## Activate the conda environment
# conda activate ML4LTDB
