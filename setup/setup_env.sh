#!/bin/bash
## Setup the conda environment with ROOT in it
# conda-forge allows to get ROOT
conda create -n ML4LTDB python=3.8 root=6.24.6 -c conda-forge

## Activate the new conda environment
conda config --add channels conda-forge
conda activate ML4LTDB

## Install necessary python libraries
pip install matplotlib numpy pandas scikit-learn uproot4 numba pytest hdbscan 

# For jupyter notebooks...
pip install jupyterlab notebook
conda install ipykernel
python -m ipykernel install --user --name=ML4LTDB