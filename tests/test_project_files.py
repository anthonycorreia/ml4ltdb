from project import files

import numpy as np

def test_compute_max_consec_true_mask():
    A = np.array([
        [1, 0, 1, 1, 1, 0, 0, 0, 1, 1],
        [1, 0, 0, 0, 1, 1, 1, 1, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0, 0, 0]
    ])

    A_max_length = np.array([3, 4, 1])

    assert np.array_equal(files.compute_max_consec_true_mask(A),
        A_max_length)

